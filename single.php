<?php
/**
 * wpinstant.v2 WordPress Theme
 * @package wpinstant.v2 WordPress Theme
 * User: dankerizer
 * Date: 06/11/2017 / 15.29
 */
if ( ! is_user_logged_in() ) {
	wp_redirect( WPINSTANT_USER_LOGIN );
	exit;
	//$pagename = 'user-login';
}
get_header();
$slug = '';
if ( have_posts() ) :
	$terms = get_the_terms( $post_id, 'category' );
	$slug  = $terms[0]->slug;
endif;
?>
    <main id="main" class="section" role="main">
		<?php
		if ( ! user_is_active() ):
			echo '<div class="container text-center">
            <div class="wellcome-message">
            <h2>Mohon Maaf, Anda tidak mempunyai Akses Kehalaman ini.</h2>
            <a href="'.WPINSTANT_PRODUCT_URL.'" class="btn btn-primary">Upgrade Sekarang</a>
            </div>
</div>';
		else:
			?>
			<?php if ( $slug == 'panduan-blogging-evergreen' ): ?>
			<?php get_template_part( '/template/single', 'panduan' ); ?>
		<?php else: ?>
			<?php get_template_part( '/template/single', 'default' ); ?>
		<?php endif; ?>
		<?php endif; ?>
    </main>
<?php get_footer(); ?>