<?php

/**
 * wpinstant Project
 * @package wpinstant
 * User: dankerizer
 * Date: 24/10/2017 / 23.05
 */
define( 'DANKER_BASE_PATH', TEMPLATEPATH . '/' );
define( 'DANKER_SITE_URL', get_option( 'siteurl' ) );
define( 'DANKER_DOMAIN', str_replace( array( 'http://', 'www.', '/' ), '', DANKER_SITE_URL ) );
define( 'DANKER_SITENAME', get_option( 'blogname' ) );
define( 'DANKER_SITEDESC', get_option( 'blogdescription' ) );
define( 'DANKER_THEME_URL', DANKER_SITE_URL . '/wp-content/themes/' . get_option( 'template' ) );

define( 'DANKER_THEME_ASSETS', DANKER_THEME_URL. '/assets/');
define( 'TEXTDOMAIN', 'dankedev' );

define( 'WPINSTANT_USER_DASHBOARD', DANKER_SITE_URL . '/dashboard' );
define( 'WPINSTANT_USER_LOGIN', DANKER_SITE_URL . '/login' );
define( 'WPINSTANT_DATA_JSON', DANKER_SITE_URL . '/data' );


//untuk keperluan update
define( 'WPINSTANT_THEME_VERSION', '1.0.' );
define( 'WPINSTANT_THEME_LAST_UPDATE', '2017-10-26 16:17:00' );
define( 'WPINSTANT_UPDATE_DESC', 'Fixing Bug and update feature' );
define( 'WPINSTANT_UPDATE_changelog', '(Recommended) Changelog. <p>This section will be opened by default when the user clicks \'View version XYZ details\'.</p>' );

define( 'WPINSTANT_LIVE', true );
if ( WPINSTANT_LIVE ) {
	$generator = DANKER_SITE_URL . '/hasil';
	$preview   = DANKER_SITE_URL . '/preview';
} else {
	$generator = 'http://generator.dev';
	$preview   = 'http://preview.wpinstant.dev';
}

define( 'WPINSTANT_API', $generator );
define( 'WPINSTANT_PREVIEW', $preview );
define( 'WPINSTANT_AVATAR', DANKER_THEME_URL . '/assets/images/avatar.png' );
//remove wp version and wp ?ver= query
remove_action( 'wp_head', 'wp_generator' );
function vc_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) ) {
		$src = remove_query_arg( 'ver', $src );
	}

	return $src;
}

add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 15, 1 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
function cc_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';

	return $mimes;
}

add_filter( 'upload_mimes', 'cc_mime_types' );
function wpinstant_js() {
	if ( ! is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', ( get_template_directory_uri() . '/assets/js/jquery.js' ), false, '2.2.4', false );
		wp_enqueue_script( 'jquery' );
	}
	wp_register_script( 'Tether', '//npmcdn.com/tether@1.2.4/dist/js/tether.min.js', array(), '1.2.4', true );
	wp_register_script( 'popper', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js', array(), '1.12.3', true );
	wp_register_script( 'boostrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), '4.0.0', true );
	wp_register_script( 'daterangepicker',  '//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js', array(), '4.0.0', true );
	wp_register_script( 'clipboardjs',  get_template_directory_uri() . '/assets/js/clipboard.min.js', array(), '1.7.1', true );



	//wp_enqueue_script( 'apli', get_template_directory_uri() . '/assets/js/app.js', array(), '3.0.3', true );
	wp_register_script( 'font-select', get_template_directory_uri() . '/assets/js/jquery.fontselect.js', array(), '1.6.1', false );
	wp_register_script( 'customjs', get_template_directory_uri() . '/assets/js/custom.js', array(), '3.0.3', true );
	//wp_enqueue_script( 'offcanvas', '//npmcdn.com/js-offcanvas@1.0/dist/_js/js-offcanvas.pkgd.min.js', array(), '1.6.1', false );
	//wp_enqueue_script( 'modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array(), '1.6.1', false );

	wp_enqueue_script( 'Tether' );
	wp_enqueue_script( 'popper' );
	wp_enqueue_script( 'boostrap' );
	//wp_enqueue_script( 'modernizr' );
	//wp_enqueue_script( 'offcanvas' );
	if(is_page(array('lisensi','link','coupon'))){
		wp_enqueue_script( 'clipboardjs' );
	}
	if(is_page()){
		wp_enqueue_script( 'font-select' );
		wp_enqueue_script( 'customjs' );
	}



}

add_action( 'wp_enqueue_scripts', 'wpinstant_js' );
function wpinstant_css() {

	wp_enqueue_style( 'bootstrap', get_theme_file_uri( '/assets/css/bootstrap.min.css' ), array(), '1.0' );
	wp_enqueue_style( 'fontawesome', get_theme_file_uri( '/assets/css/font-awesome.min.css' ), array(), '1.0' );
	//wp_enqueue_style( 'offcanvas','https://npmcdn.com/js-offcanvas@1.0/dist/_css/minified/js-offcanvas.css' , array(), '1.0' );

	if(is_page()){
		wp_enqueue_style( 'animate', get_theme_file_uri( '/assets/css/animate.css' ), array(), '1.0' );
	}
	wp_enqueue_style( 'wpinstant-style', get_stylesheet_uri() );
	if ( is_page(array('profile','affiliate','bonus','pixel','leaderboard','coupon'))  ) {

		wp_enqueue_style( 'user-profile', get_theme_file_uri( '/assets/css/user-edit.css' ), array(), '1.0' );

	}

	if (is_page()){
		//wp_enqueue_style( 'daterangepicker','//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css', array(), '1.0' );

	}


}

add_action( 'wp_enqueue_scripts', 'wpinstant_css' );
function wp_enqueue_ajax() {
	if ( is_page( 'create' ) && isset( $_REQUEST['step'] ) ) {
		if ( $_REQUEST['step'] == '2' || $_REQUEST['step'] == '3' ) {
			wp_enqueue_script( 'form', get_template_directory_uri() . '/assets/js/form.js', array(  ), '1.0', true );
			wp_enqueue_script(
				'iris',
				admin_url( 'js/iris.min.js' ),
				array( 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch' ),
				false,
				1
			);
			wp_enqueue_script(
				'wp-color-picker',
				admin_url( 'js/color-picker.min.js' ),
				array( 'iris' ),
				false,
				1
			);
			$colorpicker_l10n = array(
				'clear' => __( 'Clear' ),
				'defaultString' => __( 'Default' ),
				'pick' => __( 'Select Color' ),
				'current' => __( '' ),
			);
			wp_localize_script( 'wp-color-picker', 'wpColorPickerL10n', $colorpicker_l10n );
			wp_enqueue_style( 'wp-color-picker' );
		}
	}
	if ( is_page() ) {
		wp_enqueue_script( 'domaining', get_template_directory_uri() . '/assets/js/domain.js', array(), '1.0', true );
		wp_enqueue_script( 'bootbox', get_template_directory_uri() . '/assets/js/bootbox.min.js', array(), '1.0', true );
		wp_localize_script( 'domaining', 'domain_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}

}

add_action( 'wp_enqueue_scripts', 'wp_enqueue_ajax' );

require_once( trailingslashit( get_template_directory() ) . 'libs/WPInstant.php' );
require_once( trailingslashit( get_template_directory() ) . 'libs/Users.php' );
require_once( trailingslashit( get_template_directory() ) . 'libs/template-init.php' );
require_once( trailingslashit( get_template_directory() ) . 'libs/wpinstant-functions.php' );
require_once( trailingslashit( get_template_directory() ) . 'libs/fonts.php' );
require_once( trailingslashit( get_template_directory() ) . 'libs/API/WPInstant_API.php' );
require_once( trailingslashit( get_template_directory() ) . 'libs/API/dataapi.php' );
require_once( trailingslashit( get_template_directory() ) . 'libs/wp-bootstrap-navwalker.php' );
require_once( trailingslashit( get_template_directory() ) . 'libs/wuoy/load.php' );
require_once( trailingslashit( get_template_directory() ) . 'admin/admin.php' );
require_once( trailingslashit( get_template_directory() ) . 'admin/AdminReport.php' );
require_once( trailingslashit( get_template_directory() ) . 'libs/WPIRender.php' );
load_plugin_textdomain( 'wpinstant', false, basename( dirname( __FILE__ ) ) . '/languages' );
define( 'WPINSTANT_PRODUCT_URL',  get_the_permalink(wpi_get_theme_option('main_product')) );

require_once( trailingslashit( get_template_directory() ) . 'libs/data-hilang.php' );
