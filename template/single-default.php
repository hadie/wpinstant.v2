<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 10/12/2017 / 16.58
 */

?>
<div id="content" class="container mt-3">
	<div class="row">
		<div class="col-md-8 col">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					?>
					<article <?php post_class();?> id="post-<?php the_ID();?>">
						<header class="mb-3">
							<?php the_title( '<h1 class="h2">', '</h1>' ); ?>
						</header>

						<div class="entry-content">
							<?php the_content(); ?>
						</div>

					</article>
					<?php
				endwhile;
			else :
				//get_template_part( 'content', 'none' );
			endif;
			?>
		</div>
		<?php get_sidebar();?>
	</div>
</div>
