<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 28/11/2017 / 20.55
 */
global $current_user,$wuoyMember;
$allsales = $wuoyMember['sales-recap'];
$main_product = wpi_get_theme_option('main_product');
$affiliates = $allsales[ wpi_get_theme_option( 'main_product' ) ];
$userid  = isset($_GET['affiliate']) ? $_GET['affiliate'] : $current_user->ID;

?>

<?php if(current_user_can('manage_options')):?>
    <form action="">
        <select name="affiliate" id="affiliate" class="form-control" onchange="this.form.submit()">
            <?php
            arsort( $affiliates );
            foreach ($affiliates as $aff=>$data){
                $user         = get_userdata( $aff );
                $name           = $user->display_name;
                $selected        = selected($aff,$userid);
                if($aff == 0){
                    $name = 'tidak ada affiliate';
                }
                echo '<option value="'.$aff.'" '.$selected.'>'.$name.'</option>';
            }?>

        </select>
    </form>
<?php
else:
    $userid = $current_user->ID;
endif;?>
<?php
$args = array(
    'post_type' => 'wuoysales',
    'posts_per_page'   => -1,
    'post_parent' => $main_product,
    'meta_query' => array(
        'relation' => 'AND',
        'state_clause' => array(
            'key' => 'affiliate',
            'value' =>$userid,
        ),


    )
);

$getpost = get_posts($args);
//var_dump($getpost);
$query = new WP_Query( $args );

if ( $query->have_posts() ) :

?>
    <table class="table table-responsive-md">
        <thead>
        <tr>
            <th>Tanggal Pembelian</th>
            <th>Nama</th>
            <th>Invoice</th>
            <th>Komisi</th>
            <th>Status</th>
        </tr>
        </thead>
    <?php
    $totalkomisi = 0;
    while ( $query->have_posts() ) : $query->the_post();
         setup_postdata( $post );
        $id = $post->ID;
        $post_author = $post->post_author;
        $post_title     = $post->post_title;
        $post_modified = $post->post_modified;
        $post_date = $post->post_date;
        $userdata = get_userdata( $post_author );
        $post_status = $post->post_status;
        $bayar = get_post_meta($id,'price');

        //var_dump($post_meta);
       switch ($post_status){
           case 'publish':
               $status = '<span class="badge badge-success">Aktif</span>';
               $komisi     = (40 / 100)* (int)$bayar[0];
               break;
           case 'pending':
               $status = '<span class="badge badge-warning">Menunggu Konfirmasi</span>';
               $komisi = '';
               break;
           case 'draft':
               $status = '<span class="badge badge-secondary">Belum Bayar</span>';
               $komisi = '';
               break;


       }

?>
<tr>
    <td><?php echo date('d M,  Y h:i:s',strtotime($post_date));?></td>
    <td><?php echo $userdata->display_name;?> <span class="text-muted small"><?php echo $userdata->nickname;?></span></td>
    <td><?php echo $post_title;?></td>
    <td><?php echo number_format($komisi);?></td>
    <td><?php echo $status;?></td>
</tr>
<?php

        $totalkomisi += $komisi;

        //$totalkomisi++;

endwhile;
?>
        <tfoot>
        <tr>
            <th colspan="3" class="text-md-right">Total</th>
            <th colspan="2"><?php echo number_format($totalkomisi);?></th>
        </tr>
        </tfoot>
    </table>
<?php
    wp_reset_postdata();
    else:
    echo 'Maaf Belum Ada data Pembelian Melalui Link Affiliate Anda. Ayo Terus Promosi dan Dapatkan Bonus spesial dari kami dengan total senilai  Rp.51juta';
    endif;

    ?>

