<?php
/**
 * wpinstant Project
 * @package wpinstant
 * User: dankerizer
 * Date: 24/10/2017 / 23.55
 */
?>
<div class="small-container mt-5 ">
	<div class="row justify-content-center">
		<div class="col-sm-6 col-xs-6 text-center ">
			<div class="card step border-0">
				<img src="<?php echo DANKER_THEME_URL;?>/assets/images/form/create-blank-page.svg" class="img-responsive" alt="Create From Blank Pages"/>

				<div class="card-body">
					<a class="btn btn-warning" href="?step=2">
						<span class="display-block ">Start Now</span></a>
                    <p class="card-text text-muted small">Follow Our 5 Step Wizard <br/> to Create Theme</p>
				</div>
			</div>

		</div>
		<div class="col-sm-6 col-xs-6 text-center mb30">
			<div class="card step border-0">
				<img src="<?php echo DANKER_THEME_URL;?>/assets/images/form/create-template.svg" class="img-responsive" alt="Create From Template"/>

				<div class="card-body">
                    <a class="btn btn-warning " href="/create/favorite" >
						<span class="display-block ">Favorite Themes</span>
					</a>
                    <p class="card-text text-muted small">Choose From Our Favorite Pick<br/> Only 3 step to Create Themes <br/>(Coming Soon)</p>
				</div>
			</div>
		</div>
	</div>
</div>
