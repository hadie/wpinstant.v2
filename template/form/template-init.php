<?php
/**
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 09/09/2017 / 14.21
 */
function wpinstant_default_install()
{
    add_post_type_support('page', 'excerpt');
    add_theme_support('post-thumbnails'); // This feature enables post-thumbnail support for a theme
    //add_theme_support('custom-background');
    //add_theme_support('custom-header');
    add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
    add_theme_support('post-formats', array('gallery', 'video'));
    add_theme_support('custom-header', array('default-image' => get_template_directory_uri() . '/assets/images/logo.png', 'random-default' => false, 'width' => 165, 'height' => 35, 'header-text' => false));
    // register_nav_menu('topmenu', __('Top Menu', TEXTDOMAIN));
    register_nav_menu('header_menu_non_login', __('Header Menu Non Login', TEXTDOMAIN));
    register_nav_menu('header_menu_login', __('Header Menu Login', TEXTDOMAIN));
    register_nav_menu('footermenu', __('Footer Menu', TEXTDOMAIN));
    add_theme_support('title-tag');
    //	if (get_option('wpi_active') == '') {
    //		//wp_safe_redirect(admin_url('themes.php?page=wpi_theme-license'));
    //		//exit;
    //	} else {
    //		add_option('wpi_active', '', '', 'yes');
    //	}
}

add_action('after_setup_theme', 'wpinstant_default_install');
add_action('wp_ajax_delete_domain_request', 'delete_domain_request');
add_action('wp_ajax_nopriv_delete_domain_request', 'delete_domain_request');
function delete_domain_request()
{
    if (defined('DOING_AJAX') && DOING_AJAX) {
        global $wpdb;
        $current_user = wp_get_current_user();
        $class_user = new Users($current_user);
        $user = $_REQUEST['user_id'];
        $domain_id = $_REQUEST['domain_id'];
        $table = 'wp_instant_user_domain';
        //$wpdb->delete( $table, array( 'id' => $id ) );
        $domain_name = 'http://' . $class_user->get_domain_name_by_id($domain_id);
        $cek_theme = $class_user->mycurl($domain_name);
        if ($cek_theme == false) {
            $delete = $wpdb->delete($table, array('id' => $domain_id));
            //$message = '';
            $response = 'success';
        } else {
            //$response['error'] = '';
            $response = 'You must deactive or choose another theme before delete your domain.';
            //$response['error'] = '';
        }
    }
    echo $response;
    die();
}

add_action('wp_ajax_copy_theme_request', 'copy_theme_request');
add_action('wp_ajax_nopriv_copy_theme_request', 'copy_theme_request');
function copy_theme_request(){
    global $wpdb,$current_user;



	if (defined('DOING_AJAX') && DOING_AJAX) {

		$db_name = 'wp_instant_data';

		$creator = $_REQUEST['creator'];
		$themeslug = $_REQUEST['theme_slug'];
		$newthemename = $_REQUEST['theme_name'];
		$id             = $_REQUEST['theme_id'];
		$response = 'success';
		$check = get_theme_id($themeslug,$current_user->ID);
		if($check == null){
            $folder     = get_home_url().'/data/' . $creator . '/' . $themeslug . '.json';
			$json       = file_get_contents($folder);
            $data = json_decode($json,ARRAY_A);
            $folder     = get_home_url().'/data/' . $creator . '/' . $themeslug . '.json';
           // echo $folder;
            $json       = file_get_contents($folder);
            $data = json_decode($json,ARRAY_A);
            $data['theme_author'] =  $current_user->user_login ;
            $data['theme_slug'] =  sanitize_title_with_dashes($newthemename);

			$newfolder     ='data/' . $current_user->user_login . '/' . sanitize_title_with_dashes($newthemename) . '.json';
			$query = "SELECT * FROM $db_name WHERE id = '" . $id . "'";
			$results = $wpdb->get_row($query,ARRAY_A);
			$date       = date( 'Y-m-d H:i:s' );

			$savit = $wpdb->insert(
				$db_name,
				array(
					'theme_name' => addslashes( $newthemename ) ,
					'theme_slug' =>  addslashes( sanitize_title_with_dashes($newthemename) ),
					'theme_desc' => $results['theme_desc'],
					'theme_type' => $results['theme_type'],
					'data_path' => 'data',
					'create_date' => $date,
					'update_date' => $date,
					'user_id' => $current_user->ID,
					'status' => 1,
				),
				array(
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%d'
				)
			);

			if ( empty( $savit ) ) {
				if ( defined( 'WP_DEBUG' ) && true === WP_DEBUG ) {
					$wpdb->show_errors();
					$wpdb->print_error();
					$response = 'aaa gagal copy';
				}
				$response = 'gagal copy';
			} else {
				$response = 'success';
                $class     = new  WPInstant();
				//copy($folder, $newfolder);
				if (file_exists($newfolder)) {
					echo "failed to copy $folder...\n";
				}else{
                    $create_json = $class->makeJson( $json, $current_user->ID, sanitize_title_with_dashes($newthemename), false );
                }
			}


		}else{
			$response ='Failed';
		}
	}else{
		$response = 'failed';
	}

	echo $response;
	die();
}
add_action('wp_ajax_delete_theme_request', 'delete_theme_request');
add_action('wp_ajax_nopriv_delete_theme_request', 'delete_theme_request');
function delete_theme_request(){
	$response = '';
	if (defined('DOING_AJAX') && DOING_AJAX) {
		global $wpdb,$current_user;
		$db_name = 'wp_instant_data';
		$user_id = $_REQUEST['user_id'];
		$themeid    = $_REQUEST['themeid'];

		if($user_id != $current_user->ID){
			$response = 'not working '.$user_id;
			echo $response;
			die();
		}
		$class_user = new Users($current_user);
		$where =array('id' => $themeid,'user_id'=>$user_id);


		$delete = $wpdb->delete( $db_name, $where, array( '%s','%s' )  );

		$response = 'success';
	}
	echo $response;
	die();
}
//login form
function custom_login_logo()
{
    echo '<style type="text/css">h1 a { background: url(' . WPINSTANT_AVATAR . ') -12px -12px!important; height: 96px!important;width: 96px!important;border-radius: 50%}</style>';
}

add_action('login_head', 'custom_login_logo');
function ajax_login_init()
{

    wp_register_script('ajax-login-script', get_template_directory_uri() . '/assets/js/ajax-login-script.js', array('jquery'));
    //wp_enqueue_script('ajax-login-script');
    wp_localize_script('ajax-login-script', 'ajax_login_object', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'redirecturl' => home_url(),
        'loadingmessage' => __('Sending user info, please wait...')
    ));
    // Enable the user with no privileges to run ajax_login() in AJAX
    add_action('wp_ajax_nopriv_ajaxlogin', 'ajax_login');
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
}
function ajax_login()
{

    // First check the nonce, if it fails the function will break
    check_ajax_referer('ajax-login-nonce', 'security');
    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    //$info['remember'] = true;
    $user_signon = wp_signon($info, false);
    if (is_wp_error($user_signon)) {
        echo json_encode(array('loggedin' => false, 'message' => __('Wrong username or password.')));
    } else {
        echo json_encode(array('loggedin' => true, 'message' => __('Login successful, redirecting...')));
    }
    die();
}

function ajax_create_coupon_init()
{

    if(!is_admin()){
        wp_register_script('wpinstant-ajax', get_template_directory_uri() . '/assets/js/ajax.js', array('jquery'), true);
        wp_enqueue_script('wpinstant-ajax');
        wp_localize_script('wpinstant-ajax', 'ajax_object', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'loadingmessage' => __('Sedang Membuat Kode Kupon, please wait...'),
            'nonce' => wp_create_nonce( "all_ajax_nonce" ),
        ));
        // Enable the user with no privileges to run ajax_login() in AJAX



    }


}

add_action('init', 'ajax_create_coupon_init');

add_action('wp_ajax_nopriv_createcoupon_action', 'create_coupon_callback');
add_action('wp_ajax_createcoupon_action', 'create_coupon_callback');

function create_coupon_callback()
{
    //var_dump($_REQUEST);
    global $current_user;
    if (defined('DOING_AJAX') && DOING_AJAX) {
        $kode = $_REQUEST['kode'];

        $check = get_posts(array(
            'post_type' => 'wuoycoupon',
            'post_author' => $current_user->ID,
            'post_status' => 'any',
            'author__in' => $current_user->ID
        ));
        if (empty($check)) {

            if (isset($_REQUEST['kode']) && !empty($_REQUEST['kode'])) {
                if (!post_exists($kode)) {
                    $args = array(
                        'post_title' => $kode,
                        'post_name' => sanitize_title_with_dashes($kode),
                        'post_parent' => wpi_get_theme_option('main_product'),
                        'post_type' => 'wuoycoupon',
                        'post_author' => $current_user->ID,
                    );
                    if (current_user_can('activate_plugins')) :
                        $args["post_status"] = 'publish';
                    else :
                        $args["post_status"] = 'draft';
                    endif;
                    $postID = wp_insert_post($args);

                    if ($postID) {
                        $sampai = '2017-12-4';
                        $diskon = 59;
                        $limit = '3000';
                        update_post_meta( (int)$postID, 'discount',59);
                        update_post_meta( (int)$postID, 'discount_type', 'percentage');
                        update_post_meta( (int)$postID, 'activate_date', $sampai);
                        update_post_meta( (int)$postID, 'limit', intval($limit));
                        update_post_meta( (int)$postID,'global',false);
                        $response =  '<p>Kupon Anda "<strong>' . $kode . '</strong>" <span class="text-success">BERHASIL</span> dibuat.</p>';
                        $response .='<p>Silahkan refresh browser Anda.</p>';

                    } else {

                        $response = 'Maaf, Kupon Gagal Dibuat';
                    }
                } else {
                    $response = 'Kupon Sudah Ada, Silahkan Buat kode kupon yang lain';

                }
            }
        } else {
            $response = 'Anda sudah memiliki kupon. Untuk menghapus kupon yang sudah ada, silahkan hubungi Admin. ';
        }
        echo $response;
        die();
    }
}

add_action('wp_ajax_nopriv_editkupon_action', 'edit_coupon_callback');
add_action('wp_ajax_editkupon_action', 'edit_coupon_callback');

function edit_coupon_callback()
{
    global $current_user;
    $response = '';
    //echo $_REQUEST['kode'].' berhasil';
    $kode = $_REQUEST['kode'];
    $before = $_REQUEST['before'];
    $id = $_REQUEST['id'];
    if (defined('DOING_AJAX') && DOING_AJAX) {
        if (isset($_REQUEST['kode']) && !empty($_REQUEST['kode'])) {

            if ($kode == $before) {
                $response = 'Anda Tidak Merubah Kode Kupon';
            } else {
                $args = array(
                    'ID' => $id,
                    'post_title' => $kode,
                    'post_name' => sanitize_title_with_dashes($kode),
                    'post_parent' => wpi_get_theme_option('main_product'),
                    'post_type' => 'wuoycoupon',
                    'post_author' => $current_user->ID,
                );
                if (!post_exists($kode)) {
                    $postID = wp_update_post($args);
                    if ($postID) {
                        $response = 'Kode kupon <strong>  ' . $kode . '</strong> <span class="text-success">BERHASIL</span> disimpan ';
                        $response .='<br/>Silahkan refresh browser Anda.';
                    } else {
                        $response = 'Kode kupon <strong>  ' . $kode . '</strong> <span class="text-danger">GAGAL</span> disimpan <br/>';
                        $response .= 'Silahkan coba lagi, atau coba gunakan kode kupon yang lain.';
                    }
                } else {
                    $response = 'Kode kupon <strong>  ' . $kode . '</strong> <span class="text-warning">SUDAH DIPAKAI</span>  <br/>';
                    $response .= 'Silahkan coba gunakan kode kupon yang lain.';
                }
            }


        }
        echo $response;
    }

}

add_action('wp_ajax_nopriv_CreateLink_action', 'create_aff_link_callback');
add_action('wp_ajax_CreateLink_action', 'create_aff_link_callback');

function create_aff_link_callback(){
    check_ajax_referer( 'all_ajax_nonce', 'nonce' );

    if( true ){
        $product_id = wpi_get_theme_option('main_product');
        $affiliate = new Affiliate($product_id,get_current_user_id());
        $slug = $affiliate->randomString();
        $args = array(
            'post_title' => 'Shortlink ' . $slug,
            'post_type' => 'wuoyshortlink',
            'post_name' => $slug,
            'post_author' => get_current_user_id(),
            'post_parent' => $product_id,
            'post_status' => 'publish'
        );
        if (empty($affiliate->cekLink())) {


            $postID = wp_insert_post($args);
            $url = get_permalink($postID);
            $url = (substr($url, -1) == '/') ? substr($url, 0, -1) : $url;
            $output = array(
                'sales_page' => $url,
                'checkout_page'    => $url.'/form',
            );
            //$response = 'berhasil';

        }
        wp_send_json_success( $output );
    }else{
        wp_send_json_error( array( 'error' => 'something wrong' ) );
    }



}








function get_aff_id_by_cokies(){
    $cookie_var	= wuoyMemberGetOption('general_cookie');
    $main_product = wpi_get_theme_option('main_product');
    $cookie_liv	= wuoyMemberGetOption('general_cookie_life');
    $cookie_nme	= $cookie_var."-".$main_product;
    $expired	= (empty($cookie_liv)) ? 0 : time() + ($cookie_liv * 24 * 60 * 60 );

    if(isset($_COOKIE[$cookie_nme])){
        return $_COOKIE[$cookie_nme];
    }else{
        null;
    }
}


