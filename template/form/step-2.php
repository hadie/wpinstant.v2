<?php
/**
 * wpinstant Project
 * @package wpinstant
 * User: dankerizer
 * Date: 25/10/2017 / 00.17
 */
$current_user = wp_get_current_user();
$user_id      = $current_user->ID;
$display_name = $current_user->user_login;
$theme_slug   = get_theme_slug( $_REQUEST['themeid'], $user_id );
//echo $theme_slug;
$folder  = './data/' . $display_name . '/' . $theme_slug . '.json';
$message = '';
if ( ! file_exists( $folder ) && isset( $_REQUEST['edit'] ) ) {

	$json    = array();
	$message = '<div class="alert alert-dark mt-5" role="alert">
   <h4 class="alert-heading">We are really sorry, we lose  your data :(</h4>
   <p>We have been make some mistake that caused your data lost. But, you still can edit your theme by upload wpinstant-config.json in your theme below.</p>
   <p>make sure your theme folder is <strong>' . $theme_slug . '</strong> and file name is <strong>wpinstant-config.json</strong></p>
   <p><a href="#">Read this tutorial</a></p>
</div>';
	//https://github.com/olaferlandsen/FileUpload-for-PHP
	$upload = '
	<div class="row justify-content-center">
	<div class="col-md-8 col-12">
	<form action="" method="post" enctype="multipart/form-data" class="form-inline">
 <div class="form-group">
   <label for="exampleFormControlFile1">Example file input</label>  
    <input type="file" class="form-control-file" name="fileToUpload" id="fileToUpload">
  
    </div>
      <input type="submit" value="Upload Json" name="submit" class="btn btn-danger">
</form>
</div>
	</div>
';
}
if ( isset( $_POST['submit'] ) && isset( $_FILES ) ) {
	$simpan = simpan_kembali( $_POST, $_FILES, $theme_slug );
	//var_dump($simpan);
	if ( $simpan['code'] == 100 ) {
		$message = '<div class="alert alert-success alert-dismissible fade show" role="alert">
   <h4 class="alert-heading">Horay...</h4>
  <p>Your theme is ready to edit.</p>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
		$upload  = '';
	} else {
		$message = '<div class="alert alert-warning" role="alert">
   <h4 class="alert-heading">Sorry... :(</h4>
   <p>May be you\'re forgot with the file name of your theme. make sure your theme folder is <strong>' . $theme_slug . '</strong> and file name is <strong>wpinstant-config.json</strong></p>
   <p><a href="#">Read this tutorial</a></p>
</div>';
	}
}
if ( $_REQUEST['edit'] == 'true' && ! empty( $_REQUEST['themeid'] && file_exists( $folder ) ) ) {
	$theme_slug = get_theme_slug( $_REQUEST['themeid'], $user_id );
	$folder     = './data/' . $display_name . '/' . $theme_slug . '.json';
	$str        = file_get_contents( $folder );
	$json       = json_decode( $str, true );
	//echo $folder;
	$theme_version      = $json['theme_version'];
	$created            = $json['created'];
	$child_version      = (int) $json['child_version'] + 1;
	$theme_type         = $json['theme_type'];
	$theme_author       = $json['theme_author'];
	$theme_name         = $json['theme_name'];
	$theme_slug         = $json['theme_slug'];
	$theme_desc         = $json['theme_desc'];
	$header_type        = $json['header_type'];
	$footer_type        = $json['footer_type'];
	$home_type          = $json['home_type'];
	$single_type        = $json['single_type'];
	$attachment_type    = $json['attachment_type'];
	$select_scheme      = $json['select_scheme'];
	$theme_width        = $json['theme_width'];
	$sidebar_width      = $json['sidebar_width'];
	$scheme_type        = $json['scheme_type'];
	$footer_color       = $json['footer_color'];
	$link_color         = $json['link_color'];
	$text_color         = $json['text_color'];
	$main_bg_color      = $json['main_bg_color'];
	$main_footer_color  = $json['main_footer_color'];
	$widget_title_color = $json['widget_title_color'];
	$framework          = $json['framework'];
	$font               = $json['font'];
	$font_base_size     = $json['font_base_size'];
	$font_heading_size  = $json['font_heading_size'];
	$navbar_main_bg     = $json['navbar_type'];
	$disable            = 'readonly';
	$action             = '?step=finish&edit=true&themeid=' . $_REQUEST['themeid'];
} else {
	//

	$json               = array();
	$theme_version      = '';
	$child_version      = '1';
	$theme_type         = '';
	$theme_author       = '';
	$theme_name         = '';
	$theme_slug         = '';
	$theme_desc         = '';
	$header_type        = '';
	$footer_type        = '';
	$home_type          = '';
	$single_type        = '';
	$attachment_type    = '';
	$select_scheme      = '';
	$theme_width        = '1020';
	$sidebar_width      = 'responsive';
	$scheme_type        = '';
	$footer_color       = 'fff';
	$text_color         = '222222';
	$link_color         = 'ffab2f';
	$main_bg_color      = '';
	$main_footer_color  = '';
	$widget_title_color = '222';
	$framework          = 'bootstrap';
	$disable            = '';
	$font               = 'Roboto';
	$font_base_size     = 'medium';
	$font_heading_size  = 'medium';
	$navbar_main_bg     = 'default';
	$action             = '?step=finish';
}
?>
<?php echo $message; ?>
<?php echo $upload; ?>
<form action="<?php echo $action; ?>" method="post" class="theme-wizard">
    <fieldset>


        <legend>Select your sites types (Blog, Wallpaper, Blog with Attachment)</legend>

        <div id="theme_type_options" class="row product-chooser">
			<?php
			$site_type = array( 'blog', 'wallpaper', 'attachment', 'woocommerce' );
			foreach ( $site_type as $site ) {
				if ( $theme_type == $site ) {
				}
				$checked  = ( strtolower( $theme_type ) == $site ) ? 'checked="checked"' : '';
				$selected = ( strtolower( $theme_type ) == $site ) ? 'selected' : '';
				$title    = '';
				if ( $site == 'blog' ) {
					$title = 'Default WordPress Blog';
					$desc  = $title;
				} elseif ( $site == 'wallpaper' ) {
					$title = 'Wallpaper Site';
					$desc  = 'One Post - One Image, Auto Insert Image to Post and No Attachment page';
				} elseif ( $site == 'woocommerce' ) {
					$title = 'Online Store';
					$desc  = 'Online Store Using Woocommerce';
				} else {
					$title = 'Gallery Site';
					$desc  = 'One Post, Multiple Image, Auto Insert Image to Post with Attachment Page';
				}
				?>
				<div class="col">
                    <div class="product-chooser-item <?php echo $selected; ?> position-relative">
                        <div class="text-center mb-2 text-bold mb-3"><?php echo $title; ?></div>
                        <img
		                        src="<?php echo DANKER_THEME_URL; ?>/assets/images/form/template-<?php echo $site; ?>.svg"
		                        class="card-img-top" alt="Create From Template"
                        />

                        <input
		                        type="radio" id="type_<?php echo $site; ?>" name="theme_type"
		                        value="<?php echo $site; ?>" <?php echo $checked; ?>>
                        <label
		                        class="text-center text-muted small p-3 d-block"
		                        for="type_<?php echo $site; ?>"
                        ><?php echo $desc; ?></label>
                    </div>
                </div>
				<?php
			}
			?>

        </div>

        <div class="mt30">
            <a href="#" class="btn btn-warning btn-flat" id="next1">Next : <span>Theme Info</span></a>
        </div>
    </fieldset><!--step 2-->
               <!-- 25 -->
    <fieldset>

        <legend class="mb-4 mt-4">Let's give your theme information.</legend>

        <div class="row">
            <div class="col input-step input-theme-author">
                <label for="theme_author">Theme Author </label>
                <input
		                class="form-control" value="<?php echo $display_name ?>" type="text" name="theme_author"
		                id="theme_author" placeholder="theme author" <?php echo $disable; ?> required="required"
                >
                <div class="invalid-feedback invalid-theme-author small">
                    theme author must be defined
                </div>
            </div>
            <div class="col input-step input-theme-name">
                <label for="theme_name">Theme Name </label>
                <input
		                class="form-control" value="<?php echo $theme_name ?>" type="text" placeholder="theme name"
		                name="theme_name" id="theme_name" required
                >
                <div class="invalid-feedback invalid-theme-name small">
                    Theme name must be defined
                </div>
            </div>
	        <?php
	        if ( $_REQUEST['edit'] == 'true' && ! empty( $_REQUEST['themeid'] && file_exists( $folder ) ) ) {
		        echo '<input type="hidden" name="theme_slug" id="theme_slug" value="' . $theme_slug . '">';
	        }
	        echo '<input type="hidden" name="child_version" id="child_version" value="' . $child_version . '">';
	        ?>
	        <!--            <div id="theme_slug_input" class="col input-step input-theme-slug">-->
	        <!--                <label for="theme_slug">Theme Slug </label>-->
	        <!--                <input class="form-control" value="-->
	        <?php //echo $theme_slug ?><!--" type="text" placeholder="theme slug"-->
	        <!--                       name="theme_slug" id="theme_slug" --><?php //echo $disable; ?><!-- required>-->
	        <!--                <div class="invalid-feedback invalid-theme-slug small">-->
	        <!--                    slug theme tidak boleh kosong dan tidak boleh ada spasi-->
	        <!--                </div>-->
	        <!--            </div>-->
            <div class="clearfix"></div>
            <div class="w-100 d-none d-md-block m-6"></div>

            <div class="col input-step mt-4">
                <textarea
		                class="form-control" rows="3" placeholder="theme Description (optional)" id="theme_desc"
		                name="theme_desc"
                ><?php echo $theme_desc ?></textarea>
                <div class="invalid-feedback small">
                    Give your theme description for identification of your theme
                </div>
            </div>
        </div>
        <p class="text-muteds">* Use only character and number without special character & Symbol for theme Author and
            Theme Name.</p>

        <div class="mt-5 ">
            <a href="#" class="btn btn-warning" id="next2">Next : <span>Template</span></a> or
            <a href="#" class="btn btn-outline-dark" id="previous1">Go back</a>

        </div>

    </fieldset>
    <fieldset class="multi">
        <legend>Choose Design Template</legend>
        <div id="sht" class="select-validate">
            <h4><span>Header Template</span></h4>

            <div class="row product-chooser justify-content-start">
				<?php
				$he_type = array(
					'header_1' => 'Big Header - Center',
					'header_2' => 'Big Header - Left Logo - With Ads Box',
					'header_3' => 'Big Header - Left Logo - Right Menu',
					'header_4' => 'Small Header - Left Logo - Right Menu',
					'header_5' => 'Medium Header - Center',
					'header_6' => 'Small Header - Left Logo - Left Menu',
					'header_7' => 'Small Header - On Click Sidebar Menu',
				);
				foreach ( $he_type as $type => $title ) {
					$checked  = ( strtolower( $header_type ) == $type ) ? 'checked="checked"' : '';
					$selected = ( strtolower( $header_type ) == $type ) ? 'selected' : '';
					?>
					<div class="col-sm-4 text-center">
                        <div class="product-chooser-item <?php echo $selected; ?> position-relative">
                            <img
		                            src="<?php echo DANKER_THEME_URL; ?>/assets/images/form/<?php echo $type; ?>.svg"
		                            class="card-img-top" alt="Create From Template"
                            />

                            <input
		                            type="radio" id="select-<?php echo $type; ?>" name="header_type"
		                            value="<?php echo $type; ?>" <?php echo $checked; ?>>
                            <label
		                            class="text-center text-muted small p-3 d-block"
		                            for="select-<?php echo $type; ?>"
                            ><?php echo $title; ?> | <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Watch Preview" style="text-decoration: none;" class="front_preview"><i class="fa fa-eye"></i></a></label>
                        </div>
                    </div>
					<?php
				}
				?>
            </div>
        </div> 

        <div id="sho" class="select-validate">
            <h4><span>Select Home Template</span></h4>
            <div class="row product-chooser justify-content-start">
				<?php
				$h_type = array( 'home_1', 'home_2', 'home_3', 'home_4', 'home_5','home_6','home_7' );
				foreach ( $h_type as $type ) {
					$checked  = ( strtolower( $home_type ) == $type ) ? 'checked="checked"' : '';
					$selected = ( strtolower( $home_type ) == $type ) ? 'selected' : '';
					?>
					<div class="col-sm-4 text-center">
                        <div class="product-chooser-item <?php echo $selected; ?> position-relative">
                            <img
		                            src="<?php echo DANKER_THEME_URL; ?>/assets/images/form/<?php echo $type; ?>.svg"
		                            class="card-img-top" alt="Create From Template"
                            />
	                        <?php $exp = explode( '_', $type );
	                        $tipe      = $exp[1]; ?>
	                        <input
			                        type="radio" id="select-<?php echo $type; ?>" name="home_type"
			                        value="<?php echo $type; ?>" <?php echo $checked; ?>>
                            <label class="text-center text-muted small p-3 d-block">Home Type <?php echo $tipe; ?> | <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Watch Preview" style="text-decoration: none;" class="front_preview"><i class="fa fa-eye"></i></a></label>
							
                        </div>
                    </div>
					<?php
				}
				?>
            </div>
        </div>
        <div id="ssp" class="select-validate">
            <h4><span>Single Pages</span></h4>
            <div class="row product-chooser justify-content-start">
				<?php
				$s_type = array(
					'single_1' => 'Left Content - Right Sidebar in different box',
					'single_2' => 'Left Content - Right Sidebar inside one box',
					'single_3' => 'One Column - No Sidebar'
				);
				foreach ( $s_type as $type => $desc ) {
					$checked  = ( strtolower( $single_type ) == $type ) ? 'checked="checked"' : '';
					$selected = ( strtolower( $single_type ) == $type ) ? 'selected' : '';
					$more     = '';
					?>
					<div class="col-sm-4 text-center">
                        <div class="product-chooser-item <?php echo $selected; ?> position-relative">
                            <img
		                            src="<?php echo DANKER_THEME_URL; ?>/assets/images/form/<?php echo $type; ?>.svg"
		                            class="card-img-top" alt="Create From Template"
                            />

                            <input
		                            type="radio" id="select-<?php echo $type; ?>" name="single_type"
		                            value="<?php echo $type; ?>" <?php echo $checked; ?>>
                            <label
		                            class="text-center text-muted small p-3 d-block"
		                            for="select-<?php echo $type; ?>"
                            ><?php echo $desc; ?> |  <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Watch Preview" style="text-decoration: none;" class="single_preview"><i class="fa fa-eye"></i></a></label>
                           
                        </div>
                    </div>
					<?php
				}
				?>
            </div>
        </div>
        <div id="attachment" class="select-validate">


            <h4><span>Attachment Pages</span></h4>
            <div class="row product-chooser justify-content-start">
                <input type="hidden" name="attachment_type" value="null" >
	            <?php
	            $a_type = array(
		            'attachment_1' => 'Attachment - Center',
		            'attachment_2' => 'Attachment - Left Ads Box'
	            );
	            foreach ( $a_type as $type => $desc ) {
		            $checked  = ( strtolower( $attachment_type ) == $type ) ? 'checked="checked"' : '';
		            $selected = ( strtolower( $attachment_type ) == $type ) ? 'selected' : '';
		            ?>
		            <div class="col-sm-4 text-center">
                        <div class="product-chooser-item <?php echo $selected; ?> position-relative">
                            <img
		                            src="<?php echo DANKER_THEME_URL; ?>/assets/images/form/<?php echo $type; ?>.svg"
		                            class="card-img-top" alt="Create From Template"
                            />

                            <input
		                            type="radio" id="select-<?php echo $type; ?>" name="attachment_type"
		                            value="<?php echo $type; ?>" <?php echo $checked; ?>>
                            <label
		                            class="text-center text-muted small p-3 d-block"
		                            for="select-<?php echo $type; ?>"
                            ><?php echo $desc; ?> | <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Watch Preview" style="text-decoration: none;" class="attachment_preview"><i class="fa fa-eye"></i></a></label>
                            
                        </div>
                    </div>
		            <?php
	            }
	            ?>

            </div>
        </div>
        <div id="sfo" class="select-validate">
            <h4><span>Footer Template</span></h4>
            <div class="row product-chooser justify-content-start">


				<?php
				$footer_selector = array(
					'footer_1' => 'Big Footer - Three Columns',
					'footer_2' => 'Small Footer',
					'footer_3' => 'Medium Footer - Center Menu'
				);
				foreach ( $footer_selector as $type => $desc ) {
					$checked  = ( strtolower( $footer_type ) == $type ) ? 'checked="checked"' : '';
					$selected = ( strtolower( $footer_type ) == $type ) ? 'selected' : '';
					?>
					<div class="col-sm-4 text-center">
                        <div class="product-chooser-item <?php echo $selected; ?> position-relative">
                            <img
		                            src="<?php echo DANKER_THEME_URL; ?>/assets/images/form/<?php echo $type; ?>.svg"
		                            class="card-img-top" alt="Create From Template"
                            />

                            <input
		                            type="radio" id="select-<?php echo $type; ?>" name="footer_type"
		                            value="<?php echo $type; ?>" <?php echo $checked; ?>>
                            <label
		                            class="text-center text-muted small p-3 d-block"
		                            for="select-<?php echo $type; ?>"
                            ><?php echo $desc; ?> | <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Watch Preview" style="text-decoration: none;" class="front_preview"><i class="fa fa-eye"></i></a></label>
                            
                        </div>
                    </div>
					<?php
				}
				?>
            </div>
        </div>
        <div class="mt-3">
            <a href="#" class="btn btn-warning" id="next3">Next : <span>Colors</span></a> or <a
			        href="#"
			        class="btn btn-outline-dark"
			        id="previous2"
	        >Go
                back</a>

        </div>
    </fieldset>
    <fieldset>
        <div class="mt-5">
        	<div class="row">
        		<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
	        		<legend>Style and Colors</legend>
	        	</div>
	        	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
	        		<a style="float: right;" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Watch Preview" id="preview_width"><i class="fa fa-eye"></i></a>
	        	</div>
        	</div>
        	
            
            <h4>Container Width</h4>
            <label
		            for="theme_width"
		            class="text-muted small"
            ><?php echo _e( 'How Wide theme container do you want', 'wpinstant' ); ?></label>
            <div class="range-slider range ">

                <input
		                type="range" name="theme_width" id="theme_width" class="range-slider__range" min="970" max="1200"
		                value="<?php echo $theme_width; ?>" step="10"
                >
                <output class="range-slider__value bg-primary text-light" id="them_width">970</output>
            </div>
            <div class="row justify-content-center">
                <div class="monitor col-md-8 col-12 theme_width_preview position-relative mt-5">
                    <div class="layar position-relative">
                        <div class="browser position-absolute">
                        </div>
                        <div class="cpreview"><h2>Container Preview</h2></div>
                    </div>
                </div>

            </div>
            <div class="w-100 mt-5 mb-5"></div>
            <h4>Sidebar Width</h4>
            <label
		            for="sidebar_width"
		            class="text-muted small"
            ><?php echo _e( 'How Wide theme Sidebar do you want', 'wpinstant' ); ?>
	            <select name="sidebar_width" id="sidebar_width" class="form-control custom-select">
                    <?php
                    $sidebars = array(
	                    'sidebar_responsive' => 'Responsive Size',
	                    'sidebar_fixed_300'  => 'Fixed 300px',
	                    'sidebar_fixed_336'  => 'Fixed 336px'
                    );
                    foreach ( $sidebars as $sidebar => $text ) {
	                    $selected = selected( $sidebar_width, $sidebar, false );
	                    echo '<option ' . $selected . ' value="' . $sidebar . '">' . $text . '</option>';
                    }
                    ?>
                </select>
            </label>
            <div class="w-100 mt-5 mb-5"></div>
            <h4>Fonts</h4>

            <div class="row mt-3 ">
                <div class="col">
                    <div class="form-group">
                        <div class="form-group">
                            <label
		                            for="font"
		                            class="text-muted small"
                            ><?php echo _e( 'Choose your favorite font', 'wpinstant' ); ?></label>
                            <div class="clearfix"></div>
                            <input
		                            id="font" name="font" type="text" value="<?php echo $font; ?>"
		                            class="form-control w-100"
                            />
                        </div>
                        <div class="form-group">
                            <label for="font_base_size" class="text-muted ">Font Base Size</label>
	                        <?php
	                        $base_size = array( 'small', 'medium', 'large' );
	                        foreach ( $base_size as $size ) {
		                        $selected = selected( $size, $font_base_size, false );
		                        echo '<label class="custom-control custom-radio" for="font_base_size[' . $size . ']">
                                <input id="font_base_size[' . $size . ']" name="font_base_size" value="' . $size . '" type="radio" ' . $selected . ' class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">' . ucwords( $size ) . '</span>
                                </label>';
	                        }
	                        ?>
                        </div>


                    </div>
                </div>
                <div class="col ">

                    <label for="font" class="text-muted small"><?php echo _e( 'Font Preview', 'wpinstant' ); ?></label>
                    <h1 class="font-heading-preview pl-2">This is Title Lorem</h1>
                    <textarea id="font-preview" cols="30" rows="2" class="font-preview form-control form-control-plaintext">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</textarea>

                </div>
            </div>
            <div class="w-100 mt-5 mb-5"></div>
            <div class="row">
            	<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            		<h4>Color Setting</h4>
            	</div>
            	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            		<a style="float: right;" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Watch Preview" id="scheme_preview"><i class="fa fa-eye"></i></a>
            	</div>
            </div>
            <label
		            for="select_scheme"
		            class="text-muted small"
            ><?php echo _e( 'Choose your favorite colors or choose from our recomendations', 'wpinstant' ); ?>
	            <select
			            id="select_scheme"
			            class="form-control custom-select" name="select_scheme"
	            >

                    <?php
                    $choices = array(
	                    'scheme' => 'Choose from our recomendations',
	                    'manual' => 'Manual Pick'
                    );
                    foreach ( $choices as $choice => $text ) {
	                    $selected = selected( $select_scheme, $choice, false );
	                    echo '<option value="' . $choice . '" ' . $selected . '>' . $text . '</option>';
                    }
                    ?>

                </select>
            </label>

            <div class="w-100 mt-5 mb-5"></div>
            <div id="pilih_scheme">

                <legend>Choose from our recomendations</legend>
	            <?php color_shchema( $scheme_type ); ?>
            </div>

            <div class="w-100 mt-5 mb-5"></div>
            <div id="manual_color">

                <h4>Manual Pick</h4>
                <legend class="small text-muted mt-3"><?php echo _e( 'Text Colors' ); ?></legend>
                <div class="row w-100 justify-content-start">
                    <?php
                    $modif = array(
	                    'text_color'         => '222222',
	                    'link_color'         => 'ffab2f',
	                    'title_color'        => '000',
	                    'widget_title_color' => '222',
	                    'footer_color'       => 'fff'
                    );
                    foreach ( $modif as $key => $color ) {
	                    if ( isset( $_REQUEST['edit'] ) and ! empty( $_REQUEST['themeid'] ) ) {
		                    $value = $json[ $key ];
		                    $value = str_replace( '#', '', $value );
	                    } else {
		                    $value = $color;
	                    }
	                    echo '<div class="col ">
                        <label class="text-muted small">' . ucwords( str_replace( '_', ' ', $key ) ) . ' </label><br/>
                            <input class="color-field " data-default-color="#' . $value . '" id="' . $key . '" name="' . $key . '" type="text" placeholder="" value="#' . $value . '">
                       
                    </div>';
                    }
                    ?>
                </div>


                <legend class="small text-muted mt-3"><?php echo _e( 'Background Colors' ); ?></legend>
                <div class="row w-100 justify-content-start">
                    <div class="col mb-3 input-group-sm">
                        <label for="navbar_type" class="small text-muted">Navbar Main Background</label>
                        <select name="navbar_type" id="navbar_type" class="custom-select form-control">
                            <?php
                            $nv = array( 'default' => 'Light', 'inverse' => 'Dark' );
                            foreach ( $nv as $n => $txt ) {
	                            $selected = selected( $navbar_main_bg, $n, false );
	                            echo '<option value="' . $n . '" ' . $selected . '>' . $txt . '</option>';
                            }
                            ?>

                        </select>
                    </div>
	                <?php
	                $background = array(
		                'header_color'      => 'ec6fcf',
		                'navbar_menu_color' => '222',
		                'main_bg_color'     => 'ffffff',
		                'main_footer_color' => '222'
	                );
	                foreach ( $background as $key => $color ) {
		                if ( isset( $_REQUEST['edit'] ) and ! empty( $_REQUEST['themeid'] ) ) {
			                $value = $json[ $key ];
			                $value = str_replace( '#', '', $value );
		                } else {
			                $value = $color;
		                }
		                $title = str_replace(
			                array( 'header_color', 'navbar_menu_color', 'main_bg_color', 'main_footer_color' ),
			                array(
				                'Header Background',
				                'Menu Background',
				                'Main background',
				                'Footer background'
			                ), $key
		                );
		                $title = str_replace( '_', ' ', $title );
		                echo '<div class="col mb-3">
                        <label class="text-muted small">' . ucwords( $title ) . ' </label><br/>
                            <input class="color-field " data-default-color="#' . $value . '" id="' . $key . '" name="' . $key . '" type="text" placeholder="" value="#' . $value . '">
                       
                    </div>';
	                }
	                ?>
                </div>

            </div>
            <div class="mt-5 w-100 ">
                <a href="#" class="btn btn-warning" id="next4">Next : Framework</a> or <a
			            href="#"
			            class="btn btn-outline-dark"
			            id="previous3"
	            >Go back</a>

            </div>
        </div>
    </fieldset>

    <fieldset>
        <h4>FrameWork</h4>
        <p class="small text-muted">Choose HTML, CSS,JS Framework</p>
        <div class="row product-chooser justify-content-start disabledX">


			<?php
			$frame = array( 'bootstrap', 'foundation', 'materialize' );
			foreach ( $frame as $f ) {
				$selected = ( $f == 'bootstrap' ) ? 'selected' : '';
				//$disable  = ( $f == 'bootstrap' ) ? '' : 'disabled';
				$disable = '';
				// $coming   = ( $f == 'bootstrap' ) ? '' : '(Coming Soon)';
				//$checked  = (strtolower($framework) == $f) ? 'checked="checked"' : '';
				$checked = '';
				if ( strtolower( $framework ) == $f || $f == 'bootstrap' ) {
					$checked = 'checked="checked"';
				}
				?>
				<div class="col-md-2">
                    <div class="product-chooser-item <?php echo $selected; ?> position-relative">
                        <img
		                        src="<?php echo DANKER_THEME_URL; ?>/assets/images/form/<?php echo $f; ?>.svg"
		                        class="card-img-top" alt="Select Framework"
                        />

                        <input
		                        type="radio" id="select-<?php echo $f; ?>" name="framework"
		                        value="<?php echo $f; ?>" <?php echo $checked; ?>>
                        <label
		                        class="text-center text-muted small p-3 d-block"
		                        for="select-<?php echo $f; ?>"
                        ><?php echo ucwords( $f ); ?></label>
                    </div>
                </div>
				<?php
			}
			?>
        </div>
        <p class="text-muted">
			<?php echo __( 'After you push this button, WPInstant will build your custom theme.', 'wpinstant' ); ?>
        </p>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i>
            Create Themes Now
        </button>
        or <a class="btn btn-outline-dark" href="#" id="previous4">Go back</a>
    </fieldset>
</form>
