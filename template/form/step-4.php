<?php
/*


'header'  => '#',
'body'    => '#',
'content' => '#',
'link'    => '#',
'footer'  => '#',


 */

$skema = array(
	'scheme_1'  => array(
		'header'  => '#ffffff',
		'body'    => '#f7f7f7',
		'content' => '#ffffff',
		'link'    => '#ff922b',
		'footer'  => '#000000',
	),
	'scheme_2'  => array(
		'header'  => '#000000',
		'body'    => '#1f1f1f',
		'content' => '#ffffff',
		'link'    => '#ff922b',
		'footer'  => '#000000',
	),
	'scheme_3'  => array(
		'header'  => '#ffffff',
		'body'    => '#ebf6dd',
		'content' => '#ffffff',
		'link'    => '#f56991',
		'footer'  => '#314713',
	),
	'scheme_4'  => array(
		'header'  => '#fafafa',
		'body'    => '#c8c8a9',
		'content' => '#ffffff',
		'link'    => '#fe4365',
		'footer'  => '#264537',
	),
	'scheme_5'  => array(
		'header'  => '#385da9',
		'body'    => '#e9ebee',
		'content' => '#ffffff',
		'link'    => '#365899',
		'footer'  => '#20345d',
	),
	'scheme_6'  => array(
		'header'  => '#eff2e2',
		'body'    => '#daddcd',
		'content' => '#ffffff',
		'link'    => '#fc987a',
		'footer'  => '#70685c',
	),
	'scheme_7'  => array(
		'header'  => '#ffffff',
		'body'    => '#927781',
		'content' => '#ffffff',
		'link'    => '#91204d',
		'footer'  => '#452632',
	),
	'scheme_8'  => array(
		'header'  => '#ffffff',
		'body'    => '#a37140',
		'content' => '#ffffff',
		'link'    => '#a37140',
		'footer'  => '#482808',
	),
	'scheme_9'  => array(
		'header'  => '#ffffff',
		'body'    => '#609367',
		'content' => '#ffffff',
		'link'    => '#39b54a',
		'footer'  => '#02400a',
	),
	'scheme_10' => array( 'header'  => '#ffffff',
	                      'body'    => '#cb4d52',
	                      'content' => '#ffffff',
	                      'link'    => '#d90d14',
	                      'footer'  => '#630408',
	),
	'scheme_11' => array( 'header'  => '#f6ece8',
	                      'body'    => '#dab09e',
	                      'content' => '#faf5f3',
	                      'link'    => '#923339',
	                      'footer'  => '#5c2626',
	),
	'scheme_12' => array( 'header'  => '#000000',
	                      'body'    => '#252525',
	                      'content' => '#000000',
	                      'link'    => '#fff200',
	                      'footer'  => '#000000',
	),
	'scheme_13' => array( 'header'  => '#ffffff',
	                      'body'    => '#effdff',
	                      'content' => '#ffffff',
	                      'link'    => '#437abb',
	                      'footer'  => '#083869',
	),
	'scheme_14' => array( 'header'  => '#f8feff',
	                      'body'    => '#ddf0f2',
	                      'content' => '#ffffff',
	                      'link'    => '#5bc6d0',
	                      'footer'  => '#030423',
	),
	'scheme_15' => array( 'header'  => '#ffffff',
	                      'body'    => '#e9e5be',
	                      'content' => '#ffffff',
	                      'link'    => '#9c9346',
	                      'footer'  => '#374021',
	),
	'scheme_16' => array( 'header'  => '#f7f5f5',
	                      'body'    => '#dad5d2',
	                      'content' => '#ffffff',
	                      'link'    => '#c4483e',
	                      'footer'  => '#4a4c4f',
	),
	'scheme_17' => array( 'header'  => '#ffffff',
	                      'body'    => '#e6cfd7',
	                      'content' => '#ffffff',
	                      'link'    => '#ec6fcf',
	                      'footer'  => '#5c1e4e',
	),
	'scheme_18' => array( 'header'  => '#6e6e6e',
	                      'body'    => '#797474',
	                      'content' => '#303030',
	                      'link'    => '#f7941d',
	                      'footer'  => '#4e4d4d',
	),
	'scheme_19' => array( 'header'  => '#e3e3e3',
	                      'body'    => '#c9c9c9',
	                      'content' => '#f5f3f3',
	                      'link'    => '#ac5726',
	                      'footer'  => '#59321c',
	),
	'scheme_20' => array( 'header'  => '#43542a',
	                      'body'    => '#495d52',
	                      'content' => '#ffffff',
	                      'link'    => '#e4b434',
	                      'footer'  => '#43542a',
	),
);
echo ' <div class="row product-chooser justify-content-start">';
foreach ( $skema as $scheme => $warna ) {

	$selected = ( strtolower( $scheme_type ) == $warna ) ? 'selected' : '';
	?>
    <div class="col-md-4 ">

        <div class="product-chooser-item <?php echo $selected; ?> position-relative">
			<?php
			?>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 105.833 7.937">
                <g transform="translate(0 -289.1)">
                    <rect width="21.167" height="7.937" y="289.063" fill="<?php echo $warna['header']; ?>" ry="0"/>
                    <rect width="21.167" height="7.937" x="21.167" y="289.063" fill="<?php echo $warna['body']; ?>"
                          ry="0"/>
                    <rect width="21.167" height="7.937" x="42.333" y="289.063" fill="<?php echo $warna['content']; ?>"
                          ry="0"/>
                    <rect width="21.167" height="7.937" x="63.5" y="289.063" fill="<?php echo $warna['link']; ?>"
                          ry="0"/>
                    <rect width="21.167" height="7.937" x="84.667" y="289.063" fill="<?php echo $warna['footer']; ?>"
                          ry="0"/>
                    <rect width="21.167" height="7.937" x="21.167" y="289.063" fill="#daddcd" ry="0"/>
                    <text x="4.14" y="294.335" style="line-height:100%;-inkscape-font-specification:Arial"
                          font-size="3.704" letter-spacing="0" word-spacing="0">
                        <tspan x="4.14" y="294.335">Header</tspan>
                    </text>
                    <text x="27.011" y="293.967" style="line-height:100%;-inkscape-font-specification:'Arial Bold'"
                          font-size="3.704" letter-spacing="0" word-spacing="0">
                        <tspan x="27.011" y="293.967" font-weight="normal">Body</tspan>
                    </text>
                    <text x="45.85" y="294.357" style="line-height:100%;-inkscape-font-specification:'Arial Bold'"
                          font-size="3.704" letter-spacing="0" word-spacing="0">
                        <tspan x="45.85" y="294.357" font-weight="normal">Content</tspan>
                    </text>
                    <text x="70.151" y="294.357" fill="#eee"
                          style="line-height:100%;-inkscape-font-specification:'Arial Bold'" font-size="3.704"
                          letter-spacing="0" word-spacing="0">
                        <tspan x="70.151" y="294.357" font-weight="normal">Link</tspan>
                    </text>
                    <text x="89.323" y="294.335" fill="#999"
                          style="line-height:100%;-inkscape-font-specification:'Arial Bold'" font-size="3.704"
                          letter-spacing="0" word-spacing="0">
                        <tspan x="89.323" y="294.335" font-weight="normal">Footer</tspan>
                    </text>
                </g>
            </svg>

        </div>
    </div>
	<?php
}
echo '</div>';
$schema = array(
	'palete0'  => array( '#ffffff', '#f7f7f7', '#ffffff', '#ff922b', '#000000' ),
	'palete1'  => array( '#000000', '#1f1f1f', '#ffffff', '#ff922b', '#000000' ),
	'palete2'  => array( '#ffffff', '#ebf6dd', '#ffffff', '#f56991', '#314713' ),
	'palete3'  => array( '#fafafa', '#c8c8a9', '#ffffff', '#fe4365', '#264537' ),
	'palete4'  => array( '#385da9', '#e9ebee', '#ffffff', '#365899', '#20345d' ),
	'palete5'  => array( '#eff2e2', '#daddcd', '#ffffff', '#fc987a', '#70685c' ),
	'palete6'  => array( '#ffffff', '#927781', '#ffffff', '#91204d', '#452632' ),
	'palete7'  => array( '#ffffff', '#a37140', '#ffffff', '#a37140', '#482808' ),
	'palete8'  => array( '#ffffff', '#609367', '#ffffff', '#39b54a', '#02400a' ),
	'palete9'  => array( '#ffffff', '#cb4d52', '#ffffff', '#d90d14', '#630408' ),
	'palete10' => array( '#f6ece8', '#dab09e', '#faf5f3', '#923339', '#5c2626' ),
	'palete11' => array( '#000000', '#252525', '#000000', '#fff200', '#000000' ),
	'palete12' => array( '#ffffff', '#effdff', '#ffffff', '#437abb', '#083869' ),
	'palete13' => array( '#f8feff', '#ddf0f2', '#ffffff', '#5bc6d0', '#030423' ),
	'palete14' => array( '#ffffff', '#e9e5be', '#ffffff', '#9c9346', '#374021' ),
	'palete15' => array( '#f7f5f5', '#dad5d2', '#ffffff', '#c4483e', '#4a4c4f' ),
	'palete16' => array( '#ffffff', '#e6cfd7', '#ffffff', '#ec6fcf', '#5c1e4e' ),
	'palete17' => array( '#6e6e6e', '#797474', '#303030', '#f7941d', '#4e4d4d' ),
	'palete18' => array( '#e3e3e3', '#c9c9c9', '#f5f3f3', '#ac5726', '#59321c' ),
	'palete19' => array( '#43542a', '#495d52', '#ffffff', '#e4b434', '#43542a' ),
);
$i      = 1;
foreach ( $schema as $key => $item ) {
	//    echo "
	//    'scheme_$i' => array(
	//		'header'  => '$item[0]',
	//		'body'    => '$item[1]',
	//		'content' => '$item[2]',
	//		'link'    => '$item[3]',
	//		'footer'  => '$item[4]',
	//	),<br/>
	//
	//    ";
	$i ++;
}
?>

<div class="w-100 mt-5 mb-5"></div>

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 105.833 7.937">
    <g transform="matrix(1.1 0 0 1.1 -69.8 -225.9)">
        <rect width="18.653" height="6.995" x="61.468" y="199.072" fill="#fff" ry="0"/>
        <rect width="18.653" height="6.995" x="80.121" y="199.072" fill="#f7f7f7" ry="0"/>
        <rect width="18.653" height="6.995" x="98.775" y="199.072" fill="#fff" ry="0"/>
        <rect width="18.653" height="6.995" x="117.428" y="199.072" fill="#ff922b" ry="0"/>
        <rect width="18.653" height="6.995" x="136.081" y="199.072" ry="0"/>
    </g>
</svg>