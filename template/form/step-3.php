<div id="sho" class="select-validate">
            <h4><span>Select Home Template</span></h4>
            <div class="row product-chooser justify-content-start">
				<?php
				$h_type = array( 'home_1', 'home_2', 'home_3', 'home_4', 'home_5','home_6','home_7' );
				foreach ( $h_type as $type ) {
					$checked  = ( strtolower( $home_type ) == $type ) ? 'checked="checked"' : '';
					$selected = ( strtolower( $home_type ) == $type ) ? 'selected' : '';
					?>
					<div class="col-sm-4 text-center">
                        <div class="product-chooser-item <?php echo $selected; ?> position-relative">
                            <img
	                            src="<?php echo DANKER_THEME_URL; ?>/assets/images/form/<?php echo $type; ?>.svg"
	                            class="card-img-top" alt="Create From Template"
                            />
	                        <?php $exp = explode( '_', $type );
	                        $tipe      = $exp[1]; ?>
	                        <input
		                        type="radio" id="select-<?php echo $type; ?>" name="home_type"
		                        value="<?php echo $type; ?>" <?php echo $checked; ?>>
                            <label class="text-center text-muted small p-3 d-block">Home Type <?php echo $tipe; ?> | <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Watch Preview" style="text-decoration: none;" class="front_preview"><i class="fa fa-eye"></i></a></label>

                        </div>
                    </div>
					<?php
				}
				?>
            </div>
        </div>