<?php
/**
 * wpinstant Project
 *
 * @package wpinstant
 * User: dankerizer
 * Date: 26/10/2017 / 00.12
 */

//var_dump($_REQUEST);
$current_user = wp_get_current_user();
if ( ! isset( $_REQUEST['theme_slug'] ) ) {
	$theme_slug = sanitize_title_with_dashes( $_REQUEST['theme_name'] );

} else {
	$theme_slug = $_REQUEST['theme_slug'];
}
$theme_slug = str_replace( ' ', '-', $theme_slug );
/**
 * Versioning
 */
if ( $_REQUEST['child_version'] == null || $_REQUEST['child_version'] == '1' ) {
	$version = WPINSTANT_THEME_VERSION . '0';
} else {
	$version = WPINSTANT_THEME_VERSION . '' . $_REQUEST['child_version'];

}
if ( $version == '1.0.' ) {
	$version = '1.0.0';
}

$data['theme_version'] = $version;
$data['created']       = date( 'c' );
$data['child_version'] = $_REQUEST['child_version'];
if ( $_REQUEST['child_version'] == null ) {
	$data['child_version'] = '1';
}
$data['theme_type']   = $_REQUEST['theme_type'];
$data['theme_author'] = $_REQUEST['theme_author'];
$data['theme_name']   = $_REQUEST['theme_name'];
$data['theme_slug']   = $theme_slug;
$data['theme_desc']   = isset( $_REQUEST['theme_desc'] ) ? $_REQUEST['theme_desc'] : '';
$data['header_type']  = $_REQUEST['header_type'];
$data['footer_type']  = $_REQUEST['footer_type'];
$data['home_type']    = $_REQUEST['home_type'];
$data['single_type']  = $_REQUEST['single_type'];
if ( $_REQUEST['attachment_type'] == 'null' ) {
	$data['attachment_type'] = null;
} else {
	$data['attachment_type'] = $_REQUEST['attachment_type'];
}
//step 5
$data['theme_width']   = $_REQUEST['theme_width'];
$data['sidebar_width'] = $_REQUEST['sidebar_width'];
$data['select_scheme'] = $_REQUEST['select_scheme'];
if ( $_REQUEST['select_scheme'] == 'manual' ) {
	$data['scheme_type'] = 'custom';
} else {
	$data['scheme_type'] = $_REQUEST['scheme_type'];
}
$data['text_color']         = $_REQUEST['text_color'];
$data['header_color']       = $_REQUEST['header_color'];
$data['navbar_menu_color']  = $_REQUEST['navbar_menu_color'];
$data['navbar_type']        = $_REQUEST['navbar_type'];
$data['footer_color']       = $_REQUEST['footer_color'];
$data['link_color']         = $_REQUEST['link_color'];
$data['title_color']        = $_REQUEST['title_color'];
$data['main_bg_color']      = $_REQUEST['main_bg_color'];
$data['main_footer_color']  = $_REQUEST['main_footer_color'];
$data['widget_title_color'] = $_REQUEST['widget_title_color'];
$data['framework']          = $_REQUEST['framework'];
//if ( $_REQUEST['framework'] != 'bootstrap' ) {
//	$data['framework'] = $_REQUEST['framework'];
//}
$data['font']           = ( $_REQUEST['font'] ) ? $_REQUEST['font'] : 'Roboto';
$data['font_base_size'] = ( $_REQUEST['font_base_size'] ) ? $_REQUEST['font_base_size'] : 'medium';
$json                   = json_encode( $data, JSON_PRETTY_PRINT );
if ( current_user_can( 'manage_options' ) ) {
	//var_dump($data);
}
//$theme_slug = ($_REQUEST['theme_slug']) ? $_REQUEST['theme_slug'] : '';
$folder = 'data';
if ( user_is_active() || in_array( 'administrator', (array) $current_user->roles ) ) {
	$user_id   = $current_user->ID;
	$class     = new  WPInstant();
	$user_info = get_userdata( $user_id );
	$user_name = $user_info->user_login;
	$file      = 'data/' . $user_name . '/' . $theme_slug . '.json';
	$gen       = WPINSTANT_API . '/index.php/WPInstant_Generator/index/' . $user_name . '/' . $theme_slug;
	$preview   = WPINSTANT_PREVIEW . '?theme=' . $theme_slug . '&user=' . $user_name;
	if ( isset( $_REQUEST['edit'] ) ) {
		if ( file_exists( $file ) ) {

			$create_json = $class->makeJson( $json, $user_id, $theme_slug, true );
			$createDB    = $class->createDB( $json, $folder, $user_id, true );
			$theme_id    = $class->getThemeid( $theme_slug, $user_id );
			?>
            <section class="section mt-4">
                <div class="card mt-3">
                    <div class="card-header"><?php echo _e( 'Your Theme Information' ); ?> <span
                                class="badge badge-danger">Edited</span></div>
                    <div class="card-body">
                        <h3 class="card-title"><?php echo $_REQUEST['theme_name']; ?>
                            <small class="text-muted"><?php echo $_REQUEST['theme_slug']; ?></small>
                        </h3>
                        <p class="card-text"><?php echo $_REQUEST['theme_desc']; ?></p>
                        <!--                        <ul class="list-unstyled info-theme">-->
                        <!--                            <li><span>Tipe Theme </span> <span>: -->
						<?php //echo $_REQUEST['theme_type']; ?><!--</span></li>-->
                        <!--                            <li><span>Header</span> <span>: -->
						<?php //echo $_REQUEST['header_type']; ?><!--</span></li>-->
                        <!--                            <li><span>Home Page </span> <span>: -->
						<?php //echo $_REQUEST['home_type']; ?><!--</span></li>-->
                        <!--                            <li><span>Single </span> <span>: -->
						<?php //echo $_REQUEST['single_type']; ?><!--</span></li>-->
                        <!--                            <li><span>Attachment </span> <span>: -->
						<?php //echo $_REQUEST['attachment_type']; ?><!--</span></li>-->
                        <!--                            <li><span>Footer </span> <span>: -->
						<?php //echo $_REQUEST['footer_type']; ?><!--</span></li>-->
                        <!--                        </ul>-->
                        <p><a href="<?php echo $gen; ?>" class="btn btn-primary">Download Now</a>
                            <a href="/create?step=2&edit=true&themeid=<?php echo $theme_id; ?>"
                               class="btn btn-secondary btn-outline-secondary">Edit</a>
                            <a href="<?php echo $preview; ?>" target="_blank" class="btn btn-outline-dark">Preview</a>
                        </p>
                        <p class="text-muted">Read how to install WPInstant theme in your website in <a href="/support"
                                                                                                        target="_blank">Support
                                Page</a></p>
                    </div>
                </div>
            </section>
			<?php

		}

	} else {
		if ( file_exists( $file ) ) {
			?>
            <section class="section mt-4">


                <div class="card mt-3 text-center">
                    <div class="card-body">
                        <h4 class="card-title"><?php echo _e( 'Your Theme Information' ); ?> <span
                                    class="badge badge-warning">failed</span></h4>
                        <div class="card-body">
                            <p class="card-text">Theme <strong>"<?php echo $_REQUEST['theme_name']; ?> </strong> Have
                                been create before, please create another theme with different <em>Theme Name</em> dan
                                <em>Theme
                                    Slug</em></p>
                            <a href="/create" class="btn btn-danger"><i class="fa fa-plus"></i> Create New Theme</a>
                            <a href="<?php echo $gen; ?>" target="_blank" class="btn btn-primary"><i
                                        class="fa fa-arrow-down"></i> Download</a>
                            <a href="<?php echo $preview; ?>" target="_blank" class="btn btn-secondary"><i
                                        class="fa fa-external-link"></i> Preview</a>
                            <p class="text-muted">Read how to install WPInstant theme in your website in <a
                                        href="/support" target="_blank">Support Page</a></p>
                        </div>
                    </div>
                </div>
            </section>
			<?php

		} else {
			$create_json = $class->makeJson( $json, $user_id, $theme_slug );
			$createDB    = $class->createDB( $json, $folder, $user_id, false );
			$theme_id    = $class->getThemeid( $theme_slug, $user_id );
			?>
            <section class="section mt-4">
                <div class="card mt-3">
                    <div class="card-header"><?php echo _e( 'Your Theme Information' ); ?> <span
                                class="badge badge-warning">New</span></div>
                    <div class="card-body">
                        <h3 class="card-title"><?php echo $_REQUEST['theme_name']; ?>
                            <small class="text-muted"><?php echo $_REQUEST['theme_slug']; ?></small>
                        </h3>
                        <p class="card-text"><?php echo $_REQUEST['theme_desc']; ?></p>
                        <!--                        <ul class="list-unstyled info-theme">-->
                        <!--                            <li><span>Tipe Theme </span> <span>: -->
						<?php //echo $_REQUEST['theme_type']; ?><!--</span></li>-->
                        <!--                            <li><span>Header</span> <span>: -->
						<?php //echo $_REQUEST['header_type']; ?><!--</span></li>-->
                        <!--                            <li><span>Home Page </span> <span>: -->
						<?php //echo $_REQUEST['home_type']; ?><!--</span></li>-->
                        <!--                            <li><span>Single </span> <span>: -->
						<?php //echo $_REQUEST['single_type']; ?><!--</span></li>-->
                        <!--                            <li><span>Attachment </span> <span>: -->
						<?php //echo $_REQUEST['attachment_type']; ?><!--</span></li>-->
                        <!--                            <li><span>Footer </span> <span>: -->
						<?php //echo $_REQUEST['footer_type']; ?><!--</span></li>-->
                        <!--                        </ul>-->
                        <a href="<?php echo $gen; ?>" target="_blank" class="btn btn-primary"><i class="fa fa-plus"></i>
                            Download Now</a>
                        <a href="/create?step=2&edit=true&themeid=<?php echo $theme_id; ?>"
                           class="btn btn-secondary btn-outline-secondary"><i class="fa fa-pencil-square-o"></i>Edit</a>
                        <a href="<?php echo $preview; ?>" target="_blank" class="btn btn-outline-dark"><i
                                    class="fa fa-external-link"></i> Preview</a>
                        <p class="text-muted">Read how to install WPInstant theme in your website in <a href="/support"
                                                                                                        target="_blank">Support
                                Page</a></p>
                    </div>
                </div>
            </section>

			<?php

		}
	}

} elseif ( in_array( 'subscriber', (array) $current_user->roles ) ) {
	?>
    <section class="section mt-4">


        <div class="card mt-3 text-center">
            <div class="card-body">
                <h4 class="card-title"><?php echo _e( 'Halo, Terimakasih Sudah Mencoba' ); ?></h4>
                <p class="card-text">Untuk dapat mendownload theme <strong>"<?php echo $_REQUEST['theme_name']; ?>
                        "</strong> anda harus upgrade terlebih dahulu.</p>

                <a href="<?php echo WPINSTANT_PRODUCT_URL; ?>" class="btn btn-primary">Upgrade Sekarang</a>
            </div>
        </div>
    </section>
	<?php

} else {
	?>
    <section class="section mt-4">


        <div class="card mt-3 text-center">
            <div class="card-body">
                <h4 class="card-title"><?php echo _e( 'Halo, Terimakasih Sudah Mencoba' ); ?></h4>
                <p class="card-text">Untuk dapat mendownload theme <strong>"<?php echo $_REQUEST['theme_name']; ?>
                        "</strong> anda harus upgrade terlebih dahulu.</p>
                <a href="<?php echo WPINSTANT_PRODUCT_URL; ?>" class="btn btn-primary">Upgrade Sekarang</a>
            </div>
        </div>
    </section>
	<?php

}
?>
