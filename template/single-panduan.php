<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 10/12/2017 / 17.00
 */
?>
<div id="panduan-content">
<?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();

        $post_author = $post->post_author;
		$author_name = get_the_author_meta('display_name',$post_author);
		$user_level = get_the_author_meta('user_level',$post_author);

		$avatar = get_avatar($post_author,60,'//image.flaticon.com/icons/png/128/149/149071.png',$author_name,array('class'=>'mr-3 rounded-circle'));
		?>
        <div id="post-<?php the_ID();?>" <?php post_class('row justify-content-center py-5');?>>
            <div class=" position-relative col-md-7 col-12">
                <header>
                    <h1 class=" post-title entry-title"><?php the_title() ?></h1>
                    <div class="authorbox media py-3">
				        <?php echo $avatar;?>
                        <div class="media-body">
                            <h5 class="mt-0"><?php echo $author_name;?></h5>
                            <p class="text-muted"><?php echo the_date('j M, Y');?></p>
                        </div>
                    </div>
                </header>
            </div>
			<?php if ( has_post_thumbnail() ) : ?>
                <div class="col-12">
                    <figure class="coverimage">
						<?php the_post_thumbnail( 'full', array(
							'class' => 'img-fluid w-100',
							'alt'   => get_the_title() . ''
						) ); ?>

                    </figure>
                </div>
                <div class="clearfix"></div>
			<?php endif; ?>
            <div class=" position-relative col-md-7 col-12">
		        <?php the_content();?>

                <div id="comment">
                    <div class="fb-comments" data-href="<?php the_permalink();?>" data-width="100%" data-numposts="5"></div>
                </div>
            </div>
        </div>

		<?php
	endwhile;
else :
	//get_template_part( 'content', 'none' );
endif;
?>
</div>
