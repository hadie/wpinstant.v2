<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 11/11/2017 / 02.53
 */
$is_aff = wpi_get_user_role(array('administrator', 'adv_affiliate'));
$active = wuoyMemberGetOption( 'general_affiliate_bonus' );
//var_dump($active);
if ( $active ) :
	//return true;




global $current_user, $wpdb;

$product_id = wpi_get_theme_option( 'main_product' );
$user_id    = $current_user->ID;
$Affiliate  = new Affiliate( $product_id, $user_id );
$editor_id = 'data[bonus]';
$settings  = array(
	'wpautop'       => false, // use wpautop?
	'media_buttons' => false, // show insert/upload button(s)
	'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
	'textarea_rows' => get_option( 'default_post_edit_rows', 10 ), // rows="..."
	'tabindex'      => '',
	'editor_css'    => '', //  extra styles for both visual and HTML editors buttons,
	'editor_class'  => '', // add extra class(es) to the editor textarea
	'teeny'         => false, // output the minimal editor config used in Press This
	'dfw'           => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
	'tinymce'       => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
	'quicktags'     => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
);
    $bonus = $Affiliate->getbonus();
if(isset($_POST['data'])){
    $content = $_POST['data']['bonus'];
}else{

    $content = $bonus['bonus'];
}

//var_dump($bonus['bonus']);
?>
    <h1 class="h3">Pengaturan Halaman Bonus</h1>
    <p> Silahkan anda isi editor dibawah dengan instruksi mendapatkan bonus khusus untuk pembeli dari affiliasi
        anda </p>

    <form method="post" action="">
		<?php
		echo ent2ncr( create_none( 'action_simpan_bonus', 'simpan_bonus_nonce' ) );
        if ( ! empty( $bonus ) ) {
			echo '<input type="hidden" name="data[id]" value="' . $bonus['id'] . '">';
		} ?>
        <input type="hidden" name="data[productID]" value="<?php echo $product_id; ?>">
        <input type="hidden" name="data[affiliateID]" value="<?php echo $user_id; ?>">
		<?php

		wp_editor( stripslashes($content), 'bonus', $settings ); ?>
        <p class="text-muted">Anda dapat menggunakan shortcode berikut ini <code>[first_name]</code>, <code>[last_name]</code> dan <code>[full_name]</code></p>

        <p class="text-muted">Shortcode tersebut nantinya akan berubah menjadi nama dari user yang membeli melalui link affiliasi Anda.</p>
        <button name="action" type="submit" value="update"
                class="btn btn-primary mt-4"><?php _e( "Simpan", "wpinstant" ); ?></button>

    </form>
<?php
    if(!empty($bonus)) :?>
        <h4 class="mt-5">Preview</h4>

        <div class="card mt-5">
            <div class="card-header">Bonus dari  <?php echo $current_user->display_name;?></div>
            <div class="card-body">
			    <?php echo wpi_text_bonus($content,true);?>
            </div>
            <div class="card-footer text-muted">
                Note: Hal-Hal yang berkaitan dengan bonus dari Affiliate, di luar tanggung jawab kami (WPInstant)
            </div>
        </div>
   <?php endif; ?>
<?php
global $wpdb;
//$output = array(
//	'status'  => false,
//	'message' => '[wpi_456] Menyimpan Kode Pixel Gagal, Silahkan Ulangi Lagi',
//);

    var_dump(stripslashes($_POST['data']['bonus']));
if ( isset( $_POST['data'] ) && wp_verify_nonce( $_POST['simpan_bonus_nonce'], 'action_simpan_bonus' ) ) {
	$data  = $_POST['data'];
	//update_option( 'wpinstant_options', $_POST['wpinstant_options'] );
	if ( ! empty($data['bonus'] ) ) {

		$table = $wpdb->prefix . "wuoy_affiliate_bonus";

		//$data['affiliateID']	=$data['affiliateID'];



			if ( empty( $bonus ) ) :
				$insert = $wpdb->insert(
					$table,
					array(
						'productID'   => $data['productID'],
						'affiliateID' => get_current_user_id(),
						'bonus'       =>  stripslashes($data['bonus'])
					)
				);
				if($insert):
					$output['status']   = true;
					$output['message'] = 'Simpan Halaman Bonus Berhasil';
				else :
					$output['message'] = '[wpi_602] Simpan Bonus Gagal, Silahkan Ulangi Lagi';
				endif;
			else :
				//var_dump($_POST);
				$insert = $wpdb->update(
					$table,
					array(
						'bonus' => stripslashes($data['bonus'])
					),
					array(
						'id'          => $data['id'],
						'productID'   => $data['productID'],
						'affiliateID' => get_current_user_id()
					)
				);
				if($insert):
					$output['status']   = true;
					$output['message'] = 'update Halaman Bonus Berhasil';
				else :
					$output['message'] = '[wpi_xxxx] update Bonus Gagal, Silahkan Ulangi Lagi';
				endif;
			endif; //endif id

		//var_dump($insert);
	}
	//var_dump($output);
}

endif;
?>