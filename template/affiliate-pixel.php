<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 11/11/2017 / 01.37
 */
$active     	= wuoyMemberGetOption('general_affiliate_fb_pixel');
//var_dump($active);
if ( $active ) :


//echo 'test';
$product_id = wpi_get_theme_option('main_product');
global $current_user,$wpdb;
$user_id = $current_user->ID;
$Affiliate  = new Affiliate( $product_id, $user_id);

$simpan_pixel = $Affiliate->simpan_pixel($_POST);
	$status = $simpan_pixel['status'];
	$message = $simpan_pixel['message'];
$pixel = $Affiliate->getpixel();
$code_pixel = '';
//var_dump($pixel);
if($pixel !=false){
	$code_pixel = $pixel;
}
?>
<?php if(isset($_POST['simpan_pixel_nonce'])){
	if($status){
		$alert = 'success';
	}else{
		$alert = 'danger';
	}
	echo '<div class="alert alert-'.$alert.' alert-dismissible fade show" role="alert">'.$message.'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
}
?>
<h1 class="h2">Gunakan Facebook Pixel</h1>
<form action="" method="post">
	<input type="hidden" name="data[productID]" value="<?php echo $product_id;?>">
	<input type="hidden" name="data[affiliateID]" value="<?php echo $user_id;?>">
	<?php echo ent2ncr( create_none('action_simpan_pixel_code','simpan_pixel_nonce')) ?>
	<div class="form-group">
	<label for="">Kode Pixel Anda</label>
	<input type="text" name="data[code]" class="form-control" value="<?php echo $code_pixel;?>" placeholder="contoh: 1144044718986240">
	</div>
	<button class="btn btn-primary" name="simpan" type="submit">Simpan</button>
</form>


<?php if($pixel !=false):
	$item   = get_post($product_id);

	?>

	<div class="mt-5 mb-5">

		<button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#ConversionTips" aria-expanded="false" aria-controls="ConversionTips">
			Lihat Cara Membuat Custom Conversion
		</button>
	</div>


<article class="mt-5 mb-5 collapse" id="ConversionTips">

	<p>Berikut ini adalah URL Untuk membuat Tracking Conversion. Untuk Panduan Cara Penggunaanya Anda bisa merujuk ke sini
		<a href="https://www.facebook.com/business/help/434245993430255" target="_blank">https://www.facebook.com/business/help/434245993430255</a></p>
	<h5>1.  Initiate Checkout</h5>
	<p class="text-success">URL Equals:</p>
		<p>	<?php echo get_permalink($product_id);?></p>
	<h5>2. Purchase</h5>
	<p class="text-success"> URL Contains : </p>
	<p> <?php echo wuoyMemberGetOption('general_redirect_page','post');?> <code>AND</code> </p>
	<p> product=<?php echo $item->post_name;?> <code>AND</code> </p>
	<p> purpose=invoice</p>
	<h5>3. CompleteRegistration</h5>
	<p class="text-success"> URL Contains : </p>
	<p> <?php echo wuoyMemberGetOption('general_redirect_page','post');?> <code>AND</code> </p>
	<p> product=<?php echo $item->post_name;?> <code>AND</code> </p>
	<p> purpose=complete</p>
</article>

<?php endif;?>
<?php endif; //check if aktif?>

