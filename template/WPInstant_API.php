<?php
/**
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 09/10/2017 / 17.01
 * @todo samakan process/licensing.php
 */

class WPInstant_API  extends WP_REST_Controller{

	//The namespace and version for the REST SERVER
	var $my_namespace = 'wpiapi/v';
	var $my_version   = '1';
	var $db;
	var $db_name;
	var $user_domain;

	public function __construct() {
		global $wpdb;
		$this->db      = $wpdb;
		$this->user_domain = 'wp_instant_user_domain';
		$this->db_name = 'wp_instant_data';
	}

	public function register_routes() {
		$namespace = $this->my_namespace . $this->my_version;
		$base      = 'lisensi';
		$update_base    = 'update';
		register_rest_route( $namespace, '/' . $base, array(
			array(
				'methods'         => WP_REST_Server::ALLMETHODS,
				'callback'        => array( $this, 'jalankanAPI' ),
				'permission_callback'   => array( $this, 'wpinstant_user_permission' )
			),
			array(
				'methods'         => WP_REST_Server::CREATABLE,
				'callback'        => array( $this, 'add_post_to_category' ),
				//'permission_callback'   => array( $this, 'add_post_to_category_permission' )
			)
		)  );

		register_rest_route( $namespace, '/' . $update_base, array(
			array(
				'methods'         => WP_REST_Server::ALLMETHODS,
				'callback' => array($this,'UpdateAPI'),
				'permisson_callbaack'   => array($this,'wpinstant_user_permission')
			)
			)
		);


	}

	/**
	 * untuk update theme
	 */
	public function UpdateAPI(WP_REST_Request $request){

		if ( isset($_POST['themeid'])){
			$email = $_POST['email'];
			//var_dump($_POST);
		    $ube    = get_user_by('email',urldecode_deep($email));
			$theme  = $this->Theme($_POST['themeid'],$ube->ID);

			$theme_name = $theme->theme_name;
			$theme_desc = $theme->theme_desc;
			$theme_type = $theme->theme_type;
			$theme_user = $theme->user_id;
			$user_meta		= get_userdata($theme_user);
			$user_name  = $user_meta->user_login;
			//http://generator.dev/index.php/WPInstant_Generator/index/admin/blog_header_1
           // $gen       = WPINSTANT_API . '/index.php/WPInstant_Generator/index/' . $user_name . '/' . $theme_slug;
                //http://app.wpinstant.co/hasil/index.php/WPInstant_Generator/index/hadie/adada
            $download_url   = WPINSTANT_API.'/index.php/WPInstant_Generator/index/'.$user_name.''.$_POST['themeid'];
            $folder     = 'http:///app.wpinstant.co/data/' . $user_name . '/' . sanitize_title_with_dashes($_POST['themeid']) . '.json';
            $str        = file_get_contents( $folder );
            $json       = json_decode( $str, true );
             $version =    $json['theme_version'];

             $lastupdate =     isset($json['created']) ? $json['created'] : WPINSTANT_THEME_LAST_UPDATE;

			$return = array(
				'name'  => $theme_name,
				'slug'  =>$_REQUEST['themeid'],
				'homepage'  =>'http://wpinstant.co',
				'details_url'  =>'http://app.wpinstant.co',
				'download_url'  =>$download_url,
				'version'       =>$version, //$version
				'last_updated'  => $lastupdate,
				'upgrade_notice'    =>'Time to Update Your "'.$theme_name.'" Theme',
				'author' => $user_name,
				'sections'      => array(
					'description'   =>WPINSTANT_UPDATE_DESC,
					'changelog'   => WPINSTANT_UPDATE_changelog,
				)

			);
//			$return = array(
//				'version' => WPINSTANT_THEME_VERSION,
//				'details_url' =>'https://developer.wordpress.org/reference/functions/add_query_arg/',
//				'download_url' =>$download_url,
//			);
		}else{
			$return = array(

			);
		}

		//return $return + $_REQUEST;
		return $return;


	}

	// Register our REST Server
	public function hook_rest_server(){
		add_action( 'rest_api_init', array( $this, 'register_routes' ) );
	}

	public function wpinstant_user_permission(){
		if ( isset($_REQUEST['key']) && isset($_REQUEST['email']) ){
			$return	= false;
			//ob_start();

			$args	= array(
				'name'				=> strtolower($_REQUEST['key']),
				'post_type'			=> 'wuoylicense',
				'posts_per_page'	=> 1,
				'post_status'		=> 'publish'
			);

			$query	= new WP_Query($args);
			$posts	= $query->posts;
			if(count($posts) > 0){
				$post 		= $posts[0];
				$owner		= get_userdata($post->post_author);
				if($owner->user_email == $_REQUEST['email']){
					$return	= true;
				}

			}

			//ob_end_clean();

			return $return;

			//exit();

		}
	}

	public function jalankanAPI(WP_REST_Request $request){
		if ( isset($_REQUEST['key']) && isset($_REQUEST['email']) && isset($_REQUEST['domain']) ){

			$return	= array(
				'code'	=> 'rest_forbidden',
				'message'	=> 'Sorry, you are not allowed to do that. Please to Try again or contact wpinstant support',
				'data'		=>array(
					'status' => 403,
					'end' => '',
				),
			);
			ob_start();

			$args	= array(
				'name'				=> strtolower($_REQUEST['key']),
				'post_type'			=> 'wuoylicense',
				'posts_per_page'	=> 1,
				'post_status'		=> 'publish'
			);

			$query	= new WP_Query($args);
			$posts	= $query->posts;
			if(count($posts) > 0){
				$post 		= $posts[0];
				$owner		= get_userdata($post->post_author);
				if($owner->user_email == $_REQUEST['email']){


					$time_type                      = $this->get_field('time_type',$post->post_parent);
					$license_type                   = $this->get_field('license_type',$post->post_parent);
					$license_max                    = $this->get_field('license_max',$post->post_parent);
					$sales_form_deactivate_message  = $this->get_field('sales_form_deactivate_message',$post->post_parent);
					$active_domain                  = $this->domain_count($post->post_author);
					//$active_domain                  = 38769;


					if($active_domain >= $license_max){
						//$return = new WP_Error('rest_forbidden','Please Upgrade your package or contact us.',array('status'=>'403','end'=>''));

						$return	= array(
							'code'	=> 'rest_forbidden',
							'message'	=> 'Please Upgrade your package or contact us.',
							'data'		=>array(
								'status' => 400,
								'end' => '',
							),
						);
//
//						$return['result'] = false;
//						$return['message'] = 'Please Upgrade your package or contact us.';
//						$return['end'] = 'end';
					}else{

						$check	= apply_filters('wuoyMember-recurring-check-sales-active',$post->post_parent,$post->post_author);



						$insert = $this->add_domain($_REQUEST['domain'],$post->post_author);
						//$return = new WP_Error( $insert['result'],$insert,array('status'=>'202','end'=>''));
						$return	= array(
							'code'	=> $insert['code'],
							'message'	=> $insert['message'],
							'data'		=>array(
								'status' => 200,
								'end' => $check['end'],
							),
						);
						//
//						$return['result'] = $insert['result'];
//						$return['message'] = $insert['message'];
//						$return['end'] = 'end';


					}


				}

			}
			//$return['message']	= ob_get_contents();
			ob_end_clean();

			return ($return + $_REQUEST);

			exit();

		}else{
			return new WP_Error( 'rest_forbidden', __( 'Sorry, you are not allowed to do that. Please to Try again or contact wpinstant support' ), array( 'status' => 403 ) );
		}

	}

	function get_field($name,$postID = NULL,$type = 'text',$default	= ""){
		global $post;

		$postID	= ( is_null($postID) ) ? get_the_ID() 	: $postID;
		$postID	= ( is_null($postID) ) ? $post->ID 		: $postID;

		$value	= get_post_meta($postID,$name);
		$value	= (is_array($value) && sizeof($value) > 0) ? end($value) : $default;

		switch($type) :

			case "image"	: $value	= "<img src='".$value."' alt='' />"; break;
			case "post"		: $value	= get_permalink($value); break;
			case "attach"	: $value	= wp_get_attachment_url($value); break;
			default			:
			case "text"		: break;

		endswitch;

		return $value;
	}
	public function add_domain($domain,$user_id){
		$date       = date( 'Y-m-d H:i:s' );
		$return = array(
			'code'	=> 'add_domain_problem',
			'message'	=> '',
			'status'	=> '',
		);
		$postchk = $this->db->get_var( "SELECT COUNT(id) FROM $this->user_domain WHERE domain_hash = '" . md5($domain ). "' AND user_id =  '" . $user_id . "';" );
		if ( $postchk > 0 ) {
			$return['code'] = 'domain_listed';
			$return['status'] = 403;
			$return['message'] = 'You have added this domain before';


		}else{
			$savit = $this->db->insert(
				$this->user_domain,
				array(
					'domain_name' => addslashes( $domain ) ,
					'domain_hash' => md5( $domain ),
					'create_date' => $date,
					'update_date' => $date,
					'user_id' => $user_id,
					'status' => 1,
				),
				array(
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%d'
				)
			);
			if ( empty( $savit ) ) {
				if ( defined( 'WP_DEBUG' ) && true === WP_DEBUG ) {
					$this->db->show_errors();
					$this->db->print_error();
				}
				$return['status'] = 403;
				$return['message'] = 'Something Wrong with Your Database, Please try again or contact us';

			} else {
				$return['code'] = 'add_domain_success';
				$return['status'] = 200;
				$return['message'] = 'success';
			}
		}
	return $return;
	}

	public function domain($user_id,$limit = 100){
		$limit = 5;

		$domain = $this->db->get_results( "SELECT * FROM $this->user_domain WHERE user_id = '$user_id' ORDER BY `id` DESC LIMIT 0,$limit");
		return $domain;
	}

	function domain_count($user_id){
		$themes = $this->db->get_results( "SELECT * FROM $this->user_domain WHERE user_id = '$user_id' ");
		return count($themes);
	}


	public function Theme($slug = '',$user_id = 0){
		//$query = "SELECT * FROM $this->db_name WHERE theme_slug = '" . $slug . "' AND user_id =  '" . $user_id . "';";
        $query = "SELECT * FROM $this->db_name WHERE theme_slug = '" . $slug . "' OR theme_name LIKE '" . $slug . "' AND user_id =  '" . $user_id . "';";

        $theme = $this->db->get_row( $query );
		return $theme;
	}
}

$WPInstant_API = new WPInstant_API();
$WPInstant_API->hook_rest_server();