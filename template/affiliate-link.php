<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 16/11/2017 / 14.18
 */
global $current_user;
$user_id = $current_user->ID;
$main_product = wpi_get_theme_option('main_product');

$Affiliate  = new  Affiliate($main_product , $user_id );
$getLink = $Affiliate->getAffLink();

$checkout_page = ($getLink['checkout_page']) ? $getLink['checkout_page'] : '';
$sales_page = ($getLink['sales_page']) ? $getLink['sales_page'] : '';

	$cekLink = $Affiliate->cekLink();
$display = empty($cekLink) ? 'd-none' : '';
//var_dump($getLink);
	?>
<div id="preview"></div>
    <?php if(empty($cekLink)) : ?>
    <form action="<?php echo get_the_permalink(); ?>" method="post" class="form p-5 text-center bg-light" id="createlink">
        <?php wp_nonce_field( 'create_aff_link', 'create_aff_link_filed' ); ?>

        <button class="btn  btn-lg btn-outline-secondary" name="generate_link" type="submit">Dapatkan Link  Affiliasi Anda!

        </button>
    </form>
    <?php endif;?>
    <div id="linkaffiliate" class="<?php echo $display;?>">
            <div  class="row">
                <div class="col-md-2 col-4 text-bold text-md-right">Judul</div>
                <div class="col-auto">Halaman Landing Page</div>
                <div class="w-100"></div>
                <div class="col-md-2 col-4 text-bold text-md-right">Target Halaman</div>
                <div class="col-auto"><a href="http://wpinstant.co" target="_blank">http://wpinstant.co</a></div>
                <div class="w-100 mt-4"></div>
                <div class="col-12">
                    <div class="input-group link-aff mb-3 ">
                        <input type="text" class="form-control" id="sales_page" value="<?php echo $sales_page;?>">
                        <button type="button" id="copyurl" class="btn copy bg-primary text-light input-group-addon" data-clipboard-text="<?php echo $sales_page;?>"> Copy</button>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-2 col-4 strong text-md-right">Judul</div>
                <div class="col-auto">Halaman Form Order</div>
                <div class="w-100"></div>
                <div class="col-md-2 col-4 text-bold text-md-right">Target Halaman</div>
                <div class="col-auto"><a href="<?php echo get_permalink($main_product);?>" target="_blank"><?php echo get_permalink($main_product);?></a></div>
                <div class="w-100 mt-4"></div>
                <div class="col-12">
                    <div class="input-group link-aff mb-3 ">
                        <input type="text" class="form-control" id="checkout_page" value="<?php echo $checkout_page;?>">
                        <button type="button" id="copyurl" class="btn copy bg-primary text-light input-group-addon" data-clipboard-text="<?php echo $checkout_page;?>"> Copy</button>

                    </div>
                </div>
            </div>
    </div>
