<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 28/10/2017 / 10.19
 */
?>
    <h3 class="">Data Pembelian Anda</h3>

<?php
$userID = get_current_user_id();
$args   = array(
	'post_type'      => 'wuoysales',
	'post_status'    => 'any',
	'orderby'        => 'date',
	'order'          => 'DESC',
	'posts_per_page' => - 1,
	'author'         => $userID,
);
$Users  = new Users();
wp_reset_query();
$myposts = get_posts( $args );
//var_dump($myposts);
if ( ! empty( $myposts ) ) {


	echo '<table class="table">';
	echo '
<tr>
<th>Tanggal</th>
<th>Invoice</th>
<th>Produk</th>
<th>Harga</th>
<th>Affiliate</th>

<th>Status</th>

</tr>
';
	foreach ( $myposts as $post ) {
		setup_postdata( $post );
		$date = date( 'j F, Y ', strtotime( $post->post_date ) );;
		$harga        = wuoyMemberCustomField( 'price', $post->ID );
		$affiliate_id = wuoyMemberCustomField( 'affiliate', $post->ID );
		$affiliate    = '';
		if ( $affiliate_id ) {
			$affiliate = get_userdata( $affiliate_id )->display_name;
		}
		$post_status = $post->post_status;
		if ( $post->post_status == 'publish' ) {
			$status = '<span class="badge badge-success">Aktif</span>';
		} else {
			$status = '<span class="badge badge-warning">Meunggu</span> <a href="#" class="badge badge-danger">Konfirmasi</a> ';
		}
		echo '
	<tr>
	<td>' . $date . '</td>
	<td>' . $post->post_title . '</td>
	<td>' . get_the_title( $post->post_parent ) . '</td>
	<td>' . number_format( $harga ) . '</td>
		<td>' . $affiliate . '</td>

	<td>' . $status . '</td>
</tr>
	';
	}
	echo '</table>';
	$sale   = $Users->findSaleHasAffiliate( $post->post_parent );
	$access = $Users->getBonus( wpi_get_theme_option( 'main_product' ), $affiliate_id );
	// if (($access && !empty($affiliate_id)) || current_user_can('manage_options')) {
	if ( ( $access && ! empty( $affiliate_id ) ) ) {
		?>
        <div class="card">
            <div class="card-header bg-dark text-light border-dark">Bonus dari
                <strong><?php echo $affiliate; ?></strong></div>
            <div class="card-body">
				<?php echo wpi_text_bonus( $access['bonus'] ); ?>
            </div>
            <div class="card-footer text-muted">
                Note: Hal-Hal yang berkaitan dengan bonus dari Affiliate adalah <strong>di luar tanggung jawab kami
                    (WPInstant)</strong>
            </div>
        </div>

		<?php
	} else {
		?>

		<?php
	}
	?>

    <div id="bonus" class="card mt-5">
        <div class="card-header bg-dark text-light border-dark"><strong>Official Bonus</strong></div>
        <div class="card-body">
            Berikut ini adalah bonus-bonus dari kami (wpinstant).
			<?php
			$official_bonus = array(
				'evergreen'      => array(
					'title'       => 'Panduan Blogging EverGreen Niche',
					'description' => 'Berikut Ini adalah Panduan Blogging Evergreen Niche. Silahkan Klik Link-link di bawah ini. Untuk Bertanya dan diskusi silahkan klik di link Diskusi atau di Telegram WPInstant Support',
					'link'        => array(
						array(
							'title' => 'Panduan Blogging EverGreen Part-1',
							'url'   => 'http://app.wpinstant.co/panduan-blogging-evergreen-part-1',
						),
						array(
							'title' => 'Panduan Blogging EverGreen Part-2',
							'url'   => 'http://app.wpinstant.co/panduan-blogging-evergreen-part-2-riset',
						),
						array(
							'title' => 'Panduan Blogging EverGreen Part-3 (Content)',
							'url'   => 'http://app.wpinstant.co/panduan-blogging-evergreen-part-3-content-grand-design',
						),
                        array(
							'title' => 'Panduan Blogging EverGreen Part-4 (Structure Grand Design)',
							'url'   => 'http://app.wpinstant.co/panduan-blogging-evergreen-part-4-website-structure-grand-design',
						),
                        array(
							'title' => 'Panduan Blogging EverGreen Part-5 (Link Building)',
							'url'   => 'http://app.wpinstant.co/panduan-blogging-evergreen-part-5-link-building',
						),
                        array(
							'title' => 'Panduan Blogging EverGreen Part-6 (Optimalisasi Income)',
							'url'   => 'http://app.wpinstant.co/panduan-blogging-evergreen-part-6-optimalisasi-income',
						),
                        array(
							'title' => 'Panduan Blogging EverGreen Part-7 (Building The Team)',
							'url'   => 'http://app.wpinstant.co/panduan-blogging-evergreen-part-7-building-the-team',
						),
						array(
							'title' => 'Diskusi',
							'url'   => 'https://t.me/joinchat/BlIUIUlDNchMN1QZbulzig',
						)
					),
					'help'        => '/support',
					'image'       => '//wpinstant.co/wp-content/uploads/2017/12/panduan-blogging-web.png',
					'type'        => 'panduan',
				),
				'instantupload'  => array(
					'title'       => 'Plugin InstantUpload',
					'description' => 'Plugin ini membantu Anda mempermudah dalam membuat postingan khususnya untuk blog dengan type wallpaper (single image). Langsung Upload Ratusan atau Ribuan gambar, pilih kategori, pilih tanggal upload maka postingan Anda bertambah sesuai dengan jumlah gambar.',
					'link'        => 'https://trello-attachments.s3.amazonaws.com/5a05cc4ddb6c0faafc7fefd2/5a08918f1c5460848ec8dafe/fc7fef5697ccaa3e290a3ff722f23f8e/InstantUpload.zip',
					'help'        => '/plugin-instantupload',
					'image'       => DANKER_THEME_ASSETS . 'images/bonus/instantupload.svg',
					'type'        => 'file',
				),
				'instantProCons' => array(
					'title'       => 'Plugin instantProCons',
					'description' => 'Plugin ini terkhusus untuk Anda yang membuat blog Review produk yang biasanya untuk dimonetize dengan Affiliate seperti Amazon, Lazada, Blibli dan lain-lain.',
					'link'        => 'https://trello-attachments.s3.amazonaws.com/5a05cc4ddb6c0faafc7fefd2/5a1bdef5c6174c4fdbaa6ca2/e3a3daa4091f2b1f817aab2315904430/instantproscons.zip',
					'help'        => '/plugin-instantproscons',
					'image'       => DANKER_THEME_ASSETS . 'images/bonus/instantproscons.svg',
					'type'        => 'file',
				),
			);
			foreach ( $official_bonus as $key => $bonus ) {
				?>
                <div href="#" class="media mt-4 mt-2 pb-2" id="<?php echo $key; ?>">
                    <img class="mr-3" src="<?php echo $bonus['image']; ?>" width="60"/>
                    <div class="media-body">
                        <h5><?php echo $bonus['title']; ?></h5>
                        <p> <?php echo $bonus['description']; ?></p>
						<?php if ( $bonus['type'] == 'file' ): ?>
                            <div class="btn-group-sm btn-groups">
                                <a href="<?php echo $bonus['link']; ?>" target="_blank" class="btn btn-primary ">Download
                                    Disini</a>
                                <a href="<?php echo $bonus['help']; ?>" target="_blank"
                                   class="btn btn-outline-secondary ">Baca Panduanya
                                    Disini</a>

                            </div>
						<?php else:
							$links = $bonus['link'];
							echo '<ol>';
							foreach ( $links as $link ) {

								echo '<li><a href="' . $link['url'] . '" target="_blank">' . $link['title'] . '</a> </li>';
							}
							echo '</ol>';
							?>

						<?php endif; ?>
                    </div>
                </div>
                <hr/>
				<?php
			}
			?>

        </div>
    </div>
	<?php
} else {
	echo 'Anda Belum Melakukan Pembelian';
}
wp_reset_query();
?>