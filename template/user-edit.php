<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 10/11/2017 / 22.19
 */
//https://wordpress.stackexchange.com/questions/9775/how-to-edit-a-user-profile-on-the-front-end
/* Get user info. */
global $current_user, $wp_roles;
//get_currentuserinfo(); //deprecated since 3.1

/* Load the registration file. */
//require_once( ABSPATH . WPINC . '/registration.php' ); //deprecated since 3.1
$error = array();
/* If profile was saved, update profile. */
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {
//var_dump($_POST);
	/* Update user password. */
	if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
		if ( $_POST['pass1'] == $_POST['pass2'] )
			wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
		else
			$error[] = __('The passwords you entered do not match.  Your password was not updated.', 'profile');
	}

	/* Update user information. */
	if ( !empty( $_POST['url'] ) )
		wp_update_user( array( 'ID' => $current_user->ID, 'user_url' => esc_url( $_POST['url'] ) ) );
	if ( !empty( $_POST['email'] ) ){
		if (!is_email(esc_attr( $_POST['email'] )))
			$error[] = __('The Email you entered is not valid.  please try again.', 'profile');
		elseif(email_exists(esc_attr( $_POST['email'] )) != $current_user->id )
			$error[] = __('This email is already used by another user.  try a different one.', 'profile');
		else{
			wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
		}
	}

	if ( !empty( $_POST['first-name'] ) )
		update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
	if ( !empty( $_POST['last-name'] ) )
		update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );
	if ( !empty( $_POST['description'] ) )
		update_user_meta( $current_user->ID, 'description', esc_attr( $_POST['description'] ) );
	if ( !empty( $_POST['display_name'] ) )
	wp_update_user( array( 'ID' => $current_user->ID, 'display_name' => esc_attr( $_POST['display_name'] ) ) );
	//update_user_meta( $current_user->ID, 'display_name', esc_attr( $_POST['display_name'] ) );


	/* Redirect so the page will show updated info.*/
	/*I am not Author of this Code- i dont know why but it worked for me after changing below line to if ( count($error) == 0 ){ */
	if ( count($error) == 0 ) {
		$error[] = 'Your profile has been update';
		do_action('edit_user_profile_update', $current_user->ID);
//		wp_redirect( get_permalink() );
//		exit;
	}
}

?>
<?php if ( !is_user_logged_in() ) :
	wp_redirect( WPINSTANT_USER_LOGIN );
	exit;
    ?>

<?php else : ?>
	<?php
    if ( count($error) > 0 ){
	    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">' . implode("<br />", $error) . ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
	}?>
	<form method="post" id="adduser" class="form" action="">
		<div class="row">
            <div class="col">
                <div class="form-username form-group">
                    <label for="first-name" ><?php _e('First Name', 'profile'); ?></label>
                    <input class="text-input form-control" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
                </div><!-- .form-username -->

            </div>
            <div class="col">
                <div class="form-username form-group">
                    <label for="last-name"><?php _e('Last Name', 'profile'); ?></label>
                    <input class="form-control" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
                </div><!-- .form-username -->
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="display_name"><?php _e('Display Name', 'profile'); ?></label>
                    <select name="display_name" id="display_name" class="form-control">
		                <?php
		                $public_display = array();
		                $public_display['display_nickname']  = $current_user->nickname;
		                $public_display['display_username']  = $current_user->user_login;
		                if ( !empty($current_user->first_name) )
			                $public_display['display_firstname'] = $current_user->first_name;
		                if ( !empty($current_user->last_name) )
			                $public_display['display_lastname'] = $current_user->last_name;
		                if ( !empty($current_user->first_name) && !empty($current_user->last_name) ) {
			                $public_display['display_firstlast'] = $current_user->first_name . ' ' . $current_user->last_name;
			                $public_display['display_lastfirst'] = $current_user->last_name . ' ' . $current_user->first_name;
		                }
		                if ( !in_array( $current_user->display_name, $public_display ) ) // Only add this if it isn't duplicated elsewhere
			                $public_display = array( 'display_displayname' => $current_user->display_name ) + $public_display;
		                $public_display = array_map( 'trim', $public_display );
		                $public_display = array_unique( $public_display );
		                foreach ( $public_display as $id => $item ) {
			                ?>
                            <option <?php selected( $current_user->display_name, $item ); ?>><?php echo $item; ?></option>
			                <?php
		                }
		                ?>
                    </select>
                </div>
            </div>
        </div>
		<div class="form-email form-group">
			<label for="email"><?php _e('E-mail *', 'profile'); ?></label>
			<input class="form-control" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
		</div><!-- .form-email -->
		<div class="form-url form-group">
			<label for="url"><?php _e('Website', 'profile'); ?></label>
			<input class="form-control" name="url" type="text" id="url" value="<?php the_author_meta( 'user_url', $current_user->ID ); ?>" />
		</div><!-- .form-url -->
		<div class="form-password form-group">
			<label for="pass1"><?php _e('Password *', 'profile'); ?> </label>
			<input class="form-control" name="pass1" type="password" id="pass1" />
		</div><!-- .form-password -->
		<div class="form-password form-group">
			<label for="pass2"><?php _e('Repeat Password *', 'profile'); ?></label>
			<input class="form-control" name="pass2" type="password" id="pass2" />
		</div><!-- .form-password -->
		<div class="form-textarea form-group">
			<label for="description"><?php _e('Biographical Information', 'profile') ?></label>
			<textarea name="description" class="form-control" id="description" rows="3" cols="50"><?php the_author_meta( 'description', $current_user->ID ); ?></textarea>
		</div><!-- .form-textarea -->

		<?php
		//action hook for plugin and extra fields
		do_action('edit_user_profile',$current_user);
		?>
		<div class="form-submit mt-4">
			<?php echo $referer; ?>
			<input name="updateuser" type="submit" id="updateuser" class="btn btn-primary" value="<?php _e('Update', 'profile'); ?>" />
			<?php wp_nonce_field( 'update-user' ) ?>
			<input name="action" type="hidden" id="action" value="update-user" />
		</div><!-- .form-submit -->
	</form><!-- #adduser -->
<?php endif; ?>

<?php


?>