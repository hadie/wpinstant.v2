<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
//update kupon UPDATE `wp_postmeta` SET `meta_value` = '51' WHERE `wp_postmeta`.`meta_key` = 'discount'
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 16/11/2017 / 02.55
 */
global $current_user,$wpdb;
$user_id = $current_user->ID;
$main_product = wpi_get_theme_option('main_product');
$diskon = 59;
$first_price		= wuoyMemberCustomField("first_price_recurring",$main_product);
$price		= wuoyMemberCustomField("price_recurring",$main_product);
$share	= wuoyMemberCustomField('affiliate_share_firsttime',$main_product);
$share_type	= wuoyMemberCustomField('affiliate_share_type_firsttime',$main_product);
$diskonrp   = $first_price*(59/100);
if($share_type == 'percentage'){
    $commision = (($share/100)*$price);
}else{
    $commision = $share;
}

$sampai     = '2017-12-4';
//$price		= wuoyMemberCustomField("price_recurring",$main_product);
$Affiliate  = new Affiliate($main_product,$user_id);
?>
<h1 class="h3">Kupon Diskon Untuk Affiliate</h1>


<table class="table table-responsive-md">
    <thead class="thead-light">
    <tr>
        <th>Kode Kupon</th>
        <th>Produk</th>
        <th>Aktif Sampai Dengan</th>
        <th>Diskon</th>
        <th>Digunakan</th>
        <th>Status</th>
        <th></th>
    </tr>
    </thead>
    <?php

    $user = ($current_user->ID ==0 ) ?  -1 : $current_user->ID;
$tombol = '';
    $args = array(
        'post_type' => 'wuoycoupon',
        'post_author' => $current_user->ID,
        'post_status' => 'any',
        'author__in' => $current_user->ID
    );

    //$the_query = new WP_Query( $args );

   // var_dump($the_query);

    $coupons = get_posts($args);
    if (!empty($coupons)) {


        foreach ($coupons as $coupon) {
            $id = $coupon->ID;
            $kode = $coupon->post_title;
            $product = $coupon->post_parent;
            $pemilik = $coupon->post_author;
            $active_date = wuoyMemberCustomField('activate_date', $id);
            $discount = wuoyMemberCustomField('discount', $id);
            $discount_type = wuoyMemberCustomField('discount_type', $id);
            $limit = wuoyMemberCustomField("limit", $id);
            $status = $coupon->post_status;
            $usageq = "SELECT COUNT(*) AS TOTAL FROM `wp_postmeta` WHERE `meta_key` = 'coupon' AND `meta_value` = $id";
            $usage  = $Affiliate->cUsage($id);
            //var_dump($usage);

            $class = array();
            if ($pemilik == 0) {
                $class[]= 'table-warning';
            }
            $warna = implode(' ',$class);

            switch ($coupon->post_status){
                case 'draft':
                    $status = '<i class="badge badge-warning">Direview</i>';
                    break;
                case 'pending':
                    $status = '<i class="badge badge-info">Direview</i>';
                    break;
                case 'publish':
                    $status = '<i class="badge badge-success">Aktif</i>';
                    break;
                case 'private':

                $status = '<i class="badge badge-secondary">Non Aktif</i>';
                    break;

            }
            if($limit == 0){
                $limit = '∞';
            }
                echo "<tr class='$warna'>
    <td>$kode</td>
    <td>WPINstant " . get_the_title($product) . "</td>
    <td>".date('l, j F, Y',strtotime($active_date))."</td>
    <td>$discount%</td>
    <td>$usage / <span class='text-muted'>$limit</span></td>
    <td>$status</td>
    <td><a href='#editkupon' class='editkupon' data-kupon='$kode' data-id='$id'>Edit</a></td>
</tr>";
            }

    }else{
        $tombol = '<a href="#" class="btn btn-block btn-danger" id="buatkupon">Buat Kode kupon</a>
';
    }
    ?>
</table>

<?php echo $tombol;?>

<section>
    <h4>Informasi Tambahan</h4>$price
    <p>Harga Produk adalah Rp. <?php echo number_format($first_price);?>, Jadi jika diskon nya adalah 59%, maka user akan mendapatkan potongan sebesar Rp. <?php echo number_format($diskonrp);?>. Sehingga User yang membeli dari link Affiliate Anda harus membayar sebesar Rp. <?php echo number_format( $price-$diskonrp);?> </p>
</section>