<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 28/10/2017 / 10.19
 */


?>
<h3>Data Affiliate Anda (30 Hari Terakhir)</h3>
<?php
global $current_user;
$user_id = $current_user->ID;
$main_product = wpi_get_theme_option('main_product');
$Affiliate  = new  Affiliate($main_product , $user_id );

$Click = $Affiliate->detailClick( $main_product );
$price		= wuoyMemberCustomField("price_recurring",$main_product);
//$price      = $price - ($price * (59/100));
$first_price		= wuoyMemberCustomField("first_price_recurring",$main_product);
$price		= wuoyMemberCustomField("price_recurring",$main_product);
$share	= wuoyMemberCustomField('affiliate_share_firsttime',$main_product);
$share_type	= wuoyMemberCustomField('affiliate_share_type_firsttime',$main_product);
$diskonrp   = $first_price*(59/100);

$setelahDiskon = $first_price - $diskonrp;
if($share_type == 'percentage'){
    $commision = (($share/100)*$setelahDiskon);
}else{
    $commision = $share;
}


$aff_today = number_format( $Click['today'] );
$aff_7days = number_format( $Click['last7'] );
$aff_all    = number_format( $Click['all'] );

//$commision = $price * ($share / 100);

$sale = $Affiliate->SaleCount();
$saletoday = number_format( $sale['today'] );
$sale7days = number_format( $sale['last7days'] );
$salealldays = number_format( $sale['all'] );
$traffics = $Affiliate->traffic();
//print((int)$commision);
//echo "harga asli $first_price <br/>";
//echo "harga  diskon $diskonrp <br/>";
//echo "harga  setelah diskon $setelahDiskon <br/>";
//echo "komisi $commision <br/>";
?>

<div class="row">
    <div class="col-md-4 col-12 mb-3 mb-md-0">
        <div class="stats position-relative">
            <div class="info float-left">
                <span><?php echo $saletoday; ?></span>
                <span class="small text-muted">Hari ini, dari <?php echo number_format( $Click['today'] ); ?> Click</span>
            </div>
            <div class="icon position-absolute bg-success text-light">
                <span class="h1"> <?php echo number_shorten(($commision*$saletoday),0);?></span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-12 mb-3 mb-md-0">
        <div class="stats position-relative">
            <div class="info float-left">
                <span><?php echo $sale7days; ?></span>
                <span class="small text-muted">Seminggu Terakhir, dari <?php echo number_format( $Click['last7'] ); ?> Click</span>
            </div>
            <div class="icon position-absolute bg-success text-light">
                <span class="h1"> <?php echo number_shorten(($commision*$sale7days),0);?></span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-12 mb-3 mb-md-0">
        <div class="stats position-relative">
            <div class="info float-left">
                <span><?php echo $salealldays; ?></span>
                <span class="small text-muted">Sebulan Terakhir, dari <?php echo number_format( $Click['all'] ); ?> Click</span>
            </div>
            <div class="icon position-absolute bg-success text-light">
                <span class="h1"> <?php echo number_shorten(($commision*$salealldays),0);?></span>
            </div>
        </div>
    </div>
</div>
<?php $SalesChart = $Affiliate->SalesChart();?>
<span class="badge  badge-success text-light"> Penjualan</span>
<span class="badge badge-warning text-light"> Belum Konfirmasi</span>
<p>
    Anda Bisa melihat data User yang membeli dari Refferal Anda  <a href="/affiliate/invoice" class="btn-primary btn btn-sm"><i class="fa fa-usd"></i> disini</a>

</p>
<table class="table mt-5 table-responsive-xl table-sm">
    <thead>
    <tr>
        <th>Link Refferer</th>
        <th>Tanggal</th>
        <th>Total Klik</th>
    </tr>
    </thead>


<?php

if(!empty($traffics)){
   // var_dump($traffics);
    foreach ($traffics as $traffic){
        //var_dump($traffic->id);
        $id = $traffic->id;
        $create = $traffic->create_date;
        $link   = $traffic->referer_link;
        $aff    = $traffic->affiliate;
        $count  = $traffic->count;
        $refferal = $link;
        if(empty($link)){
            $refferal = 'Direct';
        }
        echo '<tr>';
        echo '<td>'.$refferal.'</td>';
        echo '<td>'.$create.'</td>';
        echo '<td>'.$count.'</td>';
        echo '</tr>';
    }
}
?>
<table>


