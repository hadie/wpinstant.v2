<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 11/11/2017 / 22.37
 * wuoyMember-sales
 */
global $wuoyMember;
$user     = array();
$allsales = $wuoyMember['sales-recap'];
$sales = $allsales[ wpi_get_theme_option( 'main_product' ) ];
if(current_user_can('manage_options')){
//    echo '<pre>';
    //    var_dump( $sales );
    //    echo '</pre>';
}
?>

<div class="entry-content">
    <h1 class="h3 text-strong">Halaman Leaderboard WPInstant</h1>

    <p>Berikut ini adalah data leaderboard penjualan WPInstant.</p>
    <div class="leaderboard  pt-4 pb-4 pl-2 pr-2">


    <div class="row">
		<?php
		$i     = 1;

		$total = array_sum( $sales );
		arsort( $sales );
		//var_dump($sales);
		foreach ( $sales as $affiliate => $sale ) :
			$key          = 'data_' . $i;
			$user         = get_userdata( $affiliate );
			$avatar       = get_avatar( $user->ID, 50, get_option( 'avatar_default', 'mystery' ), $user->display_name, array( 'class' => 'rounded-circle' ) );
			$penjualan    = $sale;
			$perbandingan = ( $sale / $total ) * 100;
			//var_dump($user);
			if ( in_array($affiliate,array(0,1,83,17))) :
				$output = '';
			else:
                if($i == 1){
			    $bg = 'bg-success';
                }elseif ($i == 2){
	                $bg = 'bg-info';
                }elseif ($i == 3){
	                $bg = 'bg-warning';
                }elseif ($i == 4){
	                $bg = 'bg-danger';
                }elseif ($i == 4){
	                $bg = 'bg-warning';
                }else{
                    $bg = '';
                }

				$output = "

<div class='col align-self-center number'><span class='item-number'></span></div>
<div class='col align-self-center'>$avatar</div>
<div class='col-3 align-self-center'>$user->display_name</div>
<div class='col-7 align-self-center'>
<div class='progress'>
  <div class='progress-bar $bg' role='progressbar' style='width: $perbandingan%' aria-valuenow='$perbandingan' aria-valuemin='0' aria-valuemax='100'></div>
</div>
</div>

<div class='w-100 hr mt-2 mb-2'></div>
";
			endif;
			echo $output;
			if (
				( $i == 10 && ! current_user_can( 'manage_options' ) ) ||
				( $i == 20 && current_user_can( 'manage_options' ) )
			) :
				break;
			endif;
			$i ++;
		endforeach;
		?>
    </div>
    </div>
    <p>Terimakasih atas kerjasama anda, kami berikan apresiasi yang sebesar-besarnya atas kerja keras Anda membantu kami
        memberikan banyak Manfaat kepada blogger Indonesia.</p>
    <p>Terus tingkatkan Performance Anda, kami tunggu Anda di puncak leaderboard</p>
</div>
