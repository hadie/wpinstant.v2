<?php

/**
 *Template Name: Favorite Templates
 * wpinstant.v2 Project
 *
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 03/01/2018 / 09.09
 */
if ( ! is_user_logged_in() ) {
	wp_redirect( WPINSTANT_USER_LOGIN );
	exit;
	//$pagename = 'user-login';
}
global $wpdb, $current_user;
$user_login   = $current_user->user_login;
$display_name = $current_user->display_name;


get_header(); ?>
<main id="main" class="section">
    <div class="create-bar pt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-12">
                    <h2>Choose From Our Favorite Pick</h2>
                </div>
                <div class="col-md-7 col-12 ">
                    <nav aria-label="breadcrumb" class="progress-container" role="navigation">
                        <ul class="step-list breadcrumb float-lg-right float-left">
                            <li class="breadcrumb-item active"><span>1. Choose</span></li>
                            <li class="breadcrumb-item"><span>2. Edit</span></li>
                            <li class="breadcrumb-item"><span>2. Download</span></li>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
			<?php

			$page  = isset( $_GET['hal'] ) ? (int) $_GET['hal'] : 1;
			$limit = 10;
			if ( $page == 1 ) {
				$start = 0;
			} else {
				$start = $limit * (int) $page;
			}
			$Query  = "SELECT * FROM `wp_instant_data` WHERE is_public = 1 LIMIT $start,$limit";
			$public = $wpdb->get_results( $Query, ARRAY_A );
			foreach ( $public as $item ) {
				//var_dump($item);
				$id        = $item['id'];
				$slug      = $item['theme_slug'];
				$name      = $item['theme_name'];
				$desc      = $item['theme_desc'];
				$type      = $item['theme_type'];
				$create    = $item['create_date'];
				$update    = $item['update_date'];
				$user_id   = $item['user_id'];
				$user_info = get_userdata( $user_id );
				$createor  = $user_info->user_login;
				$json_path = getcwd() . '/data/' . $createor . '/' . $slug . '.json';
				$preview   = WPINSTANT_PREVIEW . '?theme=' . $slug . '&user=' . $createor;
				$download  = WPINSTANT_API . '/index.php/WPInstant_Generator/index/' . $createor . '/' . $slug;
				$demo = array(
					'demo1review'    => 'http://demo1.wpinstant.review',
					'demo2wpinstant' => 'http://demo2.wpinstant.review',
					'soreharidemo2'  => 'http://demo3.wpinstant.review',
					'demo4theme'     => 'http://demo4.wpinstant.review',
					'demo5theme'     => 'http://demo5.wpinstant.review',
					'demo6theme'     => 'http://demo6.wpinstant.review',
					'demo7pw'        => 'http://demo7.wpinstant.pw',
					'demo8pw'        => 'http://demo8.wpinstant.pw',
				);

				$link_demo = isset($demo[$slug]) ? $demo[$slug] : '#';
				$image_demo = isset($demo[$slug]) ? $slug.'.jpg' : 'demo1review.jpg';
				//echo 'ini home path '.$json_path;
				$check = get_theme_id($slug,$current_user->ID);

				$db_name = 'wp_instant_data';
				$query = "SELECT * FROM $db_name WHERE id = '" . $id . "'";
				$results = $wpdb->get_row($query,ARRAY_A);

				//var_dump($check);
				echo '
                    <div class="col-6 col-md-4 my-3">
                        <div class="card position-relative">
                            <span class="themetype position-absolute">'.$type.'</span>
                            <img class="card-img-top" src="'.DANKER_THEME_URL.'/assets/images/demo/'.$image_demo.'" alt="">
                            <div class="card-body">
                                <h5 class="card-title">' . $name . '</h5>
                                <h6 class="card-subtitle mb-2 text-muted">by WPInstant</h6>
                                <p class="card-text">' . $desc . '</p>
                                <a href="' . $link_demo . '" target="_blank" class="card-link">Demo</a>
                                <a href="#" data-current-user="'.$user_login.'" data-creator="'.$createor.'" data-id="'.$id.'" data-themename="'.$slug.'" data-download-url="'.WPINSTANT_API . '/index.php/WPInstant_Generator/index/" class="copytheme card-link">Copy</a>
                            </div>
                        </div>
					</div>
					';

			}
			//echo 'ini home path '.getcwd();
			?>
        </div>
    </div>
</main>
<?php get_footer(); ?>
