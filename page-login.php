<?php
/**
 ** Template Name: page-login Template
 * wpinstant.v2 WordPress Theme
 * @package wpinstant.v2 WordPress Theme
 * User: dankerizer
 * Date: 27/10/2017 / 17.46
 */get_header();?>
	<main id="main" class="mt-5">
		<div class="container">
			<div class="card ">
				<!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
				<img id="profile-img" class="profile-img-card" src="<?php echo WPINSTANT_AVATAR;?>" />
				<p id="profile-name" class="profile-name-card"></p>

				<form  id="login" action="login" method="post" class="form-signin">
					<p id="reauth-email" class="reauth-email status"></p>
					<input type="text" name="username" id="username" class="form-control" placeholder="Email address" required autofocus>
					<input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
					<div id="remember" class="checkbox">
						<label>
							<input type="checkbox" name="remember" value="remember-me"> Remember me
						</label>
					</div>
					<button class="btn btn-lg btn-primary btn-block submit_button" type="submit">Sign in</button>
					<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>

				</form><!-- /form -->
				<a href="<?php echo wp_lostpassword_url(); ?>" class="forgot-password">
					Forgot the password?
				</a>
			</div><!-- /card-container -->

		</div>


	</main>
<?php get_footer();?>