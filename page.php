<?php
/**
 * wpinstant.v2 WordPress Theme
 * @package wpinstant.v2 WordPress Theme
 * User: dankerizer
 * Date: 06/11/2017 / 14.57
 */
get_header(); ?>
    <main id="main" class="section" role="main">
        <div class="container mt-2">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					?>
                        <article <?php post_class();?> id="post-<?php the_ID();?>">
                            <header class="">
		                        <?php the_title( '<h1 class="h2">', '</h1>' ); ?>
                            </header>

                            <div class="entry-content">
		                        <?php the_content(); ?>
                            </div>

                        </article>
					<?php
				endwhile;
			else :
				//get_template_part( 'content', 'none' );
			endif;
			?>
        </div>
    </main>
<?php get_footer(); ?>