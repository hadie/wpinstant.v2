<?php
/**
 ** Template Name: Load Templates
 * wpinstant.v2 WordPress Theme
 * @package wpinstant.v2 WordPress Theme
 * User: dankerizer
 * Date: 04/11/2017 / 06.04
 */
get_header(); ?>
    <main id="main" class="section">
        <header class="create-bar pt-3 pb-3">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-md-8 col-12">
                        <h2><?php echo __( 'Pilih Template dari WPInstant', 'wpinstant' ); ?></h2>
                    </div>
                    <div class="col-md-3 col-12">
                        <div class="float-md-right">
                            <div class="form-group">
                                <label for="theme_type">Tipe : </label>
                                <select name="theme_type" id="theme_type" class="custom-select">
		                            <?php
		                            $theme_types = array('blog','attachment','wallpaper');
		                            foreach ($theme_types as $theme_type){
			                            echo '<option value="'.$theme_type.'">'.ucwords($theme_type).'</option>';
		                            }
		                            ?>
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </main>
<?php get_footer(); ?>