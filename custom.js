jQuery(document).ready(function ($) {
    //bagian slider
    var rangeSlider = function () {
        var slider = $('.range-slider'),
            range = $('.range-slider__range'),
            value = $('.range-slider__value');
        tpreview = $('.theme_width_preview');
        monitor_width = tpreview.width();
        cpreview = $('.cpreview');
        tpreview.hide('slow');
        slider.each(function () {

            value.each(function () {
                var value = $(this).prev().attr('value');
                $(this).html(value);

            });

            range.on('input', function () {
                $(this).next(value).html(this.value);
                tpreview.show('slow');
                var ukuran = (this.value / (100 / 50)) - (10 / 100) * monitor_width;
                cpreview.width(ukuran + 'px');
            });

        });
    };

    rangeSlider();

});

jQuery(document).ready(function ($) {

    $('input[type=radio][name=font_base_size]').change(function () {
        $('.font-preview').css('font-size', 16 + 'px');
        if (this.value == 'medium') {
            $('.font-preview').css('font-size', 16 + 'px');
            $('.font-heading-preview').css('font-size', 2.5 + 'rem');
        }
        else if (this.value == 'large') {
            $('.font-preview').css('font-size', 18 + 'px');
            $('.font-heading-preview').css('font-size', 2.6 + 'rem');
        } else {
            $('.font-preview').css('font-size', 14 + 'px');
            $('.font-heading-preview').css('font-size', 2.2 + 'rem');
        }
    });

    $('input[type=radio][name=font_heading_size]').change(function () {
        if (this.value == 'medium') {

        }
        else if (this.value == 'large') {

        } else {

        }
    });


    $('#font').fontselect().change(function () {

        // replace + signs with spaces for css
        var font = $(this).val().replace(/\+/g, ' ');

        // split font into family and weight
        font = font.split(':');

        // set family on paragraphs
        $('.font-preview').css('font-family', font[0]);
    });

    jQuery('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
        $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
        $(this).addClass('selected');
        $(this).find('input[type="radio"]').prop("checked", true);

    });

});

$(document).ready(function () {
    // executes when HTML-Document is loaded and DOM is ready
    //console.log("document is ready");
    $('#TopMenu').on('shown.bs.collapse', function () {

        // $( "<hr/>" ).insertAfter( ".main-navbar" );
        // $( ".main-navbar" ).after( "<p>Test</p>" );
        //$( ".container" ).append( $( "<div class='dropdown-divider'></div>" ) );
        $(".affiliate-nav").appendTo(".collapse");
        $(".affiliate-nav").removeClass("justify-content-center");
        $(".affiliate-nav").addClass("navbar-nav");

    })

    $(".downloadlink").each(function (index) {
        $(this).click(function (e) {
            e.preventDefault();
            var themename = $(this).attr('data-themename');
            var creator = $(this).attr('data-creator');
            var url = $(this).attr('data-download-url');
            var id = $(this).attr('data-id');

            //var download_url = url + '/' + creator + '/' + themename;

            bootbox.prompt({
                title: "Nama Theme (baru)",
                closeButton: false,
                inputType: 'text',

                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: domain_ajax.ajax_url,
                            //dataType: 'json',
                            data: {
                                them_name: result,
                                them_slug: themename,
                                theme_id: id,
                                creator: creator,
                                action: 'copy_theme_request'
                            }
                        })
                            .done(function (response) {
                                bootbox.alert(response);


                            })
                            .fail(function () {
                                bootbox.alert('Something Went Wrog ....');

                            })
                            .always(function () {
                                console.log("complete");
                            });

                        //console.log(result);

                    }
                    
                }
            });


            //$(this).attr('href',download_url);
        });
    });

// $('.navbar-toggler').on('click', function() {
//     $(".affiliate-nav").clone().appendTo(".collapse");
// })

    // $(function() {
    //     $(document).trigger("enhance");
    // });

    // document ready
});

