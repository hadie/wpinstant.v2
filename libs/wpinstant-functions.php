<?php
/**
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 12/09/2017 / 22.54
 */
function get_theme_slug($id, $user_id)
{
    global $wpdb;
    $db_name = 'wp_instant_data';
    $query = "SELECT theme_slug FROM $db_name WHERE id = '" . $id . "' AND user_id =  '" . $user_id . "';";
    $id = $wpdb->get_row($query);
    return $id->theme_slug;

}

function get_theme_id($slug = '', $user_id)
{
    global $wpdb;
    $db_name = 'wp_instant_data';
    $query = "SELECT theme_slug FROM $db_name WHERE theme_slug = '" . $slug . "' AND user_id =  '" . $user_id . "';";
    $id = $wpdb->get_row($query);
    return $id->id;
}

function hidden_domain($string)
{
    //	$len = strlen($str);
    //
    //	return substr($str, 0, 3).str_repeat('*', $len - 2).substr($str, $len - 1, 1);
    //$astNumber = strlen($string) - 1;
    //return substr($string, 0, 3) . str_repeat("*", $astNumber) . substr($string, -4);
    $parts = explode('.', $string);
    $count = count($parts);
    //var_dump($parts);
    if ($count > 2) {
        $repeat = str_repeat('*', max(1, strlen($parts[2]) - 2));
        $output = $parts[0] . '.' . substr($parts[1], 0, min(3, strlen($parts[0]) - 1)) . $repeat . '.' . end($parts);
    } else {
        $repeat = str_repeat('*', max(1, strlen($parts[0]) - 3));
        $output = substr($parts[0], 0, min(3, strlen($parts[0]) - 1)) . $repeat . '.' . end($parts);
    }
    return $output;
}

function hide_phone_number($phone)
{
    //$phone = "09350943256";
    $result = substr($phone, 0, 4);
    $result .= "****";
    $result .= substr($phone, 7, 4);
    return $result;
}

function color_shchema($scheme_type)
{

    $skema = array(
        'scheme_1' => array(
            'header' => '#ffffff',
            'body' => '#f7f7f7',
            'content' => '#ffffff',
            'link' => '#ff922b',
            'footer' => '#000000',
        ),
        'scheme_2' => array(
            'header' => '#000000',
            'body' => '#1f1f1f',
            'content' => '#ffffff',
            'link' => '#ff922b',
            'footer' => '#000000',
        ),
        'scheme_3' => array(
            'header' => '#ffffff',
            'body' => '#ebf6dd',
            'content' => '#ffffff',
            'link' => '#f56991',
            'footer' => '#314713',
        ),
        'scheme_4' => array(
            'header' => '#fafafa',
            'body' => '#c8c8a9',
            'content' => '#ffffff', //fe4365
            'link' => '#fe4365',
            'footer' => '#264537',
        ),
        'scheme_5' => array(
            'header' => '#385da9',
            'body' => '#e9ebee',
            'content' => '#ffffff',
            'link' => '#365899',
            'footer' => '#20345d',
        ),
        'scheme_6' => array(
            'header' => '#eff2e2',
            'body' => '#daddcd', //daddcd
            'content' => '#ffffff',
            'link' => '#fc987a',
            'footer' => '#70685c',
        ),
        'scheme_7' => array(
            'header' => '#ffffff',
            'body' => '#927781',
            'content' => '#ffffff',
            'link' => '#91204d',
            'footer' => '#452632',
        ),
        'scheme_8' => array(
            'header' => '#ffffff',
            'body' => '#a37140',
            'content' => '#ffffff',
            'link' => '#a37140',
            'footer' => '#482808',
        ),
        'scheme_9' => array(
            'header' => '#ffffff',
            'body' => '#609367',
            'content' => '#ffffff',
            'link' => '#39b54a',
            'footer' => '#02400a',
        ),
        'scheme_10' => array(
            'header' => '#ffffff',
            'body' => '#cb4d52',
            'content' => '#ffffff',
            'link' => '#d90d14',
            'footer' => '#630408',
        ),
        'scheme_11' => array(
            'header' => '#f6ece8',
            'body' => '#dab09e',
            'content' => '#faf5f3',
            'link' => '#923339',
            'footer' => '#5c2626',
        ),
        'scheme_12' => array(
            'header' => '#000000',
            'body' => '#252525',
            'content' => '#000000',
            'link' => '#fff200',
            'footer' => '#000000',
        ),
        'scheme_13' => array(
            'header' => '#ffffff',
            'body' => '#effdff',
            'content' => '#ffffff',
            'link' => '#437abb',
            'footer' => '#083869',
        ),
        'scheme_14' => array(
            'header' => '#f8feff',
            'body' => '#ddf0f2',
            'content' => '#ffffff',
            'link' => '#5bc6d0',
            'footer' => '#030423',
        ),
        'scheme_15' => array(
            'header' => '#ffffff',
            'body' => '#e9e5be',
            'content' => '#ffffff',
            'link' => '#9c9346',
            'footer' => '#374021',
        ),
        'scheme_16' => array(
            'header' => '#f7f5f5',
            'body' => '#dad5d2',
            'content' => '#ffffff',
            'link' => '#c4483e',
            'footer' => '#4a4c4f',
        ),
        'scheme_17' => array(
            'header' => '#ffffff',
            'body' => '#e6cfd7',
            'content' => '#ffffff',
            'link' => '#ec6fcf',
            'footer' => '#5c1e4e',
        ),
        'scheme_18' => array(
            'header' => '#6e6e6e',
            'body' => '#797474',
            'content' => '#303030',
            'link' => '#f7941d',
            'footer' => '#4e4d4d',
        ),
        'scheme_19' => array(
            'header' => '#e3e3e3',
            'body' => '#c9c9c9',
            'content' => '#f5f3f3',
            'link' => '#ac5726',
            'footer' => '#59321c',
        ),
        'scheme_20' => array(
            'header' => '#43542a',
            'body' => '#495d52',
            'content' => '#ffffff',
            'link' => '#e4b434',
            'footer' => '#43542a',
        ),
        'scheme_21' => array(
            'header' => '#ffffff',
            'body' => '#f7f7f7',
            'content' => '#ffffff',
            'link' => '#ae0101',
            'footer' => '#ffffff',
        ),
        'scheme_22' => array(
            'header' => '#ffffff',
            'body' => '#ffffff',
            'content' => '#ffffff',
            'link' => '#060606',
            'footer' => '#e2e1e1',
        ),
        'scheme_23' => array(
            'header' => '#f6f6f6',
            'body' => '#ffffff',
            'content' => '#ffffff',
            'link' => '#000000',
            'footer' => '#e2e1e1',
        ),
        'scheme_24' => array(
            'header' => '#f7f9fc',
            'body' => '#ffffff',
            'content' => '#ffffff',
            'link' => '#fc8171',
            'footer' => '#f7f9fc',
        ),
        'scheme_25' => array(
            'header' => '#feeee9',
            'body' => '#ffffff',
            'content' => '#ffffff',
            'link' => '#00000',
            'footer' => '#feeee9',
        ),
    );
    echo ' <div class="row product-chooser justify-content-start">';
    $i = 1;
    foreach ($skema as $scheme => $warna) {
        $checked = ($scheme_type == $scheme) ? 'checked="checked"' : '';
        $selected = (strtolower($scheme_type) == $scheme) ? 'selected' : '';
        ?>
        <div class="col-md-4 ">
            <?php //echo $i; ?>
            <div class="product-chooser-item <?php echo $selected; ?> position-relative">
                <?php
                ?>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 105.833 7.937">
                    <g transform="translate(0 -289.1)">
                        <rect width="21.167" height="7.937" y="289.063" fill="<?php echo $warna['header']; ?>" ry="0"/>
                        <rect width="21.167" height="7.937" x="21.167" y="289.063" fill="<?php echo $warna['body']; ?>"
                              ry="0"/>
                        <rect width="21.167" height="7.937" x="42.333" y="289.063"
                              fill="<?php echo $warna['content']; ?>"
                              ry="0"/>
                        <rect width="21.167" height="7.937" x="63.5" y="289.063" fill="<?php echo $warna['link']; ?>"
                              ry="0"/>
                        <rect width="21.167" height="7.937" x="84.667" y="289.063"
                              fill="<?php echo $warna['footer']; ?>"
                              ry="0"/>
                        <text x="4.14" y="294.335" style="line-height:100%;-inkscape-font-specification:Arial"
                              font-size="3.704" letter-spacing="0" word-spacing="0">
                            <tspan x="4.14" y="294.335">Header</tspan>
                        </text>
                        <text x="27.011" y="293.967" style="line-height:100%;-inkscape-font-specification:'Arial Bold'"
                              font-size="3.704" letter-spacing="0" word-spacing="0">
                            <tspan x="27.011" y="293.967" font-weight="normal">Body</tspan>
                        </text>
                        <text x="45.85" y="294.357" style="line-height:100%;-inkscape-font-specification:'Arial Bold'"
                              font-size="3.704" letter-spacing="0" word-spacing="0">
                            <tspan x="45.85" y="294.357" font-weight="normal">Content</tspan>
                        </text>
                        <text x="70.151" y="294.357" fill="#eee"
                              style="line-height:100%;-inkscape-font-specification:'Arial Bold'" font-size="3.704"
                              letter-spacing="0" word-spacing="0">
                            <tspan x="70.151" y="294.357" font-weight="normal">Link</tspan>
                        </text>
                        <text x="89.323" y="294.335" fill="#999"
                              style="line-height:100%;-inkscape-font-specification:'Arial Bold'" font-size="3.704"
                              letter-spacing="0" word-spacing="0">
                            <tspan x="89.323" y="294.335" font-weight="normal">Footer</tspan>
                        </text>
                    </g>
                </svg>
                <input type="radio" id="select-<?php echo $scheme; ?>" name="scheme_type"
                       value="<?php echo $scheme; ?>" <?php echo $checked; ?>>
            </div>
        </div>
        <?php
        $i++;
    }
    echo '</div>';
}

if (!function_exists('dn_pagination')) {
    function dn_pagination($pages = '', $link = '', $range = 3, $request = 'ke')
    {
        $showitems = ($range * 3) + 1;
        //global $paged; if(empty($paged)) $paged = 1;
        $paged = isset($_GET[$request]) ? $_GET[$request] : 1;
        if ($pages == '') {
            global $wp_query;
            $pages = $wp_query->max_num_pages;
            if (!$pages) {
                $pages = 1;
            }
        }
        if (1 != $pages) {
            echo "<nav aria-label=\"Lihat Halaman Selanjutnya\"><ul class='pagination'>";
            if ($paged > 2 && $paged > $range + 1 && $showitems < $pages) {
                echo "<li  class=\"page-item\"><a class='page-link'  rel='nofollow' href='" . add_query_arg(array('hal' => 1), $link) . "'>&laquo;" . __('Pertama', 'dankedev') . "</a></li>";
            }
            if ($paged > 1 && $showitems < $pages) {
                echo "<li  class=\"page-item\"><a class='inactive page-link' rel='nofollow' href='" . add_query_arg(array('hal' => ($paged - 1)), $link) . "' >&lsaquo; " . __('Sebelumnya', 'dankedev') . "</a></li>";
            }
            for ($i = 1; $i <= $pages; $i++) {
                if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                    echo ($paged == $i) ? "<li class='page-item active'><a class='currenttext page-link'>" . $i . "</a></li>" : "<li class='page-item'><a rel='nofollow' href='" . add_query_arg(array('hal' => $i), $link) . "' class='page-link'>" . $i . "</a></li>";
                }
            }
            if ($paged < $pages && $showitems < $pages) {
                echo "<li  class=\"page-item\"><a rel='nofollow' href='" . add_query_arg(array('hal' => ($paged + 1)), $link) . "' class='inactive page-link'>" . __('Berikutnya', 'dankedev') . " &rsaquo;</a></li>";
            }
            if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages) {
                echo "<li  class=\"page-item\"><a rel='nofollow' class='page-link' href='" . add_query_arg(array('hal' => $pages), $link) . "'>" . __('Terakhir', 'dankedev') . " &raquo;</a></li>";
            }
            echo "</ul>";
            echo "</nav>";
        }
    }
}
function CekAksesProduk($userID = NULL)
{
    global $post;
    $userID = (is_null($userID)) ? get_current_user_id() : $userID;
    $key = 'wuoyMember-access-' . $userID;
    $products = false;
    get_transient($key);
    $sales = array();
    if ($products === false) :
        $args = array(
            'post_type' => 'wuoysales',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'order' => 'ASC',
        );
        if ($userID == NULL) :
            if (!current_user_can('activate_plugins')) :
                $args['author'] = get_current_user_id();
            endif;
        else :
            $args['author'] = $userID;
        endif;
        wp_reset_query();
        $sales = new WP_Query($args);
        if ($sales->have_posts()) :
            while ($sales->have_posts()) :
                $sales->the_post();
                $active = false;
                $products[$post->post_parent]['product'] = get_post($post->post_parent);
                $products[$post->post_parent]['sales'] = $post;
                $price_type = wuoyMemberCustomField('price_type', $post->post_parent);
                $recurring = wuoyMemberCustomField('price_recurring', $post->post_parent);
                if ($price_type == 'recurring' && $recurring) :
                    $end = wuoyMemberCustomField('recurring_end', $post->ID);
                    if (!empty($end) && time() < $end) :
                        $active = true;
                    endif;
                // echo $products[$post->post_parent]['product']->post_title.' '.date("Y-m-d",$end).' '.date("Y-m-d").' active '.$active.' <br />';
                else :
                    $active = true;
                endif;
                $products[$post->post_parent]['active'] = $active;
                wp_reset_query();
            endwhile;
        endif;
    endif;
    return $products;
}

/**
 * @param null $user_id
 * @return mixed
 * @todo belum di cek apakah user ini aktif recuiring nya atau engga
 */
function user_is_active($user_id = NULL, $productID = NULL)
{
    $userID = (is_null($user_id)) ? get_current_user_id() : $user_id;
    $productID = (is_null($user_id)) ? wpi_get_theme_option('main_product') : $productID;
    $args = array(
        'post_type' => 'wuoysales',
        'post_status' => 'publish',
        'post_parent' => $productID,
        'author' => $userID,
        'meta_query' => array(
            array(
                'key' => 'recurring_end',
                'value' => time(),
                'compare' => '>',
                'type' => 'NUMERIC'
            )
        ),
    );
    wp_reset_query();
    $myposts = get_posts($args);
    if (!empty($myposts)) {
        $return = true;

    } else {
        $return = false;
    }
    return $return;
}

function cek_user_is_active($user_id = NULL, $productID = NULL)
{


    $metakey = 'wpinstant_lisense_domain';
    //recurring_end
    $userID = (is_null($user_id)) ? get_current_user_id() : $user_id;
    $productID = (is_null($user_id)) ? wpi_get_theme_option('main_product') : $productID;
    $cek_user_meta = get_user_meta($user_id, $metakey);
    if ($cek_user_meta == false) {
        $output = false;
    } else {
        $output = true;
    }
    return $cek_user_meta;
}

function message_for_in_active()
{
    ?>

    <div class="card mt-3 text-center">
        <div class="card-body">
            <h4 class="card-title"><?php echo _e('Halo, Terimakasih Sudah Mencoba'); ?></h4>
            <p class="card-text">Halaman Ini akan tersedia sesaat setelah anda melakukan aktivasi/pembelian di
                WPInstant</strong>
                anda harus upgrade terlebih dahulu.</p>
            <a href="<?php echo WPINSTANT_PRODUCT_URL; ?>" class="btn btn-primary">Upgrade Sekarang</a>
        </div>
    </div>
    <?php
}

function create_none($action = 'nonce_action', $name = '_my_name_nonce')
{
    return wp_nonce_field($action, $name);
}

function wpi_get_user_role($roles, $user_id = NULL)
{
    if ($user_id) $user = get_userdata($user_id);
    else $user = wp_get_current_user();
    if (empty($user)) return false;
    foreach ($user->roles as $role) {
        if (in_array($role, $roles)) {
            return true;
        }
    }
    return false;
}

function wpi_text_bonus($content = '', $is_affiliate = false)
{
    global $current_user;
    $shortcode = array(
        '[first_name]',
        '[last_name]',
        '[full_name]',
    );
    $replace = array(
        $current_user->first_name, $current_user->last_name, $current_user->display_name
    );
    if ($is_affiliate) {
        $replace = array('Fulan', 'Bin Fulan', 'Fulan Bin Fulan');
    }
    $content = str_replace($shortcode, $replace, $content);
    return stripslashes($content);
}

function number_shorten($number, $precision = 0, $divisors = null)
{

    // Setup default $divisors if not provided
    if (!isset($divisors)) {
        $divisors = array(
            pow(1000, 0) => '', // 1000^0 == 1
            pow(1000, 1) => 'K', // Thousand
            pow(1000, 2) => 'M', // Million
            pow(1000, 3) => 'B', // Billion
            pow(1000, 4) => 'T', // Trillion
            pow(1000, 5) => 'Qa', // Quadrillion
            pow(1000, 6) => 'Qi', // Quintillion
        );
    }
    // Loop through each $divisor and find the
    // lowest amount that matches
    foreach ($divisors as $divisor => $shorthand) {
        if (abs($number) < ($divisor * 1000)) {
            // We found a match!
            break;
        }
    }
    // We found our match, or there were no matches.
    // Either way, use the last defined value for $divisor.
    return number_format($number / $divisor, $precision) . $shorthand;
}