<?php
/**
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 30/08/2017 / 16.11
 */

class Users {

	var $db;
	var $db_name;
	var $user_domain;
	public function __construct() {
		global $wpdb;
		$current_user = wp_get_current_user();
		$this->db      = $wpdb;
		$this->db_name = 'wp_instant_data';
		$this->user_domain = 'wp_instant_user_domain';
		$this->userid   = $current_user->ID;
		$this->display_name    = $current_user->data->display_name;
		$this->user_registered = $current_user->user_registered;
	}

	function themes($page = 1,$return =true){
		$limit = 10;

		$start = ($page>1) ? ($page * $limit) - $limit : 0;

		$query = "SELECT * FROM $this->db_name WHERE user_id = '$this->userid' ORDER BY `wp_instant_data`.`id` DESC LIMIT $start,$limit";
		$themes = $this->db->get_results($query );
		if($return){
			return $themes;
		}else{
			return $query;
		}
	}

	function themecount(){
		$themes = $this->db->get_results( "SELECT * FROM $this->db_name WHERE user_id = '$this->userid' ");
		return count($themes);
	}

	public function user_info(){

		$date            = date_create( $this->user_registered );
			$avatar = get_avatar( $this->userid, 90, WPINSTANT_AVATAR, $this->display_name, array( 'class' => 'media-object img-rounded' ) );
			$member_since = sprintf(__( 'Member since <span class="text-bold">%s</span>', 'wpinstant' ),date_format( $date, 'M Y' ));
			$output = '<div class="media">
			<div class="media-left">
				'.$avatar.'
			</div>
			<div class="media-body">
				<h3 class="media-heading">'.$this->display_name.'</h3>
				<p>'.$member_since.'</p>
			</div>
		</div>';
		return $output;
	}

	public function lisence(){
		$args = array(
			'author'        =>  $this->userid,
			'orderby'       =>  'post_date',
			'order'         =>  'ASC',
			'post_type'        => 'wuoylicense',
			'post_status'      => 'publish',
			'posts_per_page' => 1
		);
		$posts_array = get_posts( $args );
		$lisensei = $posts_array[0]->post_name;
		$lisensei = str_replace('License : ','',$lisensei);
		return strtoupper($lisensei);

	}

	public function get_domain_name_by_id($id){
		$sql = "SELECT `domain_name` FROM `wp_instant_user_domain` WHERE  `id` = $id";
		$domain = $this->db->get_row( $sql );
		return $domain->domain_name;
	}

	public function domain($limit = 100,$sortby = 'id'){
		//$limit = 5;

		$domain = $this->db->get_results( "SELECT * FROM $this->user_domain WHERE user_id = '$this->userid' ORDER BY `$sortby` DESC LIMIT 0,$limit");
		return $domain;
	}

	function domain_count(){
		$themes = $this->db->get_results( "SELECT * FROM $this->user_domain WHERE user_id = '$this->userid' ");
		return count($themes);
	}

	public function lisence_domain_count(){
		$user_meta = get_user_meta( $this->userid,'wpinstant_lisense_domain' );
		return $user_meta[0];
		//return $this->userid;
	}

	public function mycurl($url)
	{
		$url = $url.'/wp-json/cektheme/v1/wpinstant';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, "Googlebot/2.1 (http://www.googlebot.com/bot.html)");
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);

		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($httpCode == 404) {
			return false;
		}else{
			return true;
		}
		curl_close($ch);
		// Gagal ngecURL
//		if(!$site = curl_exec($ch)){
//			return '404';
//		}
//
//		// Sukses ngecURL
//		else{
//
//			return $site;
//		}

	}

	public function cekTHeme($url){
		$curl = $this->mycurl($url);


	}

	function getBonus($productID = NULL,$affiliateID = null)
	{
		//global $wpdb;
		$table =  $this->db->prefix."wuoy_affiliate_bonus";
		$productID 	= (is_null($productID) && isset($_GET['productID']) && !empty($_GET['productID'])) ? $_GET['productID'] : $productID;
		$productID 	= (is_null($productID) && isset($_POST['productID']) && !empty($_POST['productID'])) ? $_POST['productID'] : $productID;

		$affiliateID 	= (is_null($affiliateID)) ? get_current_user_id() : $affiliateID;

		$query 		= "SELECT * FROM ".$table." ".
		                "WHERE productID = '".$productID."' AND affiliateID = '".$affiliateID."' ".
		                "LIMIT 1";

		return $this->db->get_row($query,ARRAY_A);
	}
	function findSaleHasAffiliate($productID)
	{

		$query 	= "SELECT posts.ID as ID, meta_value AS affiliateID FROM ".$this->db->posts." AS posts ".
		            "LEFT JOIN ".$this->db->postmeta." AS postmeta ON posts.ID = postmeta.post_id ".
		            "WHERE posts.post_type = 'wuoysales' ".
		            "AND posts.post_status = 'publish' ".
		            "AND posts.post_parent = '".$productID."' ".
		            "AND posts.post_author = '".$this->userid."' ".
		            "AND postmeta.meta_key = 'affiliate' ".
		            "AND postmeta.meta_value != '' ".
		            "GROUP BY posts.post_parent";

		return $this->db->get_row($query,ARRAY_A);
	}
}