<?php
/**
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 19/07/2017 / 21.43
 */

class WPInstant {

	var $db;
	var $db_name;

	public function __construct() {
		global $wpdb;
		$this->db      = $wpdb;
		$this->db_name = 'wp_instant_data';
	}

	public function makeJson( $json, $user_id, $theme_slug,$edit = false ) {
        $theme_slug = sanitize_title_with_dashes($theme_slug);
		$user_info = get_userdata( $user_id );
		$user_name = $user_info->user_login;
		$folder    = 'data/' . $user_name . '/';
		if ( file_exists( $folder ) && is_dir( $folder ) ) {

		} else {
			$mkdir = mkdir( $folder, 0755 );
		}
		if ( file_exists( $folder . $theme_slug . '.json' ) && $edit == false ) {
			$message = 'Please to make different Slug [E01]';
		} else {
            $message = 'Success';
			if ( json_decode( $json ) != null ) {
				//$data = json_encode($json,JSON_PRETTY_PRINT);
				if($edit){
					file_put_contents($folder . $theme_slug . '.json', $json);
					$file = fopen( $folder . $theme_slug . '.json', 'w+' );
					fwrite( $file, $json );
					fclose( $file );
				}else{
                    $file = fopen( $folder . $theme_slug . '.json', 'w+' );
					fwrite( $file, $json );
					fclose( $file );

				}
                $message = $folder . $theme_slug . '.json';
                $message .= 'harusnya berhasil <br/>';
                $message .= $file.'<br/>';
                //$message .= $json;
                $message .= json_encode(error_get_last());

			} else {
				// user has posted invalid JSON, handle the error
				$message = 'Invalid JSON';
			}

		}

		return $message;
	}

	public function createDB( $json, $folder, $user_id, $edit = false ) {

		$data       = json_decode( $json, true );
		$date       = date( 'Y-m-d H:i:s' );
		$theme_name = $data['theme_name'];
		$theme_slug = $data['theme_slug'];
		$theme_desc = $data['theme_desc'];
		$theme_type = $data['theme_type'];
		$data_path  = $folder;
		$postchk = $this->db->get_var( "SELECT COUNT(id) FROM $this->db_name WHERE theme_slug = '" . $theme_slug . "' AND user_id =  '" . $user_id . "';" );
		//var_dump($postchk);
		if ( $postchk > 0 ) {
			$message = 'Theme Sudah Ada';
			if($edit){
				$value = array(
					'theme_name'=>$theme_name,
					'theme_slug' => $theme_slug,
					'theme_desc' => $theme_desc,
					'theme_type' => $theme_type,
					'update_date' => $date
				);
				$where = array(
					'theme_slug' => $theme_slug,
					'user_id' => $user_id

				);

				$value_format = array('%s','%s','%s','%s','%s');
				$where_format = array('%s','%s');

				$update = $this->db->update($this->db_name,$value,$where,$value_format,$where_format);


				return $update;
			}
		} else {

			$savit = $this->db->insert(
				$this->db_name,
				array(
					'theme_name' => addslashes( $theme_name ) ,
					'theme_slug' =>  addslashes( $theme_slug ),
					'theme_desc' => $theme_desc,
					'theme_type' => $theme_type,
					'data_path' => $folder,
					'create_date' => $date,
					'update_date' => $date,
					'user_id' => $user_id,
					'status' => 1,
				),
				array(
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%d'
				)
			);

//			$savit = $this->db->query( "INSERT INTO $this->db_name,array(),
//		(theme_name,theme_slug, theme_desc,theme_type,data_path,create_date,update_date,user_id,status)
//		VALUES ('" . addslashes( $theme_name ) . "','" . addslashes( $theme_slug ) . "','$theme_desc','$theme_type','$folder','$date','$date',$user_id,1)" );

			if ( empty( $savit ) ) {
				if ( defined( 'WP_DEBUG' ) && true === WP_DEBUG ) {
					$this->db->show_errors();
					$this->db->print_error();
				}

			} else {
				return $savit;
			}
		}
	}

	public function EditDB($json,$folder,$user_id,$theme_slug)
	{
		$data       = json_decode( $json, true );
		$date       = date( 'Y-m-d H:i:s' );
		$theme_name = $data['theme_name'];
		$theme_slug = $data['theme_slug'];
		$theme_desc = $data['theme_desc'];
		$theme_type = $data['theme_type'];
		$data_path  = $folder;


	}
	public function getThemeid($slug = '',$user_id = 0){
		$query = "SELECT id FROM $this->db_name WHERE theme_slug = '" . $slug . "' AND user_id =  '" . $user_id . "';";
		$id = $this->db->get_row( $query );
		return $id->id;
	}

	public function UpdateAnalytic($request){

	}

}