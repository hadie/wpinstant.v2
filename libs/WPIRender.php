<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 24/11/2017 / 16.14
 */

class WPIRender {

    public function __construct()
    {
        add_action('wp_head', array($this, 'render_pixel'), 100);

    }

    function fbpixel( $type = '',$pixel = array())
    {
        $event = '';
        switch ($type) :
            case 'checkout' :
                $event = "fbq('track','InitiateCheckout');";
                break;
            case 'purchase' :
                $sale = wuoyMemberGetSale($_REQUEST['sales']);
                $event = "fbq('track','Purchase',{value: '" . $sale['price'] . "',currency:'" . $sale['currency'] . "'});";
                break;
            case 'complete' :
                $event = "fbq('track','CompleteRegistration');";
                break;
        endswitch;
        $product_id = wpi_get_theme_option('main_product');
        $affid = get_aff_id_by_cokies();
        $affPixel = $this->get_pixel($affid, $product_id);

        if(empty($pixel)){
            $pixel = array();

            $pixel[] = wuoyMemberGetOption('fb_pixel_code'); //mainpixel
            if($affPixel){
                $pixel[] = $this->get_pixel($affid, $product_id);
            }

            $pixel[]     = '733861176752689';
        }


       // ob_start();
        ?>
        <!-- Render Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            <?php
                    foreach ($pixel as $x){
                        echo " fbq('init', '".htmlentities(esc_attr($x))."');";
                    }
                    ?>

            fbq('track', "PageView");
            <?php echo $event; ?>
        </script>
        <?php
        foreach ($pixel as $x){
            echo '<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id='.htmlentities(esc_attr($x)).'&ev=PageView&noscript=1"/></noscript>';
        }
        ?>

        <!-- End Facebook Pixel Code -->
        <?php
//        $content = ob_get_contents();
//        ob_end_clean();
//        echo $content;
    }

    function render_pixel()
    {
        global $post;
        $id = wuoyMemberGetOption('fb_pixel_code');
        $redirect = wuoyMemberGetOption('general_redirect_page');
        if (is_singular('wuoyproduct')) :
             $this->fbpixel('checkout');
        elseif ($post->ID == $redirect && isset($_REQUEST['purpose']) && isset($_REQUEST['sales']) && isset($_REQUEST['product'])) :
                    switch($_REQUEST['purpose']) :
                        case 'invoice'   :  $this->fbpixel('purchase'); break;
                        case 'complete'  :  $this->fbpixel('complete'); break;
                    endswitch;
        else:
             $this->fbpixel('',array($id,'733861176752689'));
        endif;
    }

    public  function get_pixel($affid = null, $productid)
    {
        global $wpdb;
        $return = false;
        $table =  $wpdb->prefix.'wuoy_affiliate_fbcode';
        if ($affid != null) {


            //$affiliate 	= (is_null($affiliate)) ? get_current_user_id() : $affiliate;
            $query = "SELECT code FROM " . $table . " " .
                "WHERE affiliateID='" . $affid . "' AND productID='" . $productid . "' ";
            $result = $wpdb->get_row($query);
            if (is_object($result)) :
                $return = $result->code;
            endif;
        }
        return $return;
    }
}

new WPIRender();


add_action( 'init', 'remove_pixel_from_wuoy');

function remove_pixel_from_wuoy(){
    global $wuoyMemberFbPixel,$wuoyMemberAffiliateFB;
    remove_action('wp_head',array($wuoyMemberFbPixel,'render'));
    remove_action('wp_head',array($wuoyMemberAffiliateFB,'render'));
}