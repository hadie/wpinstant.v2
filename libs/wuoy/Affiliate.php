<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 27/10/2017 / 02.12
 */

class Affiliate {

    var $table = array(
        'statistic' => 'wuoy_link_statistic',
        'detail_stat' => 'wuoy_link_statistic_date',
    );
    private $table_fbcode;
    var $data;

    public function __construct($product_id , $user_id =false)
    {
        global $wpdb, $wuoyMember;
        $this->product_id = $product_id;
        if($user_id  ){
            $this->user_id = $user_id;
        }else{
            $this->user_id = get_current_user_id();
        }

        $this->wpdb = $wpdb;
        $this->table_fbcode = $wpdb->prefix . "wuoy_affiliate_fbcode";
        $this->table_bonus = $wpdb->prefix . "wuoy_affiliate_bonus";
        wp_register_script('moment', $wuoyMember['frameurl'] . 'js/moment.min.js', array('jquery'), null, true);
        wp_register_script('jquery-daterangepicker', $wuoyMember['frameurl'] . 'js/jquery.daterangepicker.js', array(
            'jquery',
            'moment'
        ), null, true);
        wp_register_script('chart', $wuoyMember['frameurl'] . 'js/chart.js', array('jquery'), null, true);
        add_thickbox();
        wp_enqueue_script('jquery-daterangepicker');
        wp_enqueue_script('chart');
        wp_enqueue_style('jquery-daterangepicker', $wuoyMember['frameurl'] . 'css/jquery.daterangepicker.css');
        add_filter('wpi-statistic-query', array($this, 'StaticQuery'), 1, 1);

//        add_action('wp_ajax_nopriv_create_aff_link', array($this,'CreateLink_callback'));
//        add_action('wp_ajax_create_aff_link', array($this,'CreateLink_callback'));
//        add_action( 'wp_ajax_nopriv_CreateLink_action', array( $this, 'create_aff_link_callback' ) );
//        add_action( 'wp_ajax_CreateLink_action', array( $this, 'create_aff_link_callback' ) );
    }

    public function create_aff_link_callback(){
        check_ajax_referer( 'create_aff_link_filed', 'nonce' );

        if( true )
            wp_send_json_success( 'Ajax here!' );
        else
            wp_send_json_error( array( 'error' => 'something wrong' ) );
    }
    public function cekLink()
    {

        $args = array(
            'post_type' => 'wuoyshortlink',
            'author' => $this->user_id,
            'post_parent' => $this->product_id
        );
        $data = get_posts($args);
        //		$query   = "SELECT COUNT(id) FROM wp_posts WHERE post_type = 'wuoyshortlink' AND post_author =  '" . $this->user_id . "' AND post_parrent";
        //		$postchk = $this->wpdb->get_var( $query );
        return $data;

    }

    public function CreateLink()
    {
        //ob_start();
        $output = array();
        $response = 'galgal';
        $slug = $this->randomString();
        $args = array(
            'post_title' => 'Shortlink ' . $slug,
            'post_type' => 'wuoyshortlink',
            'post_name' => $slug,
            'post_author' => $this->user_id,
            'post_parent' => $this->product_id,
            'post_status' => 'publish'
        );
        if (empty($this->cekLink())) {


            $postID = wp_insert_post($args);
            $url = get_permalink($postID);
            $url = (substr($url, -1) == '/') ? substr($url, 0, -1) : $url;
            $output = array(
                    'sales_page' => $url,
                'checkout_page'    => $url.'/form',
            );
            $response = 'berhasil';

        }
        //ob_end_clean();
        echo $response;
    }

    public function getAffLink()
    {
        $sql = "SELECT *  FROM `wp_posts` WHERE `post_author` = $this->user_id AND `post_type` = 'wuoyshortlink'";
        $posts = $this->wpdb->get_results($sql);
        $post = $posts[0];
        $url = $post->guid;
        $url = (substr($url, -1) == '/') ? substr($url, 0, -1) : $url;
        $output = array(
            'sales_page' => $url,
            'checkout_page' => $url . '/form',
        );
        return $output;
    }

    public function SalesChart()
    {
        $groups = array(
            'yearly' => __("Tahunan", "wuoyMember"),
            'monthly' => __("Bulanan", "wuoyMember"),
            'daily' => __("Harian", "wuoyMember")
        );
        $maxdate = date("Y-m-d");
        $mindate = date("Y-m-d", strtotime("-30 day"));
        $range = (isset($_GET['range'])) ? $_GET['range'] : $mindate . "==" . $maxdate;
        $alldata = $this->SalesData();
        //var_dump($alldata);
        extract($alldata);
        ?>

        <input id="dateRange" name="range" class="form-control" type="hidden"
               placeholder="<?php _e("Jangka Waktu", "wuoyMember"); ?>" value="<?php echo $range; ?>"/>
        <div class="chart-container">
            <canvas id="myChart" class="mt-5 mb-5" width="1100" height="350px"></canvas>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#dateRange').dateRangePicker({
                    format: 'YYYY-MM-DD',
                    separator: '==',
                    autoClose: true
                });

                var ctx = jQuery("#myChart").get(0).getContext("2d");
                var data = {
                    labels: [<?php echo implode(",", $label); ?>],
                    datasets: [
                        {
                            label: "Yang telah dibayar",
                            fillColor: "rgba(0,128,0,0.2)",
                            strokeColor: "rgb(0,128,0,1)",
                            pointColor: "rgba(0,128,0,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(0,128,0,1)",
                            data: [<?php echo implode(",", $data['publish']); ?>]
                        },
                        {
                            label: "Menunggu pembayaran",
                            fillColor: "rgba(255, 165, 0,0.2)",
                            strokeColor: "rgba(255, 165, 0,1)",
                            pointColor: "rgba(255, 165, 0,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(255, 165, 0,1)",
                            data: [<?php echo implode(",", $data['unconfirmed']); ?>]
                        }
                    ]
                };

                var myNewChart = new Chart(ctx).Line(data, {
//                    responsive: true,
//                    pointDotRadius: 10,
//                    bezierCurve: false,
                    //scaleShowVerticalLines: false,
                });
            });
        </script>
        <?php
    }

    public function SalesData()
    {

        global $wpdb;
        $chart = array();
        $maxdate = date("Y-m-d", strtotime("1 day"));
        $mindate = date("Y-m-d", strtotime("-30 day"));
        if (isset($_GET['range'])) :
            $date = explode("==", $_GET['range']);
            $mindate = $date[0];
            $maxdate = $date[1];
        endif;
        //		$query	= "SELECT [fields] ".
        //		            "FROM ".$wpdb->posts." AS posts  ".
        //		            "WHERE posts.post_type = 'wuoysales' ".
        //		            "AND posts.post_status = %s ".
        //		            "AND posts.post_author = ".$this->user_id." ".
        //		            "[affiliate] ".
        //		            "AND posts.post_date <= %s ".
        //		            "AND posts.post_date >= %s ".
        //		            "AND posts.post_parent = '".$this->product_id."' ".
        //		            "GROUP [group]";
        $query = "SELECT [fields] " .
            "FROM " . $wpdb->posts . " AS posts [affiliate-group] " .
            "WHERE posts.post_type = 'wuoysales' " .
            "AND posts.post_status = %s " .
            "[affiliate] " .
            "AND posts.post_date <= %s " .
            "AND posts.post_date >= %s " .
            "[product] " .
            "";
        //echo $query;
        // ====================================================
        // PRODUCT FILTER
        // ====================================================
        $query = str_replace("[product]", "AND posts.post_parent = '" . $this->product_id . "' ", $query);
        // ====================================================
        // AFFILIATE FILTER
        // ====================================================
        $query = str_replace("[affiliate-group]", "INNER JOIN " . $wpdb->postmeta . " AS postmeta ON posts.ID = postmeta.post_id ", $query);
        $query = str_replace("[affiliate]", "AND postmeta.meta_key = 'affiliate' AND postmeta.meta_value = '" . $this->user_id . "' ", $query);
        // ====================================================
        // CUSTOM FIELDS
        // ====================================================
        if (isset($default['fields'])) :
            $query = str_replace("[fields]", $default['fields'], $query);
            $query = str_replace("[group]", "", $query);
            $query = str_replace("GROUP", "", $query);
        else :
            // ====================================================
            // DAILY
            // ====================================================
            if (!isset($_GET['group']) || $_GET['group'] == 'daily') :
                $query = str_replace("[fields]", "YEAR(posts.post_date) AS `YEAR`, MONTH(posts.post_date) AS `MONTH`,DAY(posts.post_date) AS `DAY`, COUNT(*) AS `TOTAL`", $query);
                $query = str_replace("[group]", "BY EXTRACT(DAY FROM posts.post_date)", $query);

            // ====================================================
            // MONTHLY
            // ====================================================
            elseif ($_GET['group'] == 'monthly') :
                $query = str_replace("[fields]", "YEAR(posts.post_date) AS `YEAR`, MONTH(posts.post_date) AS `MONTH`, COUNT(*) AS `TOTAL`", $query);
                $query = str_replace("[group]", "BY YEAR(posts.post_date), MONTH(posts.post_date)", $query);

            // ====================================================
            // YEARLY
            // ====================================================
            elseif ($_GET['group'] == 'yearly') :
                $query = str_replace("[fields]", "YEAR(posts.post_date) AS `YEAR`, COUNT(*) AS `TOTAL`", $query);
                $query = str_replace("[group]", "BY YEAR(posts.post_date)", $query);
            endif;
        endif;
        $publish_query = $wpdb->prepare($query, 'publish', $maxdate, $mindate);
        $draft_query = $wpdb->prepare($query, 'draft', $maxdate, $mindate);
        //echo $publish_query;
        $chart['publish'] = $this->arrangeSalesData($wpdb->get_results($publish_query, ARRAY_A));
        $chart['unconfirmed'] = $this->arrangeSalesData($wpdb->get_results($draft_query, ARRAY_A));
        $data = array(
            "publish" => array(),
            "unconfirmed" => array()
        );
        $label = array();
        $value = array();
        $date = strtotime($mindate);
        $maxdate = strtotime($maxdate);
        // ====================================================
        // DAILY
        // ====================================================
        if (!isset($_GET['group']) || $_GET['group'] == 'daily') :
            while ($date <= $maxdate) :
                $key = date("Y-m-j", $date);
                $ddate = explode("-", $key);
                $year = $ddate[0];
                $month = $ddate[1];
                $day = $ddate[2];
                $label[$key] = '"' . date("d/m/y", $date) . '"';
                $data['publish'][$key] = (isset($chart['publish'][$key])) ? $chart['publish'][$key] : 0;
                $data['unconfirmed'][$key] = (isset($chart['unconfirmed'][$key])) ? $chart['unconfirmed'][$key] : 0;
                $date = $date + DAY_IN_SECONDS;
            endwhile;

        // ====================================================
        // MONTHLY
        // ====================================================
        elseif ($_GET['group'] == 'monthly') :
            while ($date <= $maxdate) :
                $ddate = explode("-", date("Y-m-j", $date));
                $year = $ddate[0];
                $month = $ddate[1];
                $key = $year . '-' . $month;
                $label[$key] = '"' . $key . '"';
                $data['publish'][$key] = (isset($chart['publish'][$key])) ? $chart['publish'][$key] : 0;
                $data['unconfirmed'][$key] = (isset($chart['unconfirmed'][$key])) ? $chart['unconfirmed'][$key] : 0;
                $date = date("Y-m-j", strtotime("+1 month", strtotime($date)));
            endwhile;

        // ====================================================
        // MONTHLY
        // ====================================================
        elseif ($_GET['group'] == 'yearly') :
            while ($date <= $maxdate) :
                $ddate = explode("-", date("Y-m-j", $date));
                $year = $ddate[0];
                $key = $year;
                $label[$key] = '"' . $key . '"';
                $data['publish'][$key] = (isset($chart['publish'][$key])) ? $chart['publish'][$key] : 0;
                $data['unconfirmed'][$key] = (isset($chart['unconfirmed'][$key])) ? $chart['unconfirmed'][$key] : 0;
                $date = date("Y-m-j", strtotime("+1 year", strtotime($date)));
            endwhile;
        endif;
        //echo $publish_query;
        return array(
            'data' => $data,
            'label' => $label,
            'value' => $value
        );

    }

    public function SaleCount()
    {
        global $wpdb;
        $results = array();
        $maxdate = date("Y-m-d", strtotime("1 day"));
        $seminggu = date("Y-m-d", strtotime("-7 day"));
        $sebulan = date("Y-m-d", strtotime("-30 day"));
        $query = "SELECT [fields] " .
            "FROM " . $wpdb->posts . " AS posts [affiliate-group] " .
            "WHERE posts.post_type = 'wuoysales' " .
            "AND posts.post_status = %s " .
            "[affiliate] " .
            "[mindate]" .
            "[maxdate]" .
            "[product] " .
            "";

        $query = str_replace("[affiliate-group]", "INNER JOIN " . $wpdb->postmeta . " AS postmeta ON posts.ID = postmeta.post_id ", $query);
        $query = str_replace("[affiliate]", "AND postmeta.meta_key = 'affiliate' AND postmeta.meta_value = '" . $this->user_id . "' ", $query);
        //$query = str_replace("[affiliate]", "AND postmeta.meta_key = 'affiliate' AND postmeta.meta_value = '2' ", $query);
        // $query = str_replace("[affiliate]", "", $query);
        //$query = str_replace("[fields]", "YEAR(posts.post_date) AS `YEAR`, MONTH(posts.post_date) AS `MONTH`,DAY(posts.post_date) AS `DAY`, COUNT(*) AS `TOTAL`", $query);
        $query = str_replace("[fields]", " COUNT(*) AS TOTAL", $query);
        $query = str_replace("[product]", "AND posts.post_parent = '" . $this->product_id . "' ", $query);

        $allquery = str_replace('[mindate]',"",$query);
        $allquery = str_replace('[maxdate]',"",$allquery);

        $query = str_replace('[mindate]',"AND posts.post_date <= %s",$query);
        $query = str_replace('[maxdate]',"AND posts.post_date >= %s",$query);
        //today
        $today = $wpdb->prepare($query, 'publish', $maxdate, $maxdate);
        $draft_query = $wpdb->prepare($query, 'draft', $maxdate, $maxdate);

        //7 days
        $last7days = $wpdb->prepare($query, 'publish', $maxdate, $seminggu);
        $draft_query = $wpdb->prepare($query, 'draft', $maxdate, $seminggu);


        $all = $wpdb->prepare($allquery, 'publish');
        $results = array(
          'today' => $this->displaycount($wpdb->get_results($today, ARRAY_A)),
          'last7days' => $this->displaycount($wpdb->get_results($last7days, ARRAY_A)),
          'all' => $this->displaycount($wpdb->get_results($all, ARRAY_A)),

        );

        return $results;
        //return $all;
    }

    static function displaycount($array)
    {
        foreach ($array as $sale) {
            return $sale['TOTAL'];
        }
    }

    function detailClick($product)
    {
        global $wpdb;
        //        $query = "SELECT * FROM " . $wpdb->prefix . $this->table['statistic'] . " " .
        //            "WHERE product = '" . $product . "' [affiliate] " .
        //            "ORDER BY `count` DESC ";
        //        $query	= str_replace("[affiliate]","AND affiliate = '".get_current_user_id()."'",$query);
        //        echo $query;
        $key = 'wuoyMember-statistic-' . $product;
        $key = $key . '-' . $this->user_id;
        $stats = get_transient($key);
        if ($stats === false) :
            // ALL
            $query = "SELECT * FROM " . $wpdb->prefix . $this->table['statistic'] . " " .
                "WHERE product = '" . $product . "' [affiliate] " .
                "ORDER BY `count` DESC ";
            $query = str_replace("[affiliate]", "AND affiliate = '" . get_current_user_id() . "'", $query);
            $all = $wpdb->get_results($query, ARRAY_A);
            $totall = 0;
            if ($all && count($all) > 0) :
                foreach ($all as $alldata) :
                    $totall += $alldata['count'];
                endforeach;
            endif;
            // LAST 7 DAYS
            $query = "SELECT detail_stat.count FROM " . $wpdb->prefix . $this->table['statistic'] . " AS statistic " .
                "INNER JOIN " . $wpdb->prefix . $this->table['detail_stat'] . " AS detail_stat " .
                "ON statistic.id = detail_stat.statisticID " .
                "WHERE " .
                "product = '" . $product . "' AND " .
                "( date BETWEEN '" . date("Y-m-d", strtotime("-7 day")) . "' AND '" . date("Y-m-d", strtotime("-1 day")) . "' ) " .
                "[affiliate] " .
                "ORDER BY statistic.count DESC ";
            $query = str_replace("[affiliate]", "AND affiliate = '" . get_current_user_id() . "'", $query);
            $last7 = $wpdb->get_results($query, ARRAY_A);
            $totls7 = 0;
            if ($last7 && count($last7) > 0) :
                foreach ($last7 as $last7data) :
                    $totls7 += $last7data['count'];
                endforeach;
            endif;
            // TODAY
            $query = "SELECT detail_stat.count FROM " . $wpdb->prefix . $this->table['statistic'] . " AS statistic " .
                "INNER JOIN " . $wpdb->prefix . $this->table['detail_stat'] . " AS detail_stat " .
                "ON statistic.id = detail_stat.statisticID " .
                "WHERE " .
                "product = '" . $product . "' AND " .
                "date = '" . date("Y-m-d") . "' " .
                "[affiliate] " .
                "ORDER BY statistic.count DESC ";
            $query = str_replace("[affiliate]", "AND affiliate = '" . get_current_user_id() . "'", $query);
            $today = $wpdb->get_results($query, ARRAY_A);
            $tottod = 0;
            if ($today && count($today) > 0) :
                foreach ($today as $todaydata) :
                    $tottod += $todaydata['count'];
                endforeach;
            endif;
            $stats = array(
                'general' => $all,
                'all' => $totall,
                'last7' => $totls7,
                'today' => $tottod,
            );
            set_transient($key, $stats, 60 * 60 * 3);
        endif;
        return $stats;

    }

    public static function StaticQuery($query)
    {
        //        if(!current_user_can('manage_options')) :
        //            $query	= str_replace("[affiliate]","AND affiliate = '".get_current_user_id()."'",$query);
        //        else :
        //            $query	= str_replace("[affiliate]","",$query);
        //        endif;
        //
        $query = str_replace("[affiliate]", "AND affiliate = '" . get_current_user_id() . "'", $query);
        return $query;
    }

    // ============================================================
    // ARRANGE SALES DATA
    // ============================================================
    function arrangeSalesData($data)
    {
        $result = array();
        if (!isset($_GET['group']) || $_GET['group'] == 'daily') :
            foreach ($data as $dat) :
                $key = $dat['YEAR'] . '-' . sprintf("%02s", $dat['MONTH']) . '-' . $dat['DAY'];
                $result[$key] = $dat['TOTAL'];
            endforeach;
        elseif ($_GET['group'] == 'monthly') :
            foreach ($data as $dat) :
                $key = $dat['YEAR'] . '-' . sprintf("%02s", $dat['MONTH']);
                $result[$key] = $dat['TOTAL'];
            endforeach;
        elseif ($_GET['group'] == 'yearly') :
            foreach ($data as $dat) :
                $key = $dat['YEAR'];
                $result[$key] = $dat['TOTAL'];
            endforeach;
        endif;
        return $result;
    }

    function randomString($low = 2, $up = 3, $num = 2)
    {
        $character_set_array = array();
        $character_set_array[] = array('count' => $low, 'characters' => 'abcdefghijkmnpqrstuvwxyz');
        $character_set_array[] = array('count' => $up, 'characters' => 'ABCDEFGHJKLMNPQRSTUVWXYZ');
        $character_set_array[] = array('count' => $num, 'characters' => '123456789');
        $temp_array = array();
        foreach ($character_set_array as $character_set) :
            for ($i = 0; $i < $character_set['count']; $i++) :
                $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
            endfor;
        endforeach;
        shuffle($temp_array);
        return implode('', $temp_array);
    }

    public function simpan_pixel($POST)
    {
        global $wpdb;
        $output = array(
            'status' => false,
            'message' => '[wpi_456] Menyimpan Kode Pixel Gagal, Silahkan Ulangi Lagi',
        );
        if (isset($POST['data']) && wp_verify_nonce($POST['simpan_pixel_nonce'], 'action_simpan_pixel_code')) {
            $data = $POST['data'];
            //$data['affiliateID']	=$data['affiliateID'];
            $code = $this->getpixel();
            if (!empty($code)) :
                $insert = $wpdb->update(
                    $this->table_fbcode,
                    $data,
                    array(
                        'affiliateID' => $data['affiliateID'],
                        'productID' => $data['productID'],
                    )
                );
                if ($insert):
                    $output['status'] = true;
                    $output['message'] = 'Update Kode Pixel Berhasil';
                else :
                    $output['message'] = '[wpi_602] Update Kode Pixel Gagal, Silahkan Ulangi Lagi atau pastikan kode pixel anda berbeda';
                endif;
            else :
                $insert = $wpdb->insert(
                    $this->table_fbcode,
                    $data,
                    array('%s', '%s', '%s')
                );
                if ($insert):
                    $output['status'] = true;
                    $output['message'] = 'Menyimpan Kode Pixel Berhasil';
                else :
                    $output['message'] = 'Menyimpan Kode Pixel Gagal, Silahkan Ulangi Lagi';
                endif;
            endif;

            //exit();
        }
        return $output;
    }

    public function getpixel()
    {
        global $wpdb;
        $return = false;
        //$affiliate 	= (is_null($affiliate)) ? get_current_user_id() : $affiliate;
        $query = "SELECT code FROM " . $this->table_fbcode . " " .
            "WHERE affiliateID='" . $this->user_id . "' AND productID='" . $this->product_id . "' ";
        $result = $wpdb->get_row($query);
        if (is_object($result)) :
            $return = $result->code;
        endif;
        return $return;
    }

    public function getbonus()
    {
        $query = "SELECT * FROM " . $this->table_bonus . " " .
            "WHERE productID = '" . $this->product_id . "' AND affiliateID = '" . $this->user_id . "' " .
            "LIMIT 1";
        return $this->wpdb->get_row($query, ARRAY_A);
    }

    public function traffic($order = 'create_date')
    {
        $query = "SELECT * FROM " . $this->wpdb->prefix . $this->table['statistic'] . " " .
            "WHERE product = '" . $this->product_id . "' [affiliate] " .
            "ORDER BY " . $order . " DESC LIMIT 100";
        $query = str_replace('[affiliate]', " AND affiliate = " . get_current_user_id(), $query);
        //$query = str_replace('[affiliate]',"",$query);
        $all = $this->wpdb->get_results($query);
        return $all;
    }

    public function cUsage($cid){
        $usageq = "SELECT COUNT(*) AS TOTAL FROM `wp_postmeta` WHERE `meta_key` = 'coupon' AND `meta_value` = $cid";
        $usage  = $this->wpdb->get_results($usageq,ARRAY_A);
        return $this->displaycount($usage);
    }
}
new Affiliate('','');