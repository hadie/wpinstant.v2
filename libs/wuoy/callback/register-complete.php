<?php

/**
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 17/10/2017 / 20.50
 */

class RegisterComplete {

	//var $data  ;
	var $metakey;
	//var $productid;
	var $user_id;

	public function __construct() {
		require( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );


		$this->data = $_POST;

//		$this->data = array(
//			'user' => array(
//				'ID' => 9,//user id
//				'user_login' => 'userbejo',
//				'user_nicename' => 'User Bejo',
//				'user-registered' => '2017-10-28 10:23:00',
//				'first_name' => 'user',
//				'last_name' => 'bejo',
//				'email' => 'hadie.87@gmail.com',
//				'display_name' => 'userbejo',
//				'phone' => '+628542546',
//			),
//			'sale' => array(
//				'ID' => 146, //invoice id
//				'invoice' => 'WPI0146',
//				'product_ID' => 140,
//				'product' => 'Produk Test 1 Hari',
//				'transaction' => 'manual',
//				'price' => '400000',
//			)
//		);
		$this->metakey        = 'wpinstant_lisense_domain';
		//$this->lisence_domain = $this->license_max;
		$this->productid            = $this->data['sale']['product_ID'];
		$this->user    = $this->data['user'];
		$this->user_id = $this->data['user']['ID'];
		$this->lisensi = $this->generateLicense( $this->productid );
		//$this->data = $data;

	}

	public function input_data() {

		//var_dump($this->data);
		if ( !empty($_POST) ) {

	//var_dump($this->data['user']['ID']);
			$cek_user_meta = get_user_meta( $this->user_id,$this->metakey );
			$license_max   = $this->get_field( 'license_max', $this->productid );
			echo $license_max;

			$args = array(
				'post_type'      => 'wuoylicense',
				'posts_per_page' => 1,
				'author'        => $this->user_id,
				'post_status'    => 'publish',

			);
			$query = new WP_Query( $args );
			$posts = $query->posts;


			if ( count( $posts ) > 0 ) {
				//echo 'ada';
				$get_parrent = wp_get_post_parent_id($this->productid);
				if(!empty($get_parrent)){
					//var_dump($get_parrent);
					$max_license_parrent =  $this->get_field( 'license_max', $get_parrent );
					//echo $max_license_parrent;
					$tambah_lisensi = $cek_user_meta[0] + $license_max;
					add_user_meta( $this->user_id, $this->metakey, $tambah_lisensi, false );
				}else{
					echo 'nill';
				}

			} else {
				//var_dump($posts);
				$create = $this->create_lisense($this->user_id,$this->productid,$this->generateLicense( $this->productid ));
				if ( empty( $cek_user_meta ) ) {
					add_user_meta( $this->user_id, $this->metakey, $license_max, false );
				} else {

					update_user_meta( $this->user_id, $this->metakey, $license_max, $cek_user_meta[0] );
				}
			}
		}


	}

	public function create_lisense($userid = 0,$productid = 0,$lisensi = '') {

		$post  = array(
			'post_type'   => 'wuoylicense',
			'post_status' => 'publish',
			'post_parent' => $productid,
			'post_name'   => $lisensi,
			'post_title'  => 'License : ' .$lisensi,
			'post_author' => $userid
		);
		$input = wp_insert_post( $post );

		return $input;
	}

	public function generateLicense( $id ) {
		return $id . '-WPI-' . $this->randomString( 0, 5, 0 ) . '-' . $this->randomString( 0, 0, 5 ) . '-' . $this->randomString( 0, 5, 1 );
	}

	public function randomString( $low = 2, $up = 3, $num = 2 ) {
		$character_set_array   = array();
		$character_set_array[] = array( 'count' => $low, 'characters' => 'abcdefghijkmnpqrstuvwxyz' );
		$character_set_array[] = array( 'count' => $up, 'characters' => 'ABCDEFGHJKLMNPQRSTUVWXYZ' );
		$character_set_array[] = array( 'count' => $num, 'characters' => '123456789' );
		$temp_array            = array();
		foreach ( $character_set_array as $character_set ) :
			for ( $i = 0; $i < $character_set['count']; $i ++ ) :
				$temp_array[] = $character_set['characters'][ rand( 0, strlen( $character_set['characters'] ) - 1 ) ];
			endfor;
		endforeach;
		shuffle( $temp_array );

		return implode( '', $temp_array );
	}

	public function get_field( $name, $postID = null, $type = 'text', $default = "" ) {
		global $post;
		$postID = ( is_null( $postID ) ) ? get_the_ID() : $postID;
		$postID = ( is_null( $postID ) ) ? $post->ID : $postID;
		$value  = get_post_meta( $postID, $name );
		$value  = ( is_array( $value ) && sizeof( $value ) > 0 ) ? end( $value ) : $default;
		switch ( $type ) :
			case "image"    :
				$value = "<img src='" . $value . "' alt='' />";
				break;
			case "post"        :
				$value = get_permalink( $value );
				break;
			case "attach"    :
				$value = wp_get_attachment_url( $value );
				break;
			default            :
			case "text"        :
				break;
		endswitch;

		return $value;
	}
}

$complete = new RegisterComplete();
$complete->input_data();
