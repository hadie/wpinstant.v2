<?php
/**
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 13/10/2017 / 03.02
 */
global $Fronts, $wuoyMemberFront;

class Fronts extends wuoyMemberSettingLayout {

    var $variable = array();
    private $pages = array();
    private $path;
    private $url;
    private $render = false;

    public function __construct()
    {

        $this->path = get_template_directory(__FILE__) . "/libs/wuoy/templates/";
        $this->url = get_template_directory_uri(dirname(__FILE__));;
        $this->pages = array(
            intval(wuoyMemberGetOption('front-end_login')),
            intval(wuoyMemberGetOption('general_redirect_page')),
            intval(wuoyMemberGetOption('general_checkout_page')),
            intval(wuoyMemberGetOption('general_thankyou_page')),
            intval(wuoyMemberGetOption('wuoym_user_register')),
            intval(wuoyMemberGetOption('wuoym_user_register_aff')),
            intval(wuoyMemberGetOption('wuoym_user_register_super_aff')),
        );
        add_filter('template_include', array($this, 'newTemplate'));
        add_action('wpinstant_header_template', array($this, 'header'));
        add_action('wpinstant_footer_template', array($this, 'footer'));
        add_action('wp_print_styles', array($this, 'RemoveStyle'), 100);
        add_action('wp_enqueue_scripts', array($this, 'NewStyle'), 10000);

        //add_action('wuoyMember-before-sales-form', array($this, 'infoBeforeForm'), 10, 2);
        return $this->path;
    }

    public function NewStyle()
    {
        //wp_enqueue_style('userstyle', get_theme_file_uri('/assets/css/users.css'), array(), '1.0');
        //		wp_enqueue_style('fontawesome', get_theme_file_uri('/assets/css/font-awesome.min.css'), array(), '1.0');
        //		wp_enqueue_style('animate', get_theme_file_uri('/assets/css/animate.css'), array(), '1.0');
    }

    public function RemoveStyle()
    {
        global $post;
        //	if ( in_array( $post->ID, $this->pages ) ) {
        if (is_page('login')) {
            global $wp_styles, $wp_scripts;
            $wp_styles->queue = array('wpinstant-style', 'bootstrap', 'fontawesome', 'animate', 'userstyle');
            $wp_scripts->queue = array('jquery', 'Tether', 'boostrap', 'customjs', 'ajax-login-script');
            show_admin_bar(false);
        }
    }

    public function header()
    {
        $this->variables = apply_filters('wuoyMember-template-variables', $this->variables);
        if (is_array($this->variables)) :
            extract($this->variables);
        endif;
        require_once($this->path . 'header.php');
    }

    public function footer()
    {
        if (is_array($this->variables)) :
            extract($this->variables);
        endif;
        require_once($this->path . 'footer.php');
    }

    public function newTemplate($template)
    {
        global $post, $wuoyMember;
        $file_template = '';
        $login = wuoyMemberGetOption('front-end_login');
        $redirect = wuoyMemberGetOption('general_redirect_page');
        $checkout = wuoyMemberGetOption('general_checkout_page');
        $thankyou = wuoyMemberGetOption('general_thankyou_page');
        $register = array(
            intval(wuoyMemberGetOption('wuoym_user_register')),
            intval(wuoyMemberGetOption('wuoym_user_register_aff')),
            intval(wuoyMemberGetOption('wuoym_user_register_super_aff')),
        );
        wp_reset_postdata();
        wp_reset_query();
        if ($login == $post->ID) :
            $file_template = $this->path . 'login.php';
        elseif ($redirect == $post->ID) :
            //$file_template = $this->path . 'redirect.php';
        elseif (in_array($post->ID, array($checkout, $thankyou))):
            //$file_template = $this->path . 'default.php';
        elseif ($post->post_type == 'wuoyproduct' || is_singular('wuoyproduct')) :
            $formavailable = true;
            $deactivate = wuoyMemberCustomField('sales_form_deactivate', $post->ID);
            if ($deactivate == 'time') :
                $start = wuoyMemberCustomField('sales_form_start_active', $post->ID);
                $end = wuoyMemberCustomField('sales_form_end_active', $post->ID);
                $now = time();
                if
                (
                    (!empty($start) && strtotime($start) > $now) ||
                    (!empty($end) && strtotime($end) < $now)
                ) :
                    $formavailable = false;
                endif;
            elseif ($deactivate) :
                $formavailable = false;
            endif;
            if (!$formavailable) :
                //$file_template = $this->path . 'payment-not-available.php';
            elseif (is_user_logged_in()) :
                //$file_template = $this->path . 'payment-logged-in.php';
            else :
                //$file_template = $this->path . 'payment-not-logged-in.php';
            endif;
        elseif (in_array($post->ID, $register)) :
            $this->render = array(
                'body' => array('wuoyMember-area', 'wuoyMember-form'),
                'holder' => array('center-area')
            );
        //$file_template = $this->path . 'register.php';
        else :
            $new_template = apply_filters('wuoyMember-template-file', $template);
            switch ($new_template) :
                case 'no-access'    :
                    $this->render = array(
                        'body' => array('wuoyMember-area', 'no-access'),
                        'holder' => array('center-area')
                    );
                    $file_template = $this->path . 'no-access.php';
                    break;
                case 'form'         :
                    $this->render = array(
                        'body' => array('wuoyMember-area', 'wuoyMember-form'),
                        'holder' => array('center-area')
                    );
                    $file_template = $this->path . 'default.php';
                    break;
                case 'register'     :
                    $this->render = array(
                        'body' => array('wuoyMember-area', 'wuoyMember-form'),
                        'holder' => array('center-area')
                    );
                    $file_template = $this->path . 'register.php';
                    break;
                case 'default'      :
                    $this->render = array(
                        'body' => array('wuoyMember-area'),
                        'holder' => array('center-area')
                    );
                    $file_template = $this->path . 'default.php';
                    break;
            endswitch;
        endif;
        $template = file_exists($file_template) ? $file_template : $template;
        if ((function_exists("is_woocommerce") && is_woocommerce()) && $new_template == "no-access") :
            wp_die(__('Maaf, anda tidak berhak untuk mengakses halaman ini', "wuoyMember"), __("Akses Terbatas", "wuoyMember"));
            exit();
        endif;
        return $template;

    }




}

$frontend = new Fronts();
add_action( 'init', 'remove_actions_parent_theme');
function remove_actions_parent_theme(){
    global $wuoyMemberRecurring;

    remove_action('wuoyMember-before-sales-form', array($wuoyMemberRecurring,'infoBeforeForm'),10,2);
    //add_action('wuoyMember-before-sales-form', array($wuoyMemberRecurring,'infoBeforeForm'),10,2);

}

