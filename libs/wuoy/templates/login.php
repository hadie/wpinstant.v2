<?php
/**
 *  * Template Name: Login Page
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 14/10/2017 / 02.28
 */
$message = apply_filters( 'wuoyMember-front-error', '' );
$login   = add_query_arg( $_REQUEST, wuoyMemberGetOption( 'front-end_login', 'post' ) );
do_action( 'wpinstant_header_template' ); ?>
<main id="main" class="mt-3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">


                <div id="loginf">

					<?php do_action( 'wuoyMember/form/login/top', array( 'register' => false ) ); ?>

                    <form id="login" action="<?php echo $login; ?>" method="POST" class="form-signin">

                        <p id="reauth-email" class="reauth-email status text-danger text-center"></p>
                        <div class="form-group">
                        <input type="text" name="username" id="username" class="form-control validate"
                               placeholder="Email address" required autofocus>
                        </div>
                        <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control validate"
                               placeholder="Password" required>
                            <div class="form-group">
                                <div class="form-group">
                        <div id="remember" class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" value="remember-me"> Remember me
                            </label>
                        </div>
                                </div>
                        <button class="btn btn-primary btn-block submit_button btn-flat" type="submit">Sign in</button>
						<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>

                    </form><!-- /form -->
                </div>
                <div class="float-left">
                    <a href="<?php echo wp_lostpassword_url(); ?>" class="forgot-password">
		                <?php _e( "Lupa Password", "wuoyMember" ); ?>
                    </a>
                </div>
                <div class="float-right">
                    <a href="#"><?php echo _e('Belum Punya akun?','');?></a>
                </div>

            </div><!-- /card-container -->

        </div>
    </div>


</main>

<?php do_action( 'wpinstant_footer_template' ); ?>
