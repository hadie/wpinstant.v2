<?php
/**
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 13/10/2017 / 23.42
 */
global $post;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo  apply_filters("the_title",$post->post_title); ?> | <?php bloginfo("name"); ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class();?>>
<div class="container-small mt-5">
    <div class="text-center logo-small"> <a href="<?php echo esc_url(home_url('/'));?>"><img src="<?php echo DANKER_THEME_ASSETS;?>/images/logo-black.svg" alt="WPInstant"></a>
    </div>
</div>

