<?php
/**
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 13/10/2017 / 22.58
 * @see WPI-template-form()
 */
global $post;
do_action('wpinstant_header_template');
extract(apply_filters('wuoyMember-template-variables',array()));

?>
<section class="container">
    <div class="row  justify-content-center">
        <div class="col-md-4">
	        <?php if(!is_user_logged_in() || current_user_can('manage_options')) : ?>


                <h2 class='text-center mt-3'>
					<?php echo apply_filters('the_title',$post->post_title); ?>
                </h2>
                <div class='text-muted text-center'>
					<?php echo apply_filters('the_content',$post->post_content); ?>
                </div>


				<?php
				if(isset($_REQUEST['success'])){
//					wp_redirect( WPINSTANT_USER_DASHBOARD );
//					exit;
				}
				?>

                <form class="wuoyMember-sales-form" method="post" action="<?php echo get_permalink($post->ID); ?>" role="form" data-toggle="validator">
					<?php

					apply_filters('WPI-template-form','');

					?>
					<?php
					$deactivate = wuoyMemberGetOption('general_captcha_deactivate');
					if(!$deactivate) :
						?>
                        <div class="row mb15" >
                            <div class='col text-center'>
								<?php $publickey = wuoyMemberGetOption("general_captcha_public_key"); ?>
                                <div class="g-recaptcha" data-sitekey="<?php echo $publickey; ?>" style='display:inline-block'></div>
                                <script type="text/javascript"
                                        src="https://www.google.com/recaptcha/api.js?hl=en">
                                </script>
                            </div>
                        </div>
					<?php endif; ?>

                    <div class=" mt30">
                        <div class="form-input">
                            <button class="btn btn-block btn-danger"><?php _e("Daftarkan diri saya","wuoyMember"); ?></button>
                        </div>

                    </div>
                </form>

				<?php

			else :
				//  IF USER LOGGED IN AND NOT ADMIN PERMISSION
				//wp_redirect( WPINSTANT_USER_DASHBOARD );
				//exit;
				?>
                <div class='row mt-4'>
                    <div class='col '>
                        <h1 class='text-center'>

							<?php _e("Halaman ini tidak bisa diakses","wuoyMember"); ?>
                        </h1>
                        <div class='text-center' id="wuoyMember-message">
							<?php _e("Maaf, anda sudah terdaftar dan sudah login. <br />Halaman ini khusus untuk pengunjung yang belum mendaftar","wuoyMember"); ?>
                            <p>Silahkan Kembali ke halaman dashboard <a href="<?php echo WPINSTANT_USER_DASHBOARD?>">Klik Disini</a></p>

                        </div>
                    </div>
                </div>
			<?php endif; //endif atas ?>
        </div>
    </div>
</section>


<?php do_action('wpinstant_footer_template'); ?>
