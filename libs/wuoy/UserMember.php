<?php
/**
 * instantdev Project
 * @package instantdev
 * User: dankerizer
 * Date: 14/10/2017 / 00.39
 */

class UserMember {

	var $data = array(
		'username'  => '',
		'firstname' => '',
		'lastname'  => '',
		'email'     => ''
	);
	var $default;
	var $setting;
	var $validate;
	var $customValidate = array();
	var $error;
	var $fields;
	var $options;

	public function __construct() {
		global $wuoyMember;
		$this->options['yn']         = array(
			true  => __( "Ya, aktifkan", "wuoyMember" ),
			false => __( "Nonaktif", "wuoyMember" )
		);
		$default_user_pages          = array(
			'user-edit.php',
			'profile.php'
		);
		$wuoyMember['pages']['user'] = apply_filters( 'wuoyMember-list-page-user', $default_user_pages );
		// ============================================================
		// VALIDATION
		// ============================================================
		$this->validate = array(
			'username'  => array(
				'NotEmpty'   => array(
					'message' => __( "Kolom username tidak boleh kosong", "wuoyMember" ),
				),
				'UserExists' => array(
					'logic'   => false,
					'message' => __( "Username sudah ada, gunakan usename lain", "wuoyMember" ),
				)
			),
			'firstname' => array(
				'NotEmpty' => array(
					'message' => __( "Kolom nama depan tidak boleh kosong", "wuoyMember" ),
				)
			),
			'email'     => array(
				'IsEmail'     => array(
					'message' => __( "Gunakan alamat email yang benar", "wuoyMember" ),
				),
				'EmailExists' => array(
					'logic'   => false,
					'message' => __( "Email telah digunakan", "wuoyMember" ),
				)
			),
			'password'  => array(
				'MoreThan' => array(
					'message' => 'Password harus lebih dari 6 karakter',
					'number'  => 5,
				),
				'SameWith' => array(
					'message' => 'Password yang diketik ulang tidak sama',
					'field'   => 'repassword'
				)
			)
		);
		// ============================================================
		// SETTING
		// ============================================================
		$general_page   = wuoyMemberGetOption( 'user_register' );
		$affiliate_page = wuoyMemberGetOption( 'user_register_aff' );
		$super_aff_page = wuoyMemberGetOption( 'user_register_super_aff' );
		$regpage        = array(
			'buyer'         => $general_page,
			'affiliate'     => $affiliate_page,
			'adv_affiliate' => $super_aff_page
		);
		$this->setting  = array(
			'defrole'      => wuoyMemberGetOption( 'user_buyer_role' ),
			'registration' => $regpage,
			'profile'      => wuoyMemberGetOption( 'theme_profile' ),
			'page'         => array(
				$general_page   => 'buyer',
				$affiliate_page => 'affiliate',
				$super_aff_page => 'adv_affiliate'
			),
			'activation'   => array(
				'affiliate'     => wuoyMemberGetOption( 'user_affiliate' ),
				'adv_affiliate' => wuoyMemberGetOption( 'user_super_affiliate' ),
			),
		);
//		$this->createRole();
//		add_action( 'wp', array( $this, 'prepareCustomFields' ), 1, 1 );
//		add_filter( 'wuoyMember-get-user-fields', array( $this, 'prepareCustomFields' ), 1 );
//		add_action( 'wp', array( $this, 'userAction' ), 2 );
//		add_filter( 'WPI-template-form', array( $this, 'registrationForm' ) );
//		add_action( 'WPI-user-registration', array( $this, 'userFields' ) );
		add_action( 'admin_menu', array( $this, 'remove_menus' ), 99 );
		//add_action( 'admin_init', array( $this, 'redirect_produk' ) );

		//add_action('user_profile_update_errors'		,array($this,'checkRequired'));
	}

	public function redirect_produk() {
		global $pagenow;
		if ( ! current_user_can( 'activate_plugins' ) ) {
			if ( $pagenow == 'admin.php' && isset( $_GET['page'] ) && $_GET['page'] == 'wuoy-member-list-purchase' ) {
				wp_redirect( admin_url( '/', 'http' ) );
				exit;
			}
		}
	}

	function remove_menus() {
		if ( ! current_user_can( 'activate_plugins' ) ) {
			remove_menu_page( 'index.php' );                  //Dashboard
			remove_menu_page( 'jetpack' );                    //Jetpack*
			remove_menu_page( 'edit.php' );                   //Posts
			remove_menu_page( 'upload.php' );                 //Media
			remove_menu_page( 'edit.php?post_type=page' );    //Pages
			remove_menu_page( 'edit-comments.php' );          //Comments
			remove_menu_page( 'themes.php' );                 //Appearance
			remove_menu_page( 'plugins.php' );                //Plugins
			remove_menu_page( 'users.php' );                  //Users
			remove_menu_page( 'tools.php' );                  //Tools
			remove_menu_page( 'options-general.php' );        //Settings
			remove_menu_page( 'wuoy-member-license' );
			remove_menu_page( 'wuoy-member-access-product' );
			remove_menu_page( 'wuoy-member-list-purchase' );
		}

	}

	function registrationForm( $content ) {
		global $post;
		ob_start();
		var_dump( $this->error );
		if ( ( ! is_user_logged_in() || current_user_can( 'activate_plugins' ) ) ) :
			if ( isset( $_REQUEST['success'] ) ) :
				?>
                <div class="alert alert-success alert-dismissible fade show">
                    <p><?php _e( "Silahkan cek email anda untuk detail lebih lanjut", "wuoyMember" ); ?></p>

                </div>
			<?php endif; //success
			?>


            <div class="alert alert-danger alert-dismissible fade show"
                 style="<?= ( ! $this->error ) ? "display:none" : ""; ?>" role="alert">
                <strong><?php _e( "Ada kesalahan pada pengisian", "wuoyMember" ); ?></strong>
                <p class="m-0"><?php echo $this->error; ?></p>

            </div>

			<?php do_action( 'WPI-user-registration', $this->data ); ?>

			<?php wp_nonce_field( 'wuoyMember-user-registration' ); ?>

			<?php
		else :
			?>
            <div class="row">
                <div class="large-12 columns">
                    <div id="wuoyMember-message">
                        <div class="alert-box alert">
							<?php _e( "Maaf, anda sudah terdaftar dan sudah login", "wuoyMember" ); ?>
                        </div>
                    </div>
                </div>
            </div>
			<?php
		endif;
		$content .= ob_get_contents();
		ob_end_clean();

		return $content;
	}

	public function userFields( $data ) {
		$this->data = $data;
		$class      = ( is_array( $this->fields ) && count( $this->fields ) > 0 ) ? "" : "";
		$username   = wuoyMemberGetOption( 'user_field_username' );
		$password   = wuoyMemberGetOption( 'user_field_password' );
		?>
        <div class='rows'>
            <div class='<?php echo $class; ?> '>

                <div class='card-content form-input'>

                    <p>
						<?= __( "Kolom dengan tanda bintang (*) wajib diisi", "wuoyMember" ); ?>
                        <br/><br/>
                    </p>
                    <div class="form-group">

                        <label for="wuoyMember-firstname"
                               data-wrong="<?php _e( "Wajib diisi", "wuoyMember" ); ?>"><?php _e( "Nama Depan", "wuoyMember" ); ?>
                            <sup>*</sup></label>

                        <input required="required" type="text" name="firstname" id="wuoyMember-firstname"
                               value="<?php echo $this->data['firstname']; ?>" class='validate form-control'/>
                        <div class="invalid-feedback"><?php _e( "Wajib diisi", "wuoyMember" ); ?></div>
                    </div>
                    <div class="form-group">
                        <label for="wuoyMember-lastname"><?php _e( "Nama Belakang", "wuoyMember" ); ?></label>

                        <input type="text" name="lastname" id="wuoyMember-lastname"
                               value="<?php echo $this->data['lastname']; ?>" class='form-control'/>

                    </div>
					<?php if ( $username ) :
						if ( ! empty( $this->data['username'] ) ) {
							$readonly = 'readonly';
						} else {
							$readonly = '';
						}
						?>
                        <div class="form-group">
                            <label for="wuoyMember-username"
                                   data-wrong="<?php _e( "Wajib diisi", "wuoyMember" ); ?>"><?php _e( "Username", "wuoyMember" ); ?>
                                <sup>*</sup></label>
                            <input type="text" name="username" id="wuoyMember-username"
                                   value="<?php echo $this->data['username']; ?>"
                                   class='validate form-control' <?php echo $readonly; ?> required="required"/>

                        </div>
					<?php endif; ?>


                    <div class="form-group">
                        <label for="wuoyMember-email"
                               data-wrong="<?php _e( "Wajib diisi dengan alamat email yang benar", "wuoyMember" ); ?>">
							<?php _e( "Alamat Email", "wuoyMember" ); ?> <sup>*</sup>
                        </label>
                        <input required="required" id="wuoyMember-email" type="email" name="email"
                               value="<?php echo $this->data['email']; ?>" class='validate form-control'/>


                    </div>

					<?php if ( $password ) : ?>
                        <div class="form-group">
                            <label for="wuoyMember-password"
                                   data-wrong="<?php _e( "Wajib diisi dengan password minimal 6 huruf", "wuoyMember" ); ?>">
								<?php _e( "Password", "wuoyMember" ); ?> <sup>*</sup>
                            </label>
                            <input required="required" id="wuoyMember-password" type="password" name="password" value=""
                                   class='validate form-control'/>


                        </div>
                        <div class="form-group">
                            <label for="wuoyMember-repass"
                                   data-wrong="<?php _e( "Wajib diisi dengan password yang diketik sebelumnya", "wuoyMember" ); ?>">
								<?php _e( "Password (Ketik Ulang)", "wuoyMember" ); ?> <sup>*</sup>
                            </label>
                            <input required="required" id="wuoyMember-repass" type="password" name="repassword" value=""
                                   class='validate form-control'/>


                        </div>
					<?php endif; ?>
                    <p>    <?php //_e( "<strong>Password anda akan dikirimkan melalui email</strong> setelah anda berhasil mengisi data akun anda", "wuoyMember" ); ?></p>


                </div>
            </div>
        </div>

		<?php
		$this->customFields();
	}

	// ============================================
	// CUSTOM FIELDS
	// ============================================
	public function customFields() {
		global $post;
		if ( is_array( $this->fields ) && count( $this->fields ) > 0 ) :
			?>

            <h5 class=''><?php _e( "Informasi Pribadi Lainnya", "wuoyMember" ); ?> </h5>


            <div class='card-content'>
                <p class="text-muted small">
					<?php _e( "Isi data-data di bawah untuk kepentingan administrasi website ini.", "wuoyMember" ); ?>

                </p>
				<?php
				foreach ( $this->fields as $fieldkey => $field ) :
					if ( is_array( $field['role'] ) && count( $field['role'] ) > 0 && ! empty( $field['role'][0] ) ) :
						if ( $post->post_type == 'wuoyproduct' && in_array( $this->setting['defrole'], $field['role'], $this->setting['defrole'] ) ) :
						else :
							$current_role = array_search( $post->ID, $this->setting['registration'] );
							if ( ! in_array( $current_role, $field['role'] ) ) :
								continue;
							endif;
						endif;
					endif;
					if ( $field['type'] != 'hidden' ) :
						?>

						<?php
						// FIELDS = TEXT, TEXTAREA, BIRTHDATE
						if ( in_array( $field['type'], array( 'text', 'textarea', 'birthdate' ) ) ) :
							$value = ( isset( $this->data[ $field['name'] ] ) ) ? $this->data[ $field['name'] ] : "";
							?>
                            <div class="form-group">


                                <label for="wuoyMember-<?php echo $field['name']; ?>">
									<?php echo $field['label']; ?><?php if ( $field['required'] ) { ?>
                                        <sup>*</sup><?php } ?>
                                </label>

								<?php if ( $field['type'] == 'text' ) : ?>
                                    <input class="form-control" type="text" name="<?php echo $field['name']; ?>"
                                           id="wuoyMember-<?php echo $field['name']; ?>"
                                           value="<?php echo $value; ?>"/>
								<?php elseif ( $field['type'] == 'textarea' ) : ?>
                                    <textarea name="<?php echo $field['name']; ?>"
                                              id="wuoyMember-<?php echo $field['name']; ?>"
                                              class="form-control"><?php echo $value; ?></textarea>
								<?php elseif ( $field['type'] == 'birthdate' ) : ?>
                                    <input type="text" name="<?php echo $field['name']; ?>"
                                           id="wuoyMember-<?php echo $field['name']; ?>" class="form-control"
                                           value="<?php echo $value; ?>"/>
								<?php endif; ?>


                            </div>
							<?php
						// FIELD = SELECT
                        elseif ( $field['type'] == 'select' ) :
							?>
                            <select class="form-control" name="<?php echo $field['name']; ?>"
                                    id="wuoyMember-<?php echo $field['name']; ?>">
								<?php
								if ( count( $field['options'] ) > 0 ) :
									foreach ( (array) $field['options'] as $key => $label ) :
										$selected = ( $value == $key ) ? "selected='selected'" : "";
										?>
                                        <option
                                        value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $label; ?></option><?php
									endforeach;
								endif;
								?>
                            </select>
                            <label for="wuoyMember-<?php echo $field['name']; ?>">
								<?php echo $field['label']; ?><?php if ( $field['required'] ) { ?>
                                    <sup>*</sup><?php } ?>
                            </label>
							<?php
						// FIELD = CHECKBOX
                        elseif ( $field['type'] == 'checkbox' ) :
							?>
                            <label>
								<?php echo $field['label']; ?><?php if ( $field['required'] ) { ?>
                                    <sup>*</sup><?php } ?>
                            </label>
                            <div class="checkboxes">
								<?php
								if ( count( $field['options'] ) > 0 ) :
									foreach ( $field['options'] as $option ) :
										$checked = ( isset( $this->data[ $field['name'] ] ) && in_array( $option['feature'], $this->data[ $field['name'] ] ) ) ? "checked='checked'" : "";
										?>
                                        <label>
                                            <input name="<?php echo $field['name']; ?>[]"
                                                   type="checkbox"
                                                   value="<?php echo $option['feature']; ?>" <?php echo $checked; ?> />
											<?php echo $option['feature']; ?>
                                        </label>
										<?php
									endforeach;
								endif;
								?>
                            </div>
							<?php
						else :
							//	do_action
						endif;
						?>

						<?php
					else :
						?><input type="hidden" name="<?php echo $field['name']; ?>" id="<?= $fieldkey; ?>"
                                 value="<?php echo $value; ?>" /><?php
					endif;
				endforeach;
				?>
            </div>

			<?php
		endif;
	}

	function userAction() {
		global $post, $wuoyMember;
		// ============================================================
		// USER REGISTRATION
		// ============================================================
		if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'wuoyMember-user-registration' ) ) :
			if ( ! array_key_exists( $post->ID, $this->setting['page'] ) ) :
				$validate->valid     = false;
				$validate->message[] = __( "Maaf, anda tidak bisa mengakses pendaftaran ini", "wuoyMember" );

				return;
			endif;
			$this->data = $_POST;
			$validate = apply_filters( 'wuoyMember-registration', $this->data );
			if ( $validate->valid ) :
				$role = $this->setting['page'][ $post->ID ];
				$data = $this->data;
				$data['role'] = $role;
				$this->registerUser( $data );
				wp_redirect( add_query_arg( array( 'success' => 1 ), get_permalink( $post->ID ) ) );
				exit();
			else :
				$this->error = implode( "<br />", $validate->message );
			endif;

		// ============================================================
		// USER UPDATE
		// ============================================================
        elseif ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'wuoyMember-user-update' ) ) :
			if ( $post->ID == $this->setting['profile'] ) :
				$current_user = wp_get_current_user();
				$this->data = array(
					'ID'        => $current_user->ID,
					'firstname' => $_POST['firstname'],
					'lastname'  => $_POST['lastname'],
					'email'     => $_POST['email'],
					'password'  => $_POST['pass'],
					'passconf'  => $_POST['pass_confirmation'],
				);
				foreach ( $_POST as $key => $value ) :
					if ( strpos( $key, 'wuoym_extra_' ) !== false ) :
						$this->data[ $key ] = $value;
					endif;
				endforeach;
				// setting validation;
				unset( $this->validate['username'], $this->validate['recaptcha_response_field'] );
				$this->validate['email']['EmailExists']['owner']   = $current_user->ID;
				$this->validate['email']['EmailExists']['message'] = __( 'Email ini telah digunakan', 'wuoyMember' );
				if ( ! empty( $_POST['pass'] ) ) :
					$this->validate['password'] = array(
						'MoreThan' => array(
							'number'  => 8,
							'message' => __( 'Password minimal 8 karakter' ),
						),
						'SameWith' => array(
							'field'   => 'passconf',
							'message' => __( 'Password konfirmasi tidak sama' )
						)
					);
				else :
					unset( $this->validate['password'] );
				endif;
				$validation = apply_filters( 'wuoyMember-registration', $this->data );
				if ( $validation->valid ) :
					$this->updateUser( $this->data );
					$link = add_query_arg( array(
						'message' => 'success'
					), get_permalink( $this->setting['profile'] ) );
					wp_redirect( $link );
					exit();
                elseif ( count( $validation->message ) > 0 ) :
					$this->error = implode( '<br />', $validation->message );
				endif;
			endif;
		else :
			//wuoyMemberDebug($_POST);
		endif;
	}
	// ============================================================
	// USER UPDATE CORE
	// ============================================================
	function updateUser( $data ) {
		$user = array(
			'ID'           => $data['ID'],
			'first_name'   => $data['firstname'],
			'last_name'    => $data['lastname'],
			'user_email'   => $data['email'],
			'display_name' => $data['firstname'] . ' ' . $data['lastname']
		);
		if ( ! empty( $data['password'] ) ) :
			$user['pass'] = $data['password'];
		endif;
		wp_update_user( $user );
		wp_set_password( $data['password'], $user['ID'] );
		wp_set_auth_cookie( $user['ID'], true );
		foreach ( $data as $key => $value ) :
			if ( strpos( $key, 'wuoym_extra_' ) !== false ) :
				update_user_meta( $user['ID'], $key, $value );
			endif;
		endforeach;
	}
	// ============================================================
	// USER REGISTRATION CORE
	// ============================================================
	function registerUser( $data, $notification = true ) {
		global $wuoyMember;
		$data['role'] = ( ! isset( $data['role'] ) ) ? $this->setting['defrole'] : $data['role'];
		$enable_username = wuoyMemberGetOption( 'user_field_username' );
		$enable_password = wuoyMemberGetOption( 'user_field_password' );
		if ( ! $enable_username ) :
			$data['username'] = strtolower( str_replace( ' ', '', $data['firstname'] ) . substr( time(), - 4 ) );
		endif;
		if ( ! $enable_password ) :
			$data['password'] = strtolower( str_replace( ' ', '', $data['firstname'] ) . substr( time(), - 6 ) );
		endif;
		$this->data = array(
			'user_login' => sanitize_user( $data['username'], true ),
			'user_email' => $data['email'],
			'first_name' => $data['firstname'],
			'last_name'  => $data['lastname'],
			// 'user_pass'		=> wp_generate_password(12,true,true),
			'role'       => $data['role'],
		);
		$role   = $data['role'];
		$active = true;
		$result = wp_insert_user( $this->data );
		if ( is_wp_error( $result ) ) :
			$this->data = $result;
		else :
			$this->data['id']        = $userID = $result;
			$this->data['user_pass'] = ( isset( $data['password'] ) && ! empty( $data['password'] ) ) ? $data['password'] : wp_generate_password( 12 );
			do_action( "wuoyMember/user/password/set", $userID, $this->data["user_pass"] );
			wp_set_password( $this->data['user_pass'], $userID );
			if ( $role <> 'buyer' && ! isset( $data['active'] ) ) :
				$active = $this->setting['activation'][ $role ];
				$active = ( empty( $active ) ) ? "-" : $active;
            elseif ( isset( $data['active'] ) ) :
				$active = $data['active'];
			endif;
			$this->data['status'] = ( $active && $active <> "-" ) ? __( "Aktif", "wuoyMember" ) : __( "Menunggu Konfirmasi Admin", "wuoyMember" );
			foreach ( $data as $key => $value ) :
				if ( strpos( $key, "extra_" ) !== false ) :
					update_user_meta( $userID, $key, $value );
				endif;
			endforeach;
			update_user_meta( $userID, $wuoyMember['prefix'] . 'user_active', $active );
			$this->prepareCustomFields();
			if ( $notification ) :
				$this->emailToUser();
				$this->emailToAdmin();
			endif;
			do_action( 'wuoyMember/user/registered', $userID );
		endif;

		return $this->data;
	}
	// ============================================================
	// PREPARE CUSTOM FIELDS
	// ============================================================
	function prepareCustomFields( $userRole = null ) {
		global $wuoyMember, $pagenow, $post;
		$thepost = $post;
		if ( is_admin() && ! in_array( $pagenow, $wuoyMember['pages']['user'] ) && ( is_null( $userRole ) || is_object( $userRole ) ) ) :
			if ( $pagenow == 'index.php' ) :
				global $post;
				if ( ! is_object( $post ) || ! in_array( $post->ID, $this->setting['registration'] ) ) :
					return;
				endif;
			else :
				return;
			endif;
        elseif ( ! is_admin() && $post->ID == wuoyMemberGetOption( 'theme_profile' ) ) :
			$user     = wp_get_current_user();
			$userRole = $user->roles;
		endif;
		$args = array(
			'post_type'      => 'userfield',
			'posts_per_page' => - 1,
			'orderby'        => array(
				'menu_order' => 'ASC',
				'date'       => 'DESC',
			)
		);
		global $post;
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) :
			while ( $query->have_posts() ) :
				$query->the_post();
				$name     = $wuoyMember['prefix'] . 'extra_' . sanitize_title( $post->post_title );
				$type     = wuoyMemberCustomField( 'type', $post->ID );
				$required = wuoyMemberCustomField( 'required', $post->ID );
				$message  = wuoyMemberCustomField( 'error', $post->ID );
				$options  = wuoyMemberCustomField( 'options', $post->ID );
				$role     = wuoyMemberCustomField( 'permission', $post->ID );
				if ( ! is_null( $userRole ) && is_array( $userRole ) ) :
					if ( ! empty( $role ) && is_array( $role ) && ! empty( $role[0] ) ) :
						$intersect = array_intersect( $role, $userRole );
						if ( count( $intersect ) == 0 ) :
							continue;
						endif;
					endif;
                elseif ( isset( $_POST['user_id'] ) && is_array( $role ) && count( $role ) > 0 && ! empty( $role[0] ) ) :
					$user     = get_userdata( $_POST['user_id'] );
					$required = ( count( array_intersect( $user->roles, $role ) ) > 0 ) ? $required : false;
                elseif ( ! is_admin() && is_array( $role ) && count( $role ) > 0 && ! empty( $role[0] ) ) :
					global $post;
					if ( $thepost->post_type == 'wuoyproduct' ) :
						$required = ( ! in_array( $this->setting['defrole'], $role ) ) ? false : $required;
					else :
						$current_role = array_search( $thepost->ID, $this->setting['registration'] );
						$required     = ( ! in_array( $current_role, $role ) ) ? false : $required;
					endif;
				endif;
				if ( $required ) :
					$this->customValidate[ $name ] = array(
						'NotEmpty' => array(
							'message' => $message
						)
					);
				endif;
				// REGENERATE options
				$new_options = array();
				foreach ( (array) $options as $option ) :
					if ( isset( $option["feature"] ) ) :
						$new_options[ $option['feature'] ] = $option['feature'];
					endif;
				endforeach;
				$options = $new_options;
				$this->fields[ $name ] = array(
					'label'    => $post->post_title,
					'name'     => $name,
					'type'     => $type,
					'required' => $required,
					'options'  => $options,
					'role'     => $role
				);
				$this->default[ $name ] = "";
			endwhile;
		endif;
		wp_reset_postdata();
		wp_reset_query();
		$this->fields         = apply_filters( 'wuoyMember-user-manage-fields', $this->fields );
		$this->default        = apply_filters( 'wuoyMember-user-default-fields', $this->default );
		$this->customValidate = apply_filters( 'wuoyMember-user-custom-validation', $this->customValidate );

		return $this->fields;
	}

	// ============================================
	// USER EMAIL DATA
	// ============================================

	function userEmailData($admin = false,$email = true)
	{
		ob_start();

		if($email) :
			?>
            <table border="0" style="width:500px">
                <tr>
                    <td width="30%"><?php _e("User Login","wuoyMember"); ?></td>
                    <td>: <?php echo $this->data['user_login']; ?></td>
                </tr>
                <tr>
                    <td><?php _e("Nama","wuoyMember"); ?></td>
                    <td>: <?php echo $this->data['first_name'].' '.$this->data['last_name']; ?></td>
                </tr>
                <tr>
                    <td><?php _e("Email","wuoyMember"); ?></td>
                    <td>: <?php echo $this->data['user_email']; ?></td>
                </tr>
				<?php if(!$admin) : ?>
                    <tr>
                        <td><?php _e("Password","wuoyMember"); ?></td>
                        <td>: <?php echo $this->data['user_pass']; ?></td>
                    </tr>
				<?php endif; ?>

				<?php if($admin) : ?>
                    <tr>
                        <td><?php _e("Role","wuoyMember"); ?></td>
                        <td>: <?php echo strtoupper($this->data['role']); ?></td>
                    </tr>
				<?php endif; ?>
                <tr>
                    <td><?php _e("Status","wuoyMember"); ?></td>
                    <td>: <?php echo strtoupper($this->data['status']); ?></td>
                </tr>
				<?php
				if(is_array($this->fields) && count($this->fields) > 0) :
					foreach($this->fields as $field) :
						if(
							is_array($field['role']) && count($field['role']) > 0 && !empty($field['role'][0]) &&
							!in_array($this->data['role'],$field['role'])
						) :
							continue;
						endif;

						?>
                        <tr>
                            <td><?php echo $field['label']; ?></td>
                            <td>: <?php echo apply_filters('wuoyMember-user-data-value',get_user_meta($this->data['id'],$field['name'],true),$this->data['id'],$field['name']); ?></td>
                        </tr>
						<?php
					endforeach;
				endif;
				?>
            </table>
		<?php elseif($email == false) : ?>
            login : <?php echo $this->data['user_login']; ?>, pass : <?php echo $this->data['user_pass']; ?>
			<?php
		endif;

		$content	= ob_get_contents();

		ob_end_clean();

		return $content;
	}

	// ============================================
	// EMAIL TO USER
	// ============================================
	function emailToUser() {
		global $wuoyMemberSMSSend;
		$args = array(
			'userdata' => $this->userEmailData(),
			'login'    => wp_login_url(),
		);
		// SEND EMAIL
		$content = wuoyMemberGetOption( 'email_registration_user' );
		$email   = new wuoyMemberEmail( $this->data['user_email'], $this->data['first_name'], __( "Terima kasih, anda berhasil didaftarkan", "wuoyMember" ), $content, $args );
		// SEND SMS
		$number  = get_user_meta( $this->data['id'], 'wuoym_extra_handphone', true );
		$content = apply_filters( 'wuoyMember-sms-content', 'user-registration' );
		$args = array(
			'userdata' => $this->userEmailData( false, false ),
			'name'     => $this->data['first_name'] . ' ' . $this->data['lastname']
		);
		$wuoyMemberSMSSend->prepare( 'REGIS-USER', $number, $content, $args );
	}

	// ============================================
	// EMAIL TO ADMIN
	// ============================================
	function emailToAdmin() {
		global $wuoyMemberSMSSend;
		$args = array(
			'userdata'  => $this->userEmailData( true ),
			'edit-link' => add_query_arg( array( 'user_id' => $this->data['id'] ), admin_url( 'user-edit.php' ) ),
		);
		// EMAIL
		$content = wuoyMemberGetOption( 'email_registration_admin' );
		$email   = new wuoyMemberEmail( get_bloginfo( 'admin_email' ), 'Admin', __( "Pendaftaran User Baru", "wuoyMember" ), $content, $args );
		// SEND SMS
		$number  = wuoyMemberGetOption( 'sms_phone_number' );
		$content = apply_filters( 'wuoyMember-sms-content', 'admin-registration' );
		$args = array(
			'userdata' => $this->userEmailData( false, false ),
			'name'     => $this->data['first_name'] . ' ' . $this->data['lastname']
		);
		$wuoyMemberSMSSend->prepare( 'REGIS-ADMIN', $number, $content, $args );
	}

	// ============================================
	// EMAIL ACTIVATION
	// ============================================
	function emailActivation( $userID ) {
		global $wuoyMemberSMSSend;
		$user    = get_user_by( 'id', $userID );
		$args    = array(
			'login' => wp_login_url(),
		);
		$content = wuoyMemberGetOption( 'email_user_activation' );
		$email   = new wuoyMemberEmail( $user->user_email, $user->display_name, __( "Akun anda telah diaktifkan", "wuoyMember" ), $content, $args );
		// SEND SMS
		$number  = get_user_meta( $userID, 'wuoym_extra_handphone', true );
		$content = apply_filters( 'wuoyMember-sms-content', 'user-activation' );
		$args = array(
			'userdata' => $this->userEmailData( false, false ),
			'name'     => $this->data['first_name'] . ' ' . $this->data['lastname']
		);
		$wuoyMemberSMSSend->prepare( 'ACTIVE-USER', $number, $content, $args );
	}
}

