<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 14/11/2017 / 11.15
 */


function simpan_kembali($POST,$FILES,$theme_slug = ''){
	$current_user = wp_get_current_user();
	$user_id      = $current_user->ID;
	if(isset($POST['submit'])){
		foreach ($FILES as $key => $value) {
			//GET FILE CONTENT
			$file_array[$key] = json_decode(file_get_contents($value['tmp_name']),true);
		}
		$class     = new  WPInstant();
		$json = json_encode(  $file_array['fileToUpload'], JSON_PRETTY_PRINT );
		if($file_array['fileToUpload']['theme_slug'] == $theme_slug){
			$create_json = $class->makeJson( $json, $user_id, $theme_slug, true );
			$data['code'] = 100; //'nama file sama';
			$data['slug'] = $theme_slug;
			$data['message'] = $create_json;
			//$data['slug_yang_diupload'] =$file_array['fileToUpload']['theme_slug'];
		}else{
			$data['code'] = 102; //'nama file tidak sama';
			$data['slug'] = $theme_slug;
			$data['message'] = '';
			//$data['slug_yang_diupload'] =$file_array['fileToUpload']['theme_slug'];
		}
		//$create_json = $class->makeJson( $json, $user_id, $theme_slug, true );

//		echo '<pre>';
//		print_r($file_array['fileToUpload']);
//		print_r($file_array['fileToUpload']);
//		echo '</pre>';
		$data['json'] =  $file_array['fileToUpload'];
		//echo json_encode($file_array);
		return  $data;
	}
}