<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 07/11/2017 / 20.24
 */

class WPIDataAPI  extends WP_REST_Controller{
	var $my_namespace = 'dataapi/v';
	var $my_version   = '1';
	var $db;
	var $db_name;
	var $user_domain;

	public function __construct() {

		add_action( 'rest_api_init', array( $this, 'register_routes' ) );
	}

	public function register_routes() {
		$namespace = $this->my_namespace . $this->my_version;
		$base      = 'sales';

		register_rest_route( $namespace, '/' . $base, array(
				array(
					'methods'         => WP_REST_Server::READABLE,
					'callback' => array($this,'getdata'),
					'permisson_callbaack'   => array($this,'oauth')
				)
			)
		);


	}


	public function getdata(WP_REST_Request $request){

		$datas =  $this->alldata();
		if(!empty($datas)){
			shuffle($datas);
			foreach ($datas as $data){
				$results = array(
					'first_name'   => $data['first_name'],
					'last_name'    => $data['last_name'],
					'display_name' => $data['display_name'],
					'time'         => $data['date'],
					'produk_title' => $data['product_title'],
					'product_url'  =>  $data['product_url'],
				);
			}
			return $results;
		}
		return false;
	}

	protected function oauth(){
		if ( isset($_REQUEST['key'])){
			$return	= true;

//			if($_REQUEST['key'] == 'KvT$@Bwy0oY6&9^otOpXl4IX1A2!VG^OguJ6P&udrwARtrEz8D!uU&VHn4bj1s8tdRu5b6!7XqaQky*70fby#g6wVLWICL9u^L'){
//				$return	= true;
//			}

			return $return;
		}
	}


	public function alldata() {
		$args        = array(
			'posts_per_page' => 10,
			'offset'         => 0,
			'orderby'        => 'date',
			'order'          => 'DESC',
			'post_type'      => 'wuoysales',
			'post_status'    => 'any',
		);
		$myposts = get_posts( $args );
		$output = array();
		foreach ( $myposts as $post ) {
			setup_postdata( $post );
			$user_id = $post->post_author;
			$user_info = get_userdata($user_id);
			$first_name = $user_info->first_name;
			$last_name = $user_info->last_name;
			$display_name = $user_info->display_name;
			$post_date = $post->post_date;
			$product    = $post->post_parent;
			$output[] = array(
				'first_name' => $first_name,
				'last_name' => $last_name,
				'display_name' => $display_name,
				'date'      => $post_date,
				'product_title'   => get_the_title($product),
				'product_url'   => get_the_permalink($product),
			);
		}
		return $output;
//		$args	= array(
//			'post_type'			=> 'wuoysales',
//			'posts_per_page'	=> 10,
//			'post_status'		=> 'publish'
//		);
//
//		$query	= new WP_Query($args);
//		$return = array();
//		if ( $query->have_posts() ) {
//			while ( $query->have_posts() ) {
//				$query->the_post();
//				$post = $query->post;
//				$user_id = $post->post_author;
//				$user_info = get_userdata($user_id);
//				$first_name = $user_info->first_name;
//				$last_name = $user_info->last_name;
//				$display_name = $user_info->display_name;
//
//				$post_date = $post->post_date;
//				$product    = $post->post_parent;
//				$return[] = array(
//					'first_name' => $first_name,
//					'last_name' => $last_name,
//					'display_name' => $display_name,
//					'date'      => $post_date,
//					'product_title'   => get_the_title($product),
//					'product_url'   => get_the_permalink($product),
//				);
//
//				//return $return;
//			}


//		}
	}
}

$WPIDataAPI = new WPIDataAPI();
//$WPIDataAPI->hook_rest_server();