<?php
/**
 *Template Name: Tutorial Templates
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 18/12/2017 / 14.24
 */
get_header(); ?>
<main class="main" id="main">
    <div class="container py-4">
        <h1 class="mb-5">Tutorial dan Panduan</h1>
		<?php query_posts( 'post_type=post&post_status=publish&posts_per_page=10&paged=' . get_query_var( 'paged' ) ); ?>
		<?php if ( have_posts() ): ?>
            <div class="row">
				<?php while ( have_posts() ): the_post();
					$thumbnail_id   = get_post_thumbnail_id();
					$post_thumbnail = get_the_post_thumbnail();
					$images          = wp_get_attachment_image_src( $thumbnail_id );
					if(has_post_thumbnail()){
						$image = $images[0];
                    }else{
						$image = DANKER_THEME_URL.'/assets/images/placeholder.svg';
                    }
				?>
                   <div id="post-<?php get_the_ID(); ?>" <?php post_class('col-md-4 col-12 mb-4'); ?>>
                       <div class="card" >
                           <div class="card-img-top"><img class="img-fluid w-100" src="<?php echo $image;?>" alt="Card image cap"></div>
                           <div class="card-body">
                               <a href="<?php the_permalink();?>"><?php the_title('<h2 class="h4">','</h2>');?></a>
                               <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>

                           </div>
                       </div>
                   </div>
				<?php endwhile; ?>
            </div>
            <div class="navigation">
                <span class="newer"><?php previous_posts_link( __( '« Newer', 'example' ) ) ?></span> <span
                        class="older"><?php next_posts_link( __( 'Older »', 'example' ) ) ?></span>
            </div><!-- /.navigation -->
		<?php else: ?>

            <div id="post-404" class="noposts">

                <p><?php _e( 'None found.', 'example' ); ?></p>

            </div><!-- /#post-404 -->

		<?php endif;
		wp_reset_query(); ?>
    </div>
</main>
<?php get_footer(); ?>
