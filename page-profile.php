<?php
/**
 ** Template Name: page-profile Template
 * wpinstant WordPress Theme
 * @package wpinstant WordPress Theme
 * User: dankerizer
 * Date: 26/10/2017 / 17.51
 */
global $current_user, $wuoyMember;
$affiliate_enable = wpi_get_theme_option('affiliate_active');

if ( ! is_user_logged_in() ) {
	wp_redirect( WPINSTANT_USER_LOGIN );
	exit;
	//$pagename = 'user-login';
}
$current_user = wp_get_current_user();
$user_name    = $current_user->first_name.' '.$current_user->last_name;
$user_name    = $current_user->display_name;
$user_id      = $current_user->ID;
$avatar       = get_avatar( $user_id, 90, WPINSTANT_AVATAR, $current_user->display_name, array( 'class' => 'rounded-circle' ) );
$date         = date_create( $current_user->user_registered );
$member_since = sprintf( __( 'Member since <span class="text-bold">%s</span>', 'wpinstant' ), date_format( $date, 'M Y' ) );
$users = new Users( $current_user );
//$class = new  WPInstant();
$themes     = $users->themes( $page );
$themecount = $users->themecount();
$lisensi    = $users->lisence( $user_id );
//$sisa       = $users->lisence_domain_count() - $users->domain_count();
$sisa       = $users->lisence_domain_count() - $users->domain_count();
$phone_number = do_shortcode( "[wuoyMember-value type='user' value='wuoym_extra_handphone']" );

$product_id = wpi_get_theme_option('main_product');

//$allsales   = $wuoyMember['sales-recap'];
//$allsales   = $wuoyMember['total'];
//$product    = get_post( $_GET['product'] );
//
//$sales      = ( isset( $allsales[ $product_id ] ) ) ? $allsales[ $product_id ] : 0;
//$sale     = ( isset( $sales[ $affiliate ] ) ) ? $sales[ $affiliate ] : 0;
//var_dump($current_user);
$edit_url   = add_query_arg(array('edit'=> 'true'),get_the_permalink());
get_header(); ?>
    <main id="main" class="section mt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body text-center position-relative">
                            <a href="<?php echo get_the_permalink();?>"><?php echo $avatar ?></a>
                            <h5><?php echo $user_name; ?></h5>
                            <p class="text-muted"><?php echo $member_since ?></p>
                            <a href="<?php echo $edit_url;?>" class="edit-link position-absolute small bg-primary text-light"><i class="fa fa-pencil-square-o"></i> edit</a>
                            <a href="/profile" class="btn btn-sm btn-secondary">Profile Page</a>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col user-info text-center">
                                    <span><?php echo $themecount; ?></span>
                                    <span>Themes</span>
                                </div>
                                <div class="col user-info text-center">
                                    <span><?php echo number_format( $users->domain_count() ); ?></span>
                                    <span>Domain</span>
                                </div>
                                <div class="col user-info text-center">
                                    <span><?php echo number_format( $sisa ); ?></span>
                                    <span>Sisa Lisensi</span>
                                </div>
                            </div>
                        </div>
	                    <?php if($affiliate_enable):?>
                            <div class="card-header">
                                <strong>Affiliate</strong>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><a href="/affiliate">Dashboard Affiliate</a></li>
                                <li class="list-group-item"><a href="/affiliate/bonus">Halaman Bonus</a></li>
                                <li class="list-group-item"><a href="/affiliate/pixel">Setting Pixel</a></li>
                                <li class="list-group-item"><a href="/affiliate/leaderboard">Leaderboard</a></li>
                            </ul>

	                    <?php endif;?>
                        <div class="card-header"><strong>Informasi Lainya</strong></div>
                        <div class="card-body">
                            <span class="d-block text-muted">Phone Number</span>
                            <span class="d-block"><strong><?php echo $phone_number; ?></strong></span>

                        </div>
                    </div>
                </div>

                <div class="col-md-8">

                    <?php //?>
                    <?php

                    if(isset($_GET['edit'])){
	                    get_template_part('template/user','edit');
                    }else{
	                    get_template_part('template/user','pembelian');
                    }
                   ?>
                    <?php// get_template_part('template/user','affiliate');?>
                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>