<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 06/11/2017 / 15.43
 */
?>
<div class="col-md-4 col">
	<div class="widget mb-3">


		<div class="card">
			<div class="card-header">Intalasi</div>
			<div class="card-body">
				<ul class="list-unstyled list-group-flush">
					<?php
					$args = array( 'posts_per_page' => 5, 'offset'=> 0, 'category_name'    => 'instalasi', );
					$myposts = get_posts( $args );
					foreach ($myposts as $post){
						setup_postdata( $post );
						echo '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
					}

					?>
				</ul>
			</div>
		</div>
	</div>
	<div class="widget mb-3">
		<div class="card">
			<div class="card-header">Rekomendasi Plugin</div>
			<div class="card-body">
				<ul class="list-unstyled list-group-flush">
					<?php
					$args = array( 'posts_per_page' => 5, 'offset'=> 0, 'category_name'    => 'plugin', );
					$myposts = get_posts( $args );
					foreach ($myposts as $post){
						setup_postdata( $post );
						echo '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
					}

					?>
				</ul>
			</div>
		</div>
	</div>
	<div class="widget mb-3">
		<div class="card">
			<div class="card-header">Lain-Lain</div>
			<div class="card-body">
				<ul class="list-unstyled list-group-flush">
					<?php
					$args = array( 'posts_per_page' => 5, 'offset'=> 0, 'category_name'    => 'lain-lain', );
					$myposts = get_posts( $args );
					foreach ($myposts as $post){
						setup_postdata( $post );
						echo '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
					}

					?>
				</ul>
			</div>
		</div>
	</div>
</div>
