<?php
/**
 * Template Name: Lisensi Template
 * wpinstant WordPress Theme
 * @package wpinstant WordPress Theme
 * User: dankerizer
 * Date: 26/10/2017 / 02.22
 */
if(!is_user_logged_in()){
	wp_redirect( WPINSTANT_USER_LOGIN );
	exit;
	//$pagename = 'user-login';
}
global $current_user;

$user_name = $current_user->display_name;
$user_id         = $current_user->ID;

$message = 'Halo <strong>'.$user_name.'</strong>, ';
$users  = new Users($current_user);
$class = new  WPInstant();
$lisensi = $users->lisence();

get_header();?>
	<main id="main" class="section" role="main">
        <section class="create-bar pt-3 pb-3">
            <div class="container">
                <div class="row">

                    <div class="col-md-6 col-12">
                        <h2><?php echo _e('License','wpinstant');?></h2>
                    </div>
                    <div class="col-md-6 col-12 text-md-right small-info">
                        <span><?php echo number_format( $users->domain_count()); ?></span>
                        <span class="small text-muted">dari <?php echo number_format($users->lisence_domain_count());?> domain </span>

                        </div>
                    </div>

                </div>


            </section>

        <section class="section container mt-3">
<?php if(user_is_active()) :?>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock" aria-hidden="true"></i></span>
                <input type="text" class="form-control" value="<?php echo $users->lisence();?>" id="lisence" placeholder="Amount" disabled>
                <button type="button" id="copyurl" class="btn copy input-group-addon" data-clipboard-text="<?php echo $users->lisence();?>"><i class="fa fa-clipboard fa-xs" aria-hidden="true"></i>  Copy</button>

            </div>
            <h4 class="mt-3"><?php echo _e('List domain yang sudah terinstall dengan theme dari WPInstant','wpinstant');?></h4>

            <?php
$domains = $users->domain(100,'create_date');

if(!empty($domains)):
    ?>

        <?php
	foreach ( $domains as $domain ) :
		//var_dump($domain);
		$id             = $domain->id;
		$domain_name    = $domain->domain_name;
		$create_date    = $domain->create_date;
		$display_domain = hidden_domain( $domain_name );
?>
        <div class="row list-domain mb-3">
        <div class="col domain-item">
            <?php echo $display_domain;?>
        </div>
	       <div class="col domain-item">
            <?php echo md5($domain_name);?>
        </div>
        <div class="col small text-muted domain-item">
            <?php echo $create_date;?>
        </div>
        <div class="col text-md-right domain-item">
		 <a class='request_delete btn btn-sm btn-outline-primary' data-domain='<?php echo $id;?>' data-user='<?php echo $user_id;?>' href='#' domain-id='$id'><i class='fa fa-trash-o'></i> Request Delete</a>

            <a class='delete_domain btn btn-sm btn-danger' data-domain='<?php echo $id;?>' data-user='<?php echo $user_id;?>' href='#' domain-id='$id'><i class='fa fa-trash-o'></i> Delete</a>
        </div>
        </div>
        <?php
	endforeach;

        ?>

    <?php
endif;
?>
            <p class="text-muted">Catatan</p>
            <ol class="pl-2 list-group-flush text-muted">
                <li>Domain akan otomatis bertambah saat anda menginstall theme dari WPInstant</li>
                <li>Data domain anda kami jaga kerahasianya, untuk menjaga privasi anda.</li>
            </ol>
<?php else: message_for_in_active()?>
<?php endif;?>
        </section>
	</main>
<?php get_footer();?>