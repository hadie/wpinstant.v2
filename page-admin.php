<?php
/**
 ** Template Name: Halaman Admin Page Template
 * wpinstant.v2 WordPress Theme
 * @package wpinstant.v2 WordPress Theme
 * User: dankerizer
 * Date: 11/11/2017 / 01.08
 */
if(!current_user_can('manage_options')){
	return false;
}
get_header();?>
<main id="main" class="section mt-3">
    <div class="container">

<?php
$parent = wp_get_post_parent_id( get_the_id() );

if(!$parent){
	get_template_part('admin/content');
}else{
	//echo $post->post_name;
	get_template_part('admin/content',$post->post_name);

}

?>
    </div>
</main>
<?php get_footer();?>
