<?php
/**
 * Template Name: Dashboard
 *
 * @package Intant
 * @subpackage WpInstant
 * @since Wp Instant 1.0
 */
if(!is_user_logged_in()){
	wp_redirect( WPINSTANT_USER_LOGIN );
	exit;
	//$pagename = 'user-login';
}

global $current_user;

$user_name = $current_user->display_name;
$message = 'Halo <strong>'.$user_name.'</strong>, ';
$users  = new Users($current_user);
$class = new  WPInstant();
//$theme_slug = 'manualtheme';
//$theme_id = $class->getThemeid($theme_slug,$user_id);


$user                = trim(urldecode('coba test'));
//echo urlencode_deep('coba test');
//echo '<br/>';
//echo $user;
get_header();?>
<main id="main" class="section" role="main">
<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<div class="wellcome-message">
				<h2><?php echo $message;?> Welcome at WPInstant</h2>
				<p>The Easiest WordPress Theme Builder</p>
                <?php if(!user_is_active()):?>
                    <a href="<?php echo WPINSTANT_PRODUCT_URL;?>" class="btn btn-primary">Upgrade Sekarang</a>
                    <?php else:?>

                <div class="row mt-3 mb-3">
                    <div class="col-md-4 col-12 mb-3 mb-md-0">
                        <div class="stats position-relative">
                            <div class="info float-left">
                                <span><?php echo number_format( $users->domain_count()); ?></span>
                                <span class="small text-muted">Domain used WPInstant</span>
                            </div>
                            <div class="icon position-absolute bg-dark">
                                <i class="fa fa-globe fa-3x text-white"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3 col-12 mb-md-0">
                        <div class="stats position-relative">
                            <div class="info float-left">
                                <span><?php echo number_format( $users->themecount()); ?></span>
                                <span class="small text-muted">Theme Created in WPInstant</span>
                            </div>
                            <div class="icon position-absolute bg-dark">
                                <i class="fa fa-wordpress fa-3x text-white"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="stats position-relative">
                            <div class="info float-left">
                                <span><?php echo number_format( $users->lisence_domain_count()); ?></span>
                                <span class="small text-muted">Maximum Allowed Domain Using WPInstant</span>
                            </div>
                            <div class="icon position-absolute bg-dark">
                                <i class="fa fa-unlock-alt fa-3x text-white"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif;?>
			</div>
		</div>
	</div>
    <div class="w-100 mb-5"></div>
<?php if(user_is_active()):?>
	<div class="row">
		<div class="col">
			<div class="text-center card p-3 rounded">
				<div class="icon">
					<i class="fa text-info fa-wordpress fa-5x"></i>
				</div>
				<div class="card-body">
					<a href="/create" class="btn btn-danger">Create New Theme</a>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="text-center card p-3 rounded">
				<div class="icon">
					<i class="fa fa-download text-dark fa-5x"></i>
				</div>
				<div class="card-body">
					<a href="/themes" class="btn btn-primary">Download Themes</a>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="text-center card p-3 rounded">
				<div class="icon">
					<i class="fa fa-unlock-alt text-warning fa-5x"></i>
				</div>
				<div class="card-body">
					<a href="/lisensi" class="btn btn-primary">Manage Your License</a>
				</div>
			</div>
		</div>
	</div>
    <?php endif;?>
</div>

</main>


<?php


?>
<?php get_footer();?>