<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 19/11/2017 / 13.14
 */
$main_product = wpi_get_theme_option('main_product');
global $wuoyMember;
$user     = array();
$allsales = $wuoyMember['sales-recap'];
$sales = $allsales[ wpi_get_theme_option( 'main_product' ) ];
//var_dump($sales);

?>
<div class="row">
    <div class="col-12 col-md-6 ">
        <h4>Leaderboard</h4>
        <table class="table table-dark table-sm table-bordered table-responsive-xl">
            <thead>
            <tr class="text-center">
                <th>User</th>
                <th>Terjual</th>
            </tr>
            </thead>

        <?php
        $total = array_sum( $sales );
        arsort( $sales );
        //var_dump($sales);
        foreach ( $sales as $affiliate => $sale ) :
        $key          = 'data_' . $i;
        $user         = get_userdata( $affiliate );
        $avatar       = get_avatar( $user->ID, 50, get_option( 'avatar_default', 'mystery' ), $user->display_name, array( 'class' => 'rounded-circle' ) );
        $penjualan    = $sale;
        $nama = $user->display_name;
            if ( $affiliate == 0 ) {
                $nama = 'Tanpa Affiliate';
            }
        echo '<tr><td>'.$nama.'</td><td class="text-center">'.$penjualan.'</td></tr>';

        endforeach;
        ?>
            <tfoot>
                <tr class="bg-primary text-light">
                    <td>Total</td>
                    <td class="text-center"><?php echo $total?></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="col-12 col-md-6">
        <h4>Yang sudah Punya Afflink</h4>
        <table class="table table-sm table-responsive-xl">
            <thead>
            <tr>
                <th>User</th>
                <th>Dibuat</th>
<!--                <th>Traffic</th>-->
            </tr>
            </thead>
        <tbody>


        <?php
            $atur = array(
                'post_type' => 'wuoyshortlink',
                'post_status' => 'publish',
                'numberposts' => '200'
            );
            $afflinks = get_posts($atur);
            foreach ($afflinks as $item){

                $id = $item->ID;
                $post_date = $item->post_date;
                $post_name = $item->post_name;
                $affiliate  = $item->post_author;
                $userdata = get_userdata( $affiliate );



                echo '
               <tr>
               <td>'.$userdata->display_name.' <span class="small text-muted">('.$userdata->nickname.')</span></td>
               <td>'.date('F j, Y g:i a',strtotime($post_date)).'</td>
</tr>
               ';
            }
        ?>
        </tbody>
        </table>
    </div>

</div>
