<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 30/10/2017 / 16.01
 */

class WPInstantAdmin{
	static $params;
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'menu_page' ) );
		add_action( 'admin_init', array( $this, 'register_settings' ) );

	}

	public function menu_page(){
		add_menu_page(
			esc_html__( 'Halaman Setting WPInstant', 'wpinstant' ),
			esc_html__( 'WPInstant', 'wpinstant' ),
			'manage_options',
			'wpinstant-options',array($this,'halaman_setting'),
			'dashicons-wordpress-alt',
			11
		);
	}
	public static function get_theme_options() {
		return get_option( 'wpinstant_options' );
	}

	public static function get_theme_option( $field,$default = '' ) {
		if ( self::$params ) {
			$params = self::$params;
		} else {
			$params       = get_option( 'wpinstant_options', array() );
			self::$params = $params;

		}
		if ( isset( $params[ $field ] ) && $field ) {
			return $params[ $field ];
		} else {
			return $default;
		}
	}

	public function halaman_setting(){

		var_dump( get_option( 'wpinstant_options' ));
		//var_dump($_REQUEST);
		?>

		<div class="wrap">
			<h1><?php esc_html_e( 'Halaman Setting', 'wpinstant' ); ?></h1>
			<form method="post" action="">
				<?php ent2ncr( self::set_nonce() ) ?>
				<?php //settings_fields( 'wpinstant_options' ); ?>
				<table class="form-table">
                    <tr>
                        <th><label for="main_product">Produk Utama Kita</label></th>
                        <td>
                            <select name="wpinstant_options[main_product]" id="main_product">
								<?php
								$value = self::get_theme_option( 'main_product' );
								$items = self::get_items();
								//var_dump($items);
								foreach ($items as $key=>$item){
									$selected = selected( $value, $key, false );
									echo '<option value="'.$key.'" '.$selected.'>'.$item['title'].'</option>';
								}
								?>
                            </select>
	                        <?php echo get_the_title($value);?>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="affiliate_active">Apakah Affiliate Di Aktifkan</label></th>
                        <td>
                            <?php
                            $aktif = self::get_theme_option( 'affiliate_active',1 );
                            ?>
                            <input
	                            <?php checked( self::get_theme_option( 'affiliate_active' ), 1 ) ?>
                                    type="checkbox" value="1" name="wpinstant_options[affiliate_active]" id="affiliate_active">
                        </td>
                    </tr>
                </table>

				<?php submit_button(); ?>
			</form>
		</div>
<?php
	}

	private function get_items(){
		$args = array(
			'post_type'			=> 'wuoyproduct',
			'posts_per_page'	=> -1,
			'post_status'       => 'publish',
			'post_parent'       => 0
		);

		$results = array();
		$query	= new WP_Query($args);
		if($query->have_posts()) :
			while($query->have_posts()) :
				$query->the_post();
//				$results[]['id'] = get_the_ID();;
//				$results[]['title'] = get_the_title(get_the_ID());
				$results[get_the_ID()] = array(
					'id'    => get_the_ID(),
					'title' => get_the_title(get_the_ID()),

				);

				endwhile;
		endif;
		return $results;
	}
	public static function register_settings() {
	   // var_dump($_POST);
		//register_setting( 'wpinstant_options', 'wpinstant_options', array( 'WPInstantAdmin', 'sanitize' ) );
		if ( isset( $_POST['wpinstant_options'] ) && wp_verify_nonce( $_POST['_wpinstant_options_ajax_nonce'], 'wpinstant_options_save_paramter_settings' ) ) {
			update_option( 'wpinstant_options', $_POST['wpinstant_options'] );


		}
	}
	protected static function set_nonce() {
		return wp_nonce_field( 'wpinstant_options_save_paramter_settings', '_wpinstant_options_ajax_nonce' );
	}
	public static function sanitize( $options ) {

		// If we have options lets sanitize them
		if ( $options ) {

			// Checkbox
			if ( ! empty( $options['affiliate_active'] ) ) {
				$options['affiliate_active'] = 1;
			} else {
				unset( $options['checkbox_example'] ); // Remove from options if not checked
			}

			// Input
			if ( ! empty( $options['input_example'] ) ) {
				$options['input_example'] = sanitize_text_field( $options['input_example'] );
			} else {
				unset( $options['input_example'] ); // Remove from options if empty
			}

			// Select
			if ( ! empty( $options['main_product'] ) ) {
				$options['main_product'] = sanitize_text_field( $options['main_product'] );
			}

		}

		// Return sanitized options
		return $options;

	}
}

new WPInstantAdmin();

function wpi_get_theme_option( $id = '' ) {
	return WPInstantAdmin::get_theme_option( $id );
}