<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 19/11/2017 / 03.22
 */

$report = new AdminReport();
$user = array();
$main_product = wpi_get_theme_option('main_product');
$sales = $report->SaleCount($main_product);
//var_dump($sales);
$price = wuoyMemberCustomField("price_recurring", $main_product);
$price = $price - ($price * (59 / 100));
?>

<div class="row">





            <?php
            foreach ($sales['pending'] as $key => $pending) {
	            $title = str_replace(array('today', 'last7days', 'all'), array('Hari ini', '7 Hari', 'Semua'), $key);
	            echo '
                <div class="col">
        <div class="card">
              <div class="card-header bg-warning">' . $title . '</div>
            <div class="card-body text-center">
            <div class="h1">' . $pending . '</div>
            <p class="small">' . number_format($price * $pending) . '</p>
</div>
        </div>
    </div>
            ';
            }
            ?>




            <?php
            foreach ($sales['draft'] as $key => $draft) {
	            $title = str_replace(array('today', 'last7days', 'all'), array('Hari ini', '7 Hari', 'Semua'), $key);
	            echo '
                <div class="col">
        <div class="card">
           <div class="card-header bg-secondary text-light"> ' . $title . '</div>
            <div class="card-body text-center">
            <div class="h1">' . $draft . '</div>
            <p class="small">' . number_format($price * $draft) . '</p>
</div>
        </div>
    </div>
            ';
            }
            ?>




            <?php
            foreach ($sales['publish'] as $key => $publish) {
	            $title = str_replace(array('today', 'last7days', 'all'), array('Hari ini', '7 Hari', 'Semua'), $key);
	            echo '
                <div class="col">
        <div class="card">
            <div class="card-header bg-success text-light">' . $title . '</div>
            <div class="card-body text-center">
            <div class="h1">' . $publish . '</div>
            <p class="small">' . number_format($price * $publish) . '</p>
</div>
        </div>
    </div>
            ';
            }
            ?>



</div>
<div class="keterangan mt-5"></div>
<p><span class="badge badge-warning">Menunggu Konfirmasi</span> <span class="badge badge-secondary">Belum dibayar</span> <span class="badge badge-success">Sudah Aktif</span> </p>


