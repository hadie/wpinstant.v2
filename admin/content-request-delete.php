<?php
/**
 * Created by Hadie Danker - request-delete.php.
 * URI : https://theme.id
 * Date: 4/23/2018
 * Time: 8:43 PM
 */

$adminya = new AdminReport();
$page = isset($_GET['hal'])? (int)$_GET['hal'] : 1;
$user = isset($_GET['user']) ? $_GET['user'] : 0;
$status = 1;
$request = $adminya->reqDelete($page,$user,$status);

//var_dump($request);

?>

<div class="row">

	<div class="col-12 col-md-12 ">

		<?php
		if(!empty($request)){
			echo '<div class="table-responsive-sm"><table class="table">';
				echo " <thead>
    <tr>
    <th>Nama</th>
    <th>Email</th>
    <th>Domain</th>
    <th>Domain Hash</th>
    <th>Request Date</th>
    <th>Actions</th>
</tr>
 
</thead>";
			foreach ($request as $item){
				$user_id = $item['user_id'];
				$user_info = get_userdata($user_id);
				$link = add_query_arg(array('user' => $user_id),get_the_permalink());
				echo '<tr>';
				echo '<td><a href="'.$link.'">'.$user_info->display_name.'</a> </td>';
				echo '<td>'.$user_info->user_email.'</td>';
				echo '<td>'.hidden_domain( $item['domain_name'] ).'</td>';
				echo '<td>'.$item['domain_hash'].'</td>';
				echo '<td>'.date('d M Y', strtotime($item['create_date'])).'</td>';
				echo '<td><a href="#" class="lakukan_request_delete btn btn-sm btn-danger"  data-user="'.$user_id.'" domain-id="'.$item['domain_hash'].'">Hapus Domain</a> </td>';
				echo '</tr>';
			}
			echo '</table></div>';
		}
		?>
	</div>
</div>