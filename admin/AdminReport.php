<?php
/**
 * wpinstant.v2 Project
 * @package wpinstant.v2
 * User: dankerizer
 * Date: 19/11/2017 / 03.32
 */

class AdminReport {

    public function __construct()
    {

    }

    public function reqDelete($page = 1,$user = 0,$status = 1){
	    global $wpdb;
	    $table_name = $wpdb->prefix . "instant_request_delete";
	    $mulai = (int)$page - 1;
	    $query = "SELECT * FROM  $table_name WHERE status = $status [USERNYA] LIMIT $mulai,20 ";

	    if($user){
	    	$query = str_replace('[USERNYA]',"AND user_id = '$user'",$query);
	    }else{
		    $query = str_replace('[USERNYA]','',$query);
	    }


	    $resutls = $wpdb->get_results($query, ARRAY_A);

	    return $resutls;

    }

    public function SaleCount($productid = 0)
    {
        global $wpdb;
        $results = array();
        $maxdate = date("Y-m-d", strtotime("1 day"));
        $seminggu = date("Y-m-d", strtotime("-7 day"));
        $sebulan = date("Y-m-d", strtotime("-30 day"));
        $query = "SELECT [fields] " .
            //            "FROM " . $wpdb->posts . " AS posts [affiliate-group] " .
            "FROM " . $wpdb->posts . " AS posts  " .
            "WHERE posts.post_type = 'wuoysales' " .
            "AND posts.post_status = %s " .
            //            "[affiliate] " .
            "[mindate]" .
            "[maxdate]" .
            "[product] " .
            "";
        $query = str_replace("[affiliate-group]", "INNER JOIN " . $wpdb->postmeta . " AS postmeta ON posts.ID = postmeta.post_id ", $query);
        $query = str_replace("[affiliate]", "AND postmeta.meta_key = 'affiliate' AND postmeta.meta_value = '" . $this->user_id . "' ", $query);
        //$query = str_replace("[affiliate]", "AND postmeta.meta_key = 'affiliate' AND postmeta.meta_value = '2' ", $query);
        // $query = str_replace("[affiliate]", "", $query);
        //$query = str_replace("[fields]", "YEAR(posts.post_date) AS `YEAR`, MONTH(posts.post_date) AS `MONTH`,DAY(posts.post_date) AS `DAY`, COUNT(*) AS `TOTAL`", $query);
        $query = str_replace("[fields]", " COUNT(*) AS TOTAL", $query);
        if ($productid == 0) {
            $query = str_replace("[product]", " ", $query);
        } else {
        }
        $query = str_replace("[product]", "AND posts.post_parent = '" . $productid . "' ", $query);
        // $query = str_replace("[productid]", " AND posts.post_parent = ".$this->product_id."", $query);
        $allquery = str_replace('[mindate]', "", $query);
        $allquery = str_replace('[maxdate]', "", $allquery);
        $query = str_replace('[mindate]', "AND posts.post_date <= %s ", $query);
        $query = str_replace('[maxdate]', "AND posts.post_date >= %s ", $query);
        //today
        $today = $wpdb->prepare($query, 'publish', $maxdate, $maxdate);
        $today_draft_query = $wpdb->prepare($query, 'draft', $maxdate, $maxdate);
        $today_pending_query = $wpdb->prepare($query, 'pending', $maxdate, $maxdate);
        //7 days
        $last7days = $wpdb->prepare($query, 'publish', $maxdate, $seminggu);
        $last7days_draft_query = $wpdb->prepare($query, 'draft', $maxdate, $seminggu);
        $last7days_pending_query = $wpdb->prepare($query, 'draft', $maxdate, $seminggu);



        $all = $wpdb->prepare($allquery, 'publish');
        $all_draft = $wpdb->prepare($allquery, 'draft');
        $all_pending = $wpdb->prepare($allquery, 'pending');
        $results = array(
            'publish' => array(
                'today' => $this->displaycount($wpdb->get_results($today, ARRAY_A)),
               // 'last7days' => $this->displaycount($wpdb->get_results($last7days, ARRAY_A)),
                'all' => $this->displaycount($wpdb->get_results($all, ARRAY_A)),
            ),
            'draft' => array(
                'today' => $this->displaycount($wpdb->get_results($today_draft_query, ARRAY_A)),
               // 'last7days' => $this->displaycount($wpdb->get_results($last7days_draft_query, ARRAY_A)),
                'all' => $this->displaycount($wpdb->get_results($all_draft, ARRAY_A)),
            ),
            'pending' => array(
                'today' => $this->displaycount($wpdb->get_results($today_pending_query, ARRAY_A)),
                //'last7days' => $this->displaycount($wpdb->get_results($last7days_pending_query, ARRAY_A)),
                'all' => $this->displaycount($wpdb->get_results($all_pending, ARRAY_A)),
            ),
        );
        //return $results;
        return $results;
        //return $all;
    }

    static function prepare($wpdb, $status)
    {

    }

    static function displaycount($array)
    {
        foreach ($array as $sale) {
            return $sale['TOTAL'];
        }
    }



}
function add_toolbar_init(){
    if(current_user_can('manage_options')){
        add_action('admin_bar_menu', 'add_toolbar_items', 1000);
    }


}
add_action('init', 'add_toolbar_init');

function add_toolbar_items($admin_bar){
	global $wpdb;
    $admin_bar->add_menu( array(
        'id'    => 'AdminReport',
        'title' => 'Admin Report',
        'href'  => '/halaman-admin',
        'meta'  => array(
            'title' => __('Laporan'),
        ),
    ));
    $admin_bar->add_menu( array(
        'id'    => 'aff_report',
        'parent' => 'AdminReport',
        'title' => 'Laporan Affiliate',
        'href'  => DANKER_SITE_URL.'/halaman-admin/affiliate',
        'meta'  => array(
            'title' => __('Laporan Affiliate'),
            'class' => 'aff_report'
        ),
    ));
	$table_name = $wpdb->prefix . "instant_request_delete";
	$req_delete = $wpdb->get_var("SELECT COUNT(*) FROM $table_name WHERE status = 1 ");
	$admin_bar->add_menu( array(
		'id'    => 'req_delete',
		'title' => ''.$req_delete.' Request Delete',
		'href'  => DANKER_SITE_URL.'/halaman-admin/request-delete',
		'meta'  => array(
			'title' => __('Request Delete'),
			'class' => '',
		),
	));
//    $admin_bar->add_menu( array(
//        'id'    => 'my-second-sub-item',
//        'parent' => 'AdminReport',
//        'title' => 'Button Generator',
//        'href'  => DANKER_SITE_URL.'/button-generator.php',
//        'meta'  => array(
//            'title' => __('Button Generator'),
//            'target' => '_blank',
//            'class' => 'my_menu_item_class'
//        ),
//    ));
}
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}