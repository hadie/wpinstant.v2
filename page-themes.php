<?php
/**
 * Template Name: List Themes
 * wpinstant WordPress Theme
 * @package wpinstant WordPress Theme
 * User: dankerizer
 * Date: 26/10/2017 / 02.23
 */
if (!is_user_logged_in()) {
    wp_redirect(WPINSTANT_USER_LOGIN);
    exit;
    //$pagename = 'user-login';
}
global $current_user;
$page = isset($_GET['hal']) ? (int)$_GET['hal'] : 1;
$user_login = $current_user->user_login;
$display_name = $current_user->display_name;
$message = 'Halo <strong>' . $display_name . '</strong>, ';
$users = new Users($current_user);
$class = new  WPInstant();
$themes = $users->themes($page);
$themecount = $users->themecount();
$lisensi = $users->lisence($user_id);
get_header(); ?>
    <main id="main" class="section" role="main">
        <div class="create-bar pt-3 pb-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-12">
                        <h2>Download Theme</h2>
                    </div>

                    <div class="col-md-4 col-12 text-md-right ">
                        <a href="/create" class="btn btn-danger"><i class="fa fa-plus"></i> Create New Theme</a>
                        <a href="/lisensi" class="btn btn-outline-danger"><i class="fa fa-unlock-alt"></i> License</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <?php
            if ($themecount == 0) {
                ?>
                <div class="card mt-3 text-center">
                    <div class="card-body">
                        <h4 class="card-title"><?php echo _e('Halo, Thank you'); ?></h4>
                        <p class="card-text">You are not yet create any theme in WPInstant, Please create a new
                            theme. </p>

                    </div>
                </div>
                <?php
            } else {
                ?>
                <ul class="list-downloads list-unstyled" data-view="download">
                    <?php

                    //var_dump( $themecount );
                    foreach ($themes as $theme) {

                        $id = $theme->id;
                        $name = $theme->theme_name;
                        $slug = $theme->theme_slug;
                        $desc = $theme->theme_desc;
                        $type = $theme->theme_type;
                        $create = $theme->create_date;
                        $update = $theme->update_date;
                        $publi = $theme->is_public;
                        $gen = WPINSTANT_API . '/index.php/WPInstant_Generator/index/' . $user_login . '/' . $slug;
                        $preview = WPINSTANT_PREVIEW . '?theme=' . $slug . '&user=' . $user_login;
                        $editurl = DANKER_SITE_URL . '/create?step=2&edit=true&themeid=' . $id;
                        ?>
                        <li class="themelist-item mb-3">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="media">
                                        <img class="align-self-center mr-3" src="<?php echo WPINSTANT_AVATAR; ?>"
                                             width='40' alt='<?php echo $name ?>'>
                                        <div class='media-body'>
                                            <h5 class="mt-0"><?php echo $name; ?>
                                                <small class="text-muted"><?php echo $slug ?></small>
                                            </h5>
                                            <p class="mb-0"><?php echo $desc; ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 text-md-right mt-md-3 mt-3">
                                    <div class="align-self-center">
                                        <a href="<?php echo $gen; ?>" target="_blank" class="btn btn-success btn-sm">Download</a>
                                        <a href="<?php echo $preview; ?>" target="_blank"
                                           class="btn btn-secondary btn-sm"><i
                                                    class="fa fa-external-link"></i> Preview</a>
                                        <a href="<?php echo $editurl; ?>" class="btn btn-secondary btn-sm"><i
                                                    class="fa fa-pencil-square-o"></i> Edit</a>
                                        <?php if (current_user_can('manage_options')) { ?>
                                            <a href="#" data-themename="<?php echo $name; ?>"
                                               data-userid="<?php echo $current_user->ID; ?>"
                                               data-themeslug="<?php echo $slug; ?>"
                                               data-themeid="<?php echo $id; ?>"
                                               class="deleteheme btn btn-secondary btn-sm"><i
                                                        class="fa fa-trash-o"></i> Delete</a>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                        </li>
                        <?php
                    }
                    ?>
                </ul>

                <div class="w-100 clearfix mt-3 mb-3"></div>
                <?php
                $current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                dn_pagination(ceil($themecount / 10), $current_url, 3, 'hal');

            } //else if themecount not null
            ?>

        </div>

    </main>
<?php get_footer(); ?>