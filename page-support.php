<?php
/**
 ** Template Name: Support Template
 * wpinstant.v2 WordPress Theme
 * @package wpinstant.v2 WordPress Theme
 * User: dankerizer
 * Date: 06/11/2017 / 15.02
 */
get_header(); ?>
<main id="main" class="section" role="main">
    <div class="container mt-2">
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				?>
                <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                    <header class="">
						<?php the_title( '<h1 class="h2">', '</h1>' ); ?>
                    </header>

                    <div class="entry-content">
						<?php the_content(); ?>
                    </div>
                    <div class="row mb-5">
                        <div class="col ">
                            <a href="https://telegram.me/wpinstant" target="_blank" class="support-link d-block position-relative">
                                <div class="support-title ">
                                    <span>@wpinstant</span>
                                    <span>Telegram</span>

                                </div>
                                <div class="icon position-absolute bg-dark text-light"><i class="fa fa-telegram fa-2x"></i></div>

                            </a>
                        </div>
                        <div class="col ">
                            <a href="mailto:wpinstantco@gmail.com" class="support-link d-block position-relative">
                                <div class="support-title ">
                                    <span>wpinstantco@gmail.com</span>
                                    <span>Email</span>

                                </div>
                                <div class="icon position-absolute bg-dark text-light"><i class="fa fa-envelope fa-2x"></i></div>

                            </a>
                        </div>
                        <div class="col ">
                            <a href="tel:+6281271735107" class="support-link d-block position-relative">
                                <div class="support-title ">
                                    <span>081271735107</span>
                                    <span>Phone</span>

                                </div>
                                <div class="icon position-absolute bg-dark text-light"><i class="fa fa-phone-square fa-2x"></i></div>

                            </a>
                        </div>
                    </div>
                </article>
				<?php
			endwhile;
		else :
			//get_template_part( 'content', 'none' );
		endif;
		?>
        <div class="row">
            <div class="col-md-4 col-12">
                <div class="card">
                    <div class="card-header">Intalasi</div>
                    <div class="card-body">
                        <ul class="list-unstyled list-group-flush">
							<?php
							$args    = array( 'posts_per_page' => 10, 'offset' => 0, 'category_name' => 'Instalasi', );
							$myposts = get_posts( $args );
							foreach ( $myposts as $post ) {
								setup_postdata( $post );
								echo '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
							}
							?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-12">
                <div class="card">
                    <div class="card-header">Rekomendasi Plugin</div>
                    <div class="card-body">
                        <ul class="list-unstyled list-group-flush">
							<?php
							$args    = array( 'posts_per_page' => 10, 'offset' => 0, 'category_name' => 'plugin', );
							$myposts = get_posts( $args );
							foreach ( $myposts as $post ) {
								setup_postdata( $post );
								echo '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
							}
							?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="card">
                    <div class="card-header">Lain-Lain</div>
                    <div class="card-body">
                        <ul class="list-unstyled list-group-flush">
							<?php
							$args    = array( 'posts_per_page' => 10, 'offset' => 0, 'category_name' => 'lain-lain', );
							$myposts = get_posts( $args );
							foreach ( $myposts as $post ) {
								setup_postdata( $post );
								echo '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
							}
							?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>

