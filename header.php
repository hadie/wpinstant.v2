<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="/icon.ico" type="image/x-icon">
    <link rel="icon" href="/icon.ico" type="image/x-icon">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

<header id="header">
    <nav class="navbar navbar-expand-md navbar-light bg-light navbar-custom text-dark" role="navigation">
        <div class="container">
			<?php get_template_part( 'template/logo' ); ?>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#TopMenu" aria-controls="TopMenu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse  justify-content-end" id="TopMenu">
                <ul class="navbar-nav main-navbar">
					<?php
					if ( is_user_logged_in() ) {
						$menu = 'header_menu_login';
					} else {
						$menu = 'header_menu_non_login';
					}
					$defaults = array(
						'theme_location'  => $menu,
						'echo'            => true,
						'fallback_cb'     => '',
						'items_wrap'      => '%3$s',
						'depth'           => 2,
						'container'       => '',
						'container_class' => '',
						'menu_class'      => 'nav navbar-nav',
						'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
						'walker'          => new wp_bootstrap_navwalker()
					);
					//wp_nav_menu( $defaults );
					global $current_user;
					$user_name = $current_user->display_name;
					?>


					<?php if ( is_user_logged_in() ) : ?>
						<?php if ( ! is_page( array( 'create', 'themes' ) ) ) : ?>
                            <li class="nav-item mt-2 mt-md-0">
                                <a href="/create" class="btn nav-link  ml-2 btn-danger text-light"><i
                                            class="fa d-inline fa-lg fa-plus"></i> Create New <span
                                            class="d-none d-md-inline"></span> Theme</a></li>
						<?php endif; ?>

                        <li class="nav-item dropdown mt-2 mt-md-0">
                            <a class="dropdown-toggle nav-link btn ml-2  btn-dark text-light" href="#"
                               id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                <i class="fa d-inline fa-lg fa-user-circle"></i> <?php echo $user_name; ?> <span
                                        class="d-none d-md-inline"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/dashboard">Dashboard</a>
								<?php if ( user_is_active() ): ?>
                                    <a class="dropdown-item" href="/themes">Themes</a>
                                    <a class="dropdown-item" href="/lisensi">License</a>


								<?php endif; ?>
                                <?php
                                // var_dump(wpi_get_theme_option('affiliate_active'));
                               // if(wpi_get_theme_option('affiliate_active') == '1' && current_user_can('manage_options')){
                               if(wpi_get_theme_option('affiliate_active') == '1' && wpi_get_user_role(array('administrator', 'affiliate','adv_affiliate'))){
                                    echo '  <div class="dropdown-divider"></div><a class="dropdown-item" href="/affiliate" class="nav-link"><i class="fa d-inline fa-usd text-success"></i> Affiliate </a>';
                                }
                                ?>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/profile">Profile</a>
                                <a class="dropdown-item" href="/profile#bonus">Bonus</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item  text-danger" href="<?php echo wp_logout_url( home_url() ); ?>"><i
                                            class="fa fa-sign-out"></i>Logout</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a href="/support" target="_blank" class="nav-link"><i
                                        class="fa d-inline fa-question-circle-o"></i> Support </a></li>
					<?php else: ?>

					<?php endif; ?>

                </ul>
            </div>
        </div>
    </nav>
</header>
