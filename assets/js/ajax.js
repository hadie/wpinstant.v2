jQuery(document).ready(function($){
    bootbox.setDefaults({
        locale: "id",
        show: true,
        backdrop: true,
        closeButton: false,
        animate: true,
        className: "my-modal"

    });
    $('#buatkupon').click(function (e) {
        e.preventDefault();
        bootbox.prompt({
            title: "Masukan Kode Kupon Anda",
            closeButton: false,
            inputType: 'text',

            callback: function (result) {
                $.ajax({
                    url: domain_ajax.ajax_url,
                    //dataType: 'json',
                    data: {
                        action: 'createcoupon_action',
                        'kode': result,
                    },
                })
                    .done(function (response) {
                        bootbox.alert(response);


                    })
                    .fail(function () {
                        bootbox.alert('Something Went Wrog ....');

                    })
                    .always(function () {
                        console.log("complete");
                    });

                //console.log(result);

            },

        });

    });

    $('.editkupon').click(function (e) {
        var kupon = $(this).attr('data-kupon');
        var id = $(this).attr('data-id');
        e.preventDefault();
        bootbox.prompt({
            title: "Rubah Kode Kupon",
            value: kupon,
            closeButton: false,
            inputType: 'text',
            callback: function (result) {
                $.ajax({
                    url: domain_ajax.ajax_url,
                    //dataType: 'json',
                    data: {
                        action: 'editkupon_action',
                        'kode': result,
                        'before':kupon,
                        'id':id
                    }
                })
                    .done(function (response) {
                        bootbox.alert(response);


                    })
                    .fail(function () {
                        bootbox.alert('Something Went Wrog ....');

                    })
                    .always(function () {
                        console.log("complete");
                    });

                console.log(result);
            },
            onEscape: function() {
                window.location.reload();
            }
        });
    });

    $('#createlink').submit(function (e) {
        e.preventDefault();
        var data = {
            action: 'CreateLink_action',
            nonce : ajax_object.nonce,
        };
        data = $(this).serialize() + "&" + $.param(data);
        $.ajax({


            url: ajax_object.ajaxurl,
            dataType: 'json',
            data: data,
            success: function(data) {
                console.log(data);
               // $('#linkaffiliate').removeClass('d-none');
                $('#createlink').hide('slow');

                $("input#sales_page").val(data.sales_page);
                $("input#checkout_page").val(data.checkout_page);

                bootbox.alert('Link Affiliate Berhasil Dibuat, Silahkan Refresh Halaman ini.')
            }

        })
            .done(function (data) {
                //console.log(data);

            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
                //$('.loading').hide();
            });

    });

    $('.lakukan_request_delete').on('click',function (e) {
       e.preventDefault();

       var domain_id = $(this).attr('domain-id');
       var user_id = $(this).attr('data-user');
        var parent = $(this).parent("td").parent("tr");

        $.ajax({


            url: ajax_object.ajaxurl,
            dataType: 'json',
            data: {
                nonce : ajax_object.nonce,
                user : user_id,
                domain : domain_id,
                action: 'lakukan_request_delete'
            },

            success: function(data) {
                console.log(data.success);
               if(data.success){
                   parent.fadeOut('slow');
                   parent.hide('slow');
               }
                // bootbox.alert('Link Affiliate Berhasil Dibuat, Silahkan Refresh Halaman ini.')
            }

        })
            .done(function (data) {
                //console.log(data);

            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
                //$('.loading').hide();
            });

    });
    $(":text").keyup(function () {
        $("p").text($(":text").val());
    });

});