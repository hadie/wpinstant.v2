jQuery(document).ready(function ($) {
    //bagian slider
    var rangeSlider = function () {
        var slider = $('.range-slider'),
            range = $('.range-slider__range'),
            value = $('.range-slider__value');
        tpreview = $('.theme_width_preview');
        monitor_width = tpreview.width();
        cpreview = $('.cpreview');
        tpreview.hide('slow');
        slider.each(function () {

            value.each(function () {
                var value = $(this).prev().attr('value');
                $(this).html(value);

            });

            range.on('input', function () {
                $(this).next(value).html(this.value);
                tpreview.show('slow');
                var ukuran = (this.value / (100 / 50)) - (10 / 100) * monitor_width;
                cpreview.width(ukuran + 'px');
            });

        });
    };

    rangeSlider();

});

jQuery(document).ready(function ($) {

    $('input[type=radio][name=font_base_size]').change(function () {
        $('.font-preview').css('font-size', 16 + 'px');
        if (this.value == 'medium') {
            $('.font-preview').css('font-size', 16 + 'px');
            $('.font-heading-preview').css('font-size', 2.5 + 'rem');
        }
        else if (this.value == 'large') {
            $('.font-preview').css('font-size', 18 + 'px');
            $('.font-heading-preview').css('font-size', 2.6 + 'rem');
        } else {
            $('.font-preview').css('font-size', 14 + 'px');
            $('.font-heading-preview').css('font-size', 2.2 + 'rem');
        }
    });

    $('input[type=radio][name=font_heading_size]').change(function () {
        if (this.value == 'medium') {

        }
        else if (this.value == 'large') {

        } else {

        }
    });


    $('#font').fontselect().change(function () {

        // replace + signs with spaces for css
        var font = $(this).val().replace(/\+/g, ' ');

        // split font into family and weight
        font = font.split(':');

        // set family on paragraphs
        $('.font-preview').css('font-family', font[0]);
    });

    jQuery('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
        $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
        $(this).addClass('selected');
        $(this).find('input[type="radio"]').prop("checked", true);

    });

});

$(document).ready(function () {
    // executes when HTML-Document is loaded and DOM is ready
    //console.log("document is ready");
    $('#TopMenu').on('shown.bs.collapse', function () {

        // $( "<hr/>" ).insertAfter( ".main-navbar" );
        // $( ".main-navbar" ).after( "<p>Test</p>" );
        //$( ".container" ).append( $( "<div class='dropdown-divider'></div>" ) );
        $(".affiliate-nav").appendTo(".collapse");
        $(".affiliate-nav").removeClass("justify-content-center");
        $(".affiliate-nav").addClass("navbar-nav");

    })

    $(".copytheme").each(function (index) {
        $(this).click(function (e) {
            e.preventDefault();
            var themename = $(this).attr('data-themename');
            var creator = $(this).attr('data-creator');
            var url = $(this).attr('data-download-url');
            var id = $(this).attr('data-id');
            var currentuser = $(this).attr('data-current-user');

            //var download_url = url + '/' + creator + '/' + themename;

            bootbox.prompt({
                title: "New Theme Name",
                closeButton: false,
                inputType: 'text',

                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: domain_ajax.ajax_url,
                            //dataType: 'json',
                            data: {
                                theme_name: result,
                                theme_slug: themename,
                                theme_id: id,
                                creator: creator,
                                action: 'copy_theme_request'
                            }
                        })
                            .done(function (response) {

                                if(response == 'success'){
                                    var SuccessDialog = bootbox.dialog({
                                        title: 'Theme is ready in your library',
                                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                                        buttons:{
                                            cancel: {
                                                label: 'Close',
                                                className: 'btn-default'
                                            }
                                        }
                                    });

                                    var downloadLink    = url  + currentuser + '/' + string_to_slug(result);
                                    SuccessDialog.init(function(){
                                        setTimeout(function(){
                                            SuccessDialog.find('.bootbox-body').html('Theme <strong>'+result+'</strong> have been success copied for you. <p>Please <a href="'+downloadLink+'">Download Here</a> or look at your <a href="/themes">Theme List</a>!</p>');
                                        }, 1000);
                                    });
                                }
                                else {
                                    bootbox.alert(response);
                                }



                            })
                            .fail(function () {
                                bootbox.alert('Something Went Wrog ....');

                            })
                            .always(function () {
                                console.log("complete");
                            });

                        //console.log(result);

                    }

                }
            });


            //$(this).attr('href',download_url);
        });
    });
    $(".deleteheme").each(function (index) {
        $(this).click(function (e) {
            e.preventDefault();
            var themeid = $(this).attr('data-themeid');
            var userid = $(this).attr('data-userid');
            var themename = $(this).attr('data-themename');
            var themeslug = $(this).attr('data-themeslug');
            var parent = $(this).closest("li");
            bootbox.dialog({
                message: "Are you sure you want to delete <strong>"+themename+"</strong>?",
                title: "<i class='fa fa-trash-o'></i> Delete !",
                closeButton: false,
                buttons: {
                    success: {
                        label: "Cancel",
                        className: "btn-success btn-xs",
                        callback: function () {
                            $('.bootbox').modal('hide');
                            $("#spinner").hide();
                        }
                    },
                    danger: {
                        label: "Delete!",
                        className: "btn-danger btn-xs",
                        callback: function () {
                            $.ajax({
                                url: domain_ajax.ajax_url,
                                data: {
                                    themeid: themeid,
                                    user_id: userid,
                                    themeslug: themeslug,
                                    action: 'delete_theme_request'
                                },

                                success: function () {
                                    $("#spinner").hide();

                                }

                            })
                                .done(function (response) {

                                    if (response == 'success') {
                                        parent.fadeOut('slow');
                                        parent.hide('slow');
                                        bootbox.alert('Theme Berhasil dihapus, Silahkan Refresh (F5) halaman ini.');
                                        return;
                                    } else {
                                        bootbox.alert(response);

                                        return;
                                    }
                                    console.log(response);
                                })
                                .fail(function () {

                                    bootbox.alert('Something Went Wrog ....');

                                })
                                .always(function () {
                                    console.log("complete");
                                });


                        }
                    }
                }
            });
        });
    });
    function string_to_slug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to   = "aaaaeeeeiiiioooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    }
});

