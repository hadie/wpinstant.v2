jQuery(document).ready(function ($) {
    /*
    ================================================================================================
    ********************************** BAGIAN STEP FORM
    ================================================================================================
    */
    var colorOptions = {

        defaultColor: true,
        change: function(event, ui){},
        clear: function() {},
        hide: true,

        palettes: true
    };
    $('.color-field').wpColorPicker(colorOptions);

    $('#manual_color').hide();
    $('#pilih_scheme').show();
    if($('#select_scheme').val() == 'manual'){
        $('#manual_color').show();
        $('#pilih_scheme').hide();
    }

    $('#select_scheme').change(function () {
        if ($('#select_scheme').val() == 'manual') {
            $('#manual_color').show();
            $('#pilih_scheme').hide();
        } else {
            $('#manual_color').hide();
            $('#pilih_scheme').show();
        }
    });



    // ============ tombol next 1 di klik
    $('#next1').on("click", function () {

        var parent = $(this).parents('fieldset');
        var theme_type = $('input[name="theme_type"]:checked').val();
        var arr = [];
        parent.find('input[type="radio"]:checked').each(function () {
            arr.push($(this).val());
        });
        if (arr.length < 1) {
            //alert('please select site theme type !');
            bootbox.alert({
                message: "please select site theme type !",
                backdrop: true,

            });
        }
        else {

            parent.fadeOut(400, function () {
                $(this).next().fadeIn();
                $('.bar-generator').css({'width': '25%'}).html("25%");
                $('.progress-container ul li:nth-child(2)').addClass("active animated pulse");
            });

        }


        if ( theme_type == 'attachment') {
            $('#attachment').show();
        } else {

            $('#attachment').hide();

        }
    });


    // ============ tombol next 2 di klik
    $('#next2').on("click", function () {
        var parent = $(this).parents('fieldset');
        var arr = [];
        parent.find('input[type="text"]').each(function () {
            arr.push($(this).val());
        });

        if ($.inArray('', arr) < 0) {
            parent.fadeOut(400, function () {
                $(this).next().fadeIn();
                $('.bar-generator').css({'width': '50%'}).html("50%");
                $('.progress-container ul li:nth-child(3)').addClass("active animated pulse");
            });

            parent.find('input[type="text"],textarea').each(function () {
                $(this).removeClass('input-error');
                $('.invalid-feedback').hide();
            });

            // if ($('input[name="theme_slug"]').val().indexOf(' ') >= 0) {
            //
            //     $('input[name="theme_slug').addClass('input-error');
            //     $('.invalid-theme-slug').show();
            //     //event.preventDefault();
            // } else {
            //     parent.fadeOut(400, function () {
            //         $(this).next().fadeIn();
            //         $('.bar-generator').css({'width': '50%'}).html("50%");
            //         $('.progress-container ul li:nth-child(3)').addClass("active animated pulse");
            //     });
            //
            //     parent.find('input[type="text"],textarea').each(function () {
            //         $(this).removeClass('input-error');
            //         $('.invalid-feedback').hide();
            //     });
            //
            // }




        }
        else {


            if ($('input[name="theme_name"]').val() == '') {
                $('.invalid-theme-name').show();
                $('input[name="theme_name').addClass('input-error');
            } else {
                $('.invalid-theme-name').hide();
                $('input[name="theme_name').removeClass('input-error');
            }

            if ($('input[name="theme_author"]').val() == '') {
                $('input[name="theme_author').addClass('input-error');
                $('.invalid-theme-author').show();
            } else {
                $('.invalid-theme-author').hide();
                $('input[name="theme_author').removeClass('input-error');
            }

            if ($('input[name="theme_slug"]').val() == '') {
                $('input[name="theme_slug').addClass('input-error');
                $('.invalid-theme-slug').show();

            } else {
                $('.invalid-theme-slug').hide();
                $('input[name="theme_slug').removeClass('input-error');
                // $('.invalid-theme-slug').hide();
                // $('input[name="theme_slug').removeClass('input-error');


            }


            // parent.find('input[type="text"]').each(function () {
            //     // $(this).addClass('input-error');
            //     // $('#alert-input').show();
            //     // $('.invalid-feedback').show();
            //     event.preventDefault();
            //     event.stopPropagation();
            //     //alert('please complete the layout selection !');
            // });
        }
    });

    // ============ tombol next 3 di klik
    $('#next3').on("click", function () {


        var parent = $(this).parents('fieldset');

        var arr = [];
        parent.find('input[type="radio"]:checked').each(function () {
            arr.push($(this).val());
        });

        if (arr.length < 5) {

            if (($('input[name="theme_type"]:checked').val() == 'blog') && arr.length == 4) {
                //alert('Kamu gak milih attachment!');

                parent.fadeOut(400, function () {
                    $(this).next().fadeIn();
                    $('.progress-container ul li:nth-child(4)').addClass("active animated pulse");

                });

            }else  if (($('input[name="theme_type"]:checked').val() == 'wallpaper') && arr.length == 4) {
                //alert('Kamu gak milih attachment!');

                parent.fadeOut(400, function () {
                    $(this).next().fadeIn();
                    $('.progress-container ul li:nth-child(4)').addClass("active animated pulse");

                });

            }

            else {

                if (!$("input[name='header_type']:checked").val()) {
                    $('#sht').addClass('error');
                } else {
                    $('#sht').removeClass('error');
                }
                if (!$("input[name='home_type']:checked").val()) {
                    $('#sho').addClass('error');
                } else {
                    $('#sho').removeClass('error');
                }
                if (!$("input[name='single_type']:checked").val()) {

                    $('#ssp').addClass('error');
                } else {
                    $('#ssp').removeClass('error');
                }
                if (!$("input[name='attachment_type']:checked").val()) {

                    $('#attachment').addClass('error');
                } else {
                    $('#attachment').removeClass('error');
                }

                if (!$("input[name='footer_type']:checked").val()) {
                    $('#sfo').addClass('error');
                } else {
                    $('#sfo').removeClass('error');
                }
                bootbox.alert({
                    message: "please complete the layout selection !",
                    backdrop: true,

                });
                //alert('please complete the layout selection !');
                event.preventDefault();
                event.stopPropagation();


            }

        }
        else {
            parent.fadeOut(400, function () {
                $(this).next().fadeIn();
                $('.bar-generator').css({'width': '75%'}).html("75%");
                $('.progress-container ul li:nth-child(4)').addClass("active animated pulse");

            });
        }
    });

    // ============ tombol next 4 di klik
    $('#next4').on("click", function () {
        var parent = $(this).parents('fieldset');
        var selected_option = $('#select_scheme option:selected');

        var arr = [];
        parent.find('input[type="radio"]:checked').each(function () {
            arr.push($(this).val());
        });

        if ($('#select_scheme').val() == 'scheme') {

            if (arr.length < 1) {
                // alert("please select color scheme !");
                bootbox.alert({
                    message: "please select color scheme !",
                    backdrop: true,

                });
                event.preventDefault();
                event.stopPropagation();
            } else {
                parent.fadeOut(400, function () {
                    $(this).next().fadeIn();
                    $('.progress-container ul li:nth-child(5)').addClass("active animated pulse");

                });
            }
        } else {
            parent.fadeOut(400, function () {
                $(this).next().fadeIn();
                $('.progress-container ul li:nth-child(5)').addClass("active animated pulse");

            });
        }


    });
    //    jika tombol previous di klik
    $('#previous1').click(function () {
        var parent = $(this).parents('fieldset');
        parent.fadeOut(400, function () {
            $(this).prev().fadeIn();
            $('.bar-generator').css({'width': '0%'}).html("0%");
            $('.progress-container ul li:nth-child(2)').removeClass("active");
        });
    });
    $('#previous2').click(function () {
        var parent = $(this).parents('fieldset');
        parent.fadeOut(400, function () {
            $(this).prev().fadeIn();
            $('.bar-generator').css({'width': '25%'}).html("25%");
            $('.progress-container ul li:nth-child(3)').removeClass("active");
        });
    });
    $('#previous3').click(function () {
        var parent = $(this).parents('fieldset');
        parent.fadeOut(400, function () {
            $(this).prev().fadeIn();
            $('.bar-generator').css({'width': '50%'}).html("50%");
            $('.progress-container ul li:nth-child(4)').removeClass("active");
        });
    });
    $('#previous4').click(function () {
        var parent = $(this).parents('fieldset');
        parent.fadeOut(400, function () {
            $(this).prev().fadeIn();
            $('.bar-generator').css({'width': '75%'}).html("75%");
            $('.progress-container ul li:nth-child(5)').removeClass("active");
        });
    });
    /* 
    ===========================================================
    ** POPOVER UNTUK LIVE PREVIEW
    ===========================================================
    */ 
    $('[data-toggle="popover"]').each(function(){
        var $this = $(this);
        var url = $this.data('full');
        $this.popover({
            container: 'body',
            html: true,
            placement: 'bottom',
            trigger: 'manual',
            content: function() {
                return '<img src="' + url + '">'
            }
        });
        $this.on('click', function(){
          var image = new Image();
          image.onload=function(){
                $this.popover('toggle');
          }
          image.src=url; //Preload image in memory
        });
    });
        /* 
    ===========================================================
    ** LIVE PREVIEW HTML SCRIPT
    ===========================================================
    */ 
    var tab_home,tab_single,tab_attachment,tab_container_font = null;
    var url = 'http://app.wpinstant.co/livepreview/';
    $('.front_preview').click(function(){
        var header  = $('input[name=header_type]:checked').val();
        var body    = $('input[name=home_type]:checked').val();
        var footer  = $('input[name=footer_type]:checked').val();
        if($('[name=header_type]:checked').length && $('[name=footer_type]:checked').length && $('[name=home_type]:checked').length){
            if(tab_home == null){
                tab_home = window.open(url+"preview.php?header="+header+"&home="+body+"&footer="+footer, "_blank"); 
            }
            else if(tab_home != null){
                tab_home.location.href=url+"preview.php?header="+header+"&home="+body+"&footer="+footer;
            }
        }
        else{
            alert('please choose header, body and footer to preview !');
        }   
    });

    $('.single_preview').click(function(){
        var header  = $('input[name=header_type]:checked').val();
        var single  = $('input[name=single_type]:checked').val();
        var footer  = $('input[name=footer_type]:checked').val();

        if($('[name=header_type]:checked').length && $('[name=footer_type]:checked').length && $('[name=single_type]:checked').length){
            if(tab_single == null){
                tab_single = window.open(url+"preview.php?header="+header+"&single="+single+"&footer="+footer, "_blank");   
            }
            else if(tab_single != null){
                tab_single.location.href=url+"preview.php?header="+header+"&single="+single+"&footer="+footer;
            }
        }
        else{
            alert('please choose header, footer and single to preview !');
        }
        
    });

    $('.attachment_preview').click(function(){
        var header      = $('input[name=header_type]:checked').val();
        var attachment  = $('input[name=attachment_type]:checked').val();
        var footer      = $('input[name=footer_type]:checked').val();

        if($('[name=header_type]:checked').length && $('[name=footer_type]:checked').length && $('[name=attachment_type]:checked').length){
            if(tab_attachment == null){
                tab_attachment = window.open(url+"preview.php?header="+header+"&attachment="+attachment+"&footer="+footer, "_blank");   
            }
            else if(tab_attachment != null){
                tab_attachment.location.href=url+"preview.php?header="+header+"&attachment="+attachment+"&footer="+footer;
            }
        }
        else{
            alert('please choose header, footer and attachment to preview !');
        }
    });

    $('#preview_width').click(function(){
        var header  = $('input[name=header_type]:checked').val();
        var body    = $('input[name=home_type]:checked').val();
        var footer  = $('input[name=footer_type]:checked').val();
        var width   = $('input[name=theme_width]').val();
        if(tab_container_font == null){
            tab_container_font = window.open(url+"preview.php?header="+header+"&home="+body+"&footer="+footer+"&width="+width, "_blank"); 
        }
        else if(tab_container_font != null){
            tab_container_font.location.href=url+"preview.php?header="+header+"&home="+body+"&footer="+footer+"&width="+width;
        } 
    });

    $('#font').change(function() {
        var header  = $('input[name=header_type]:checked').val();
        var body    = $('input[name=home_type]:checked').val();
        var footer  = $('input[name=footer_type]:checked').val();
        var width   = $('input[name=theme_width]').val();
        var font    = $('input[name=font]').val();

        if(tab_container_font == null){
            tab_container_font = window.open(url+"preview.php?header="+header+"&home="+body+"&footer="+footer+"&width="+width+"&font="+font, "_blank"); 
        }
        else if(tab_container_font != null){
            tab_container_font.location.href=url+"preview.php?header="+header+"&home="+body+"&footer="+footer+"&width="+width+"&font="+font;
        } 
    });

    $('input[name=font_base_size]').change(function() {
        var header      = $('input[name=header_type]:checked').val();
        var body        = $('input[name=home_type]:checked').val();
        var footer      = $('input[name=footer_type]:checked').val();
        var width       = $('input[name=theme_width]').val();
        var font        = $('input[name=font]').val();
        var font_size   = $('input[name=font_base_size]:checked').val();

        if(tab_container_font == null){
            tab_container_font = window.open(url+"preview.php?header="+header+"&home="+body+"&footer="+footer+"&width="+width+"&font="+font+'&fs='+font_size, "_blank"); 
        }
        else if(tab_container_font != null){
            tab_container_font.location.href=url+"preview.php?header="+header+"&home="+body+"&footer="+footer+"&width="+width+"&font="+font+'&fs='+font_size;
        } 
    });

    $('#sidebar_width').change(function() {
        var header      = $('input[name=header_type]:checked').val();
        var body        = $('input[name=home_type]:checked').val();
        var footer      = $('input[name=footer_type]:checked').val();
        var width       = $('input[name=theme_width]').val();
        var font        = $('input[name=font]').val();
        var font_size   = $('input[name=font_base_size]:checked').val();
        var s_width     = $(this).val();

        if(tab_container_font == null){
            tab_container_font = window.open(url+"preview.php?header="+header+"&home="+body+"&footer="+footer+"&width="+width+"&font="+font+'&fs='+font_size+"&sw="+s_width, "_blank"); 
        }
        else if(tab_container_font != null){
            tab_container_font.location.href=url+"preview.php?header="+header+"&home="+body+"&footer="+footer+"&width="+width+"&font="+font+'&fs='+font_size+"&sw="+s_width;
        }
    });
    $('#scheme_preview').click(function() {
        var header      = $('input[name=header_type]:checked').val();
        var body        = $('input[name=home_type]:checked').val();
        var footer      = $('input[name=footer_type]:checked').val();
        var width       = $('input[name=theme_width]').val();
        var font        = $('input[name=font]').val();
        var font_size   = $('input[name=font_base_size]:checked').val();
        var s_width     = $(this).val();
        var scheme      = $('input[name=scheme_type]:checked').val();

        if(tab_container_font == null){
            tab_container_font = window.open(url+"preview.php?header="+header+"&home="+body+"&footer="+footer+"&width="+width+"&font="+font+'&fs='+font_size+"&sw="+s_width+"&st="+scheme, "_blank"); 
        }
        else if(tab_container_font != null){
            tab_container_font.location.href=url+"preview.php?header="+header+"&home="+body+"&footer="+footer+"&width="+width+"&font="+font+'&fs='+font_size+"&sw="+s_width+"&st="+scheme;
        }
    });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
    /*
   ================================================================================================
   ********************************** BAGIAN AJAX
   ================================================================================================
   */

    $('#submt').click(function () {
        // var loader = $('.loading');
        $('.loading').show();
        // var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
        var theme_properties = [{
            'step': 6,
            'theme_type': $('input[name="theme_type"]:checked').val(),
            'theme_name': $('#theme_name').val(),
            'theme_slug': $('#theme_slug').val(),
            'theme_author': $('#theme_author').val(),
            // 'theme_desc': $('#theme_desc').val(),
            'theme_desc': $('#theme_desc').text(),
            'home_type': $('input[name="home_type"]:checked').val(),
            'single_type': $('input[name="single_type"]:checked').val(),
            'attachment_type': $('input[name="attachment_type"]:checked').val(),
            'footer_type': $('input[name="footer_type"]:checked').val(),


            'theme_width': $('#theme_width').val(),
            'sidebar_width': $('#sidebar_width').val(),
            'font': $('#font').val(),
            'select_scheme': $('#select_scheme').val(),

            'color_text': $('input[name="color_text"]').val(),
            'color_link': $('input[name="color_link"]').val(),
            'color_menu_text': $('input[name="color_menu_text"]').val(),
            'color_menu_link': $('input[name="color_menu_link"]').val(),
            'color_sidebar_link': $('input[name="color_sidebar_link"]').val(),

            'background_header': $('input[name="background_header"]').val(),
            'background_menu': $('input[name="background_menu"]').val(),
            'background_content': $('input[name="background_content"]').val(),
            'background_footer': $('input[name="background_footer"]').val(),
            'background_sidebar': $('input[name="background_sidebar"]').val(),

            'framework': $('#framework').val()
        }];

        $.ajax({

            url: my_ajax_object.ajax_url,
            data: {
                data_theme: 'theme_properties',
                action: 'example_ajax_request'
            }
        })
            .done(function (data) {
                console.log(data);
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
                $('.loading').hide();
            });

    });


});

