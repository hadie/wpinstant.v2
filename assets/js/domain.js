$(document).ready(function () {

    $('.delete_domain').click(function (e) {

        e.preventDefault();
        $("#spinner").show();
        var domain = $(this).attr('data-domain');
        var userid = $(this).attr('data-user');
        var parent = $(this).parent(".col").parent(".list-domain");

        bootbox.dialog({
            message: "Are you sure you want to Delete ?",
            title: "<i class='fa fa-trash'></i> Delete !",
            closeButton: false,
            buttons: {
                success: {
                    label: "Cancel",
                    className: "btn-success btn-xs",
                    callback: function () {
                        $('.bootbox').modal('hide');
                        $("#spinner").hide();
                    }
                },
                danger: {
                    label: "Delete!",
                    className: "btn-danger btn-xs",
                    callback: function () {
                        $.ajax({
                            url: domain_ajax.ajax_url,
                            data: {
                                domain_id: domain,
                                user_id: userid,
                                action: 'delete_domain_request'
                            },

                            success: function () {
                                $("#spinner").hide();

                            },

                        })
                            .done(function (response) {

                                if (response == 'success') {
                                    parent.fadeOut('slow');
                                    parent.hide('slow');

                                    bootbox.alert('Domain Berhasil Di hapus, Silahkan Refresh (F5) halaman ini.');
                                    return;
                                } else {
                                    bootbox.alert(response);

                                    return;
                                }
                                console.log(response);
                                $("#spinner").hide();
                            })
                            .fail(function () {

                                bootbox.alert('Something Went Wrog ....');

                            })
                            .always(function () {
                                console.log("complete");
                            });


                    }
                }
            }
        });


    });

    $('.request_delete').click(function (e) {
        e.preventDefault();
        $("#spinner").show();
        var domain = $(this).attr('data-domain');
        var userid = $(this).attr('data-user');
        var parent = $(this).parent(".col").parent(".list-domain");
        bootbox.dialog({
            message: "Are you sure you want to Request Delete this Domain?",
            title: "<i class='fa fa-question-circle'></i> Request Delete !",
            closeButton: false,
            buttons: {
                success: {
                    label: "Cancel",
                    className: "btn-success btn-xs",
                    callback: function () {
                        $('.bootbox').modal('hide');
                        $("#spinner").hide();
                    }
                },
                danger: {
                    label: "Sure!",
                    className: "btn-danger btn-xs",
                    callback: function () {
                        $.ajax({
                            url: domain_ajax.ajax_url,
                            data: {
                                domain_id: domain,
                                user_id: userid,
                                action: 'request_delete_domain'
                            },

                            success: function () {
                                $("#spinner").hide();

                            },

                        })
                            .done(function (response) {

                                if (response == 'success') {
                                    parent.addClass('bg-light');
                                    $(this).addClass('disabled');
                                    // parent.hide('slow');
                                    bootbox.alert('Anda telah berhasil mengirim permintaan Penghapusan Domain. Jika dalam waktu 1x24 jam belum ada Tindakan, Silahkan hubungi Kami di telegram atau kontak support');
                                    return;
                                } else {
                                    bootbox.alert(response);

                                    return;
                                }
                                console.log(response);
                                $("#spinner").hide();
                            })
                            .fail(function () {

                                bootbox.alert('Something Went Wrog ....');

                            })
                            .always(function () {
                                console.log("complete");
                            });


                    }
                }
            }
        });
    });

});