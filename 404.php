<?php
/**
 * wpinstant.v2 WordPress Theme
 * @package wpinstant.v2 WordPress Theme
 * User: dankerizer
 * Date: 06/11/2017 / 15.29
 */
get_header(); ?>
    <main id="main" class="section" role="main">
        <div class="container mt-3">
            <h1 class="display-1 text-bold">Oops!</h1>
            <p class="lead text-bold">We Can't seem to find the page you're looking for.</p>
            <pre>Error code : 404</pre>
            <p>Here are some helpfull link instead</p>
            <ul class="list-unstyled list-group-flush">
                <li><a href="/">Dashboard</a></li>
                <li><a href="/support">Support</a></li>
                <li><a href="/create">Create New Theme</a></li>
                <li><a href="/lisensi">License</a></li>
                <li><a href="/profile">Profile</a></li>

            </ul>
        </div>
    </main>
<?php get_footer(); ?>