<?php
/**
 ** Template Name: Affiliate Page Template
 * wpinstant.v2 WordPress Theme
 * @package wpinstant.v2 WordPress Theme
 * User: dankerizer
 * Date: 11/11/2017 / 01.08
 */
global $current_user;
$allowed_roles = array('administrator', 'affiliate','adv_affiliate');
$affiliate_enable = wpi_get_theme_option('affiliate_active');

if ( ! is_user_logged_in() ) {

	wp_redirect( WPINSTANT_USER_LOGIN );
	exit;
	//$pagename = 'user-login';
}

if(!$affiliate_enable ){
	wp_redirect( WPINSTANT_USER_DASHBOARD );
	exit;
}
if (wpi_get_user_role(array('administrator', 'affiliate','adv_affiliate')) == false) {
	wp_redirect( WPINSTANT_USER_DASHBOARD );
	exit;
}
//if(!current_user_can('manage_options')){
//    wp_redirect( WPINSTANT_USER_DASHBOARD );
//    exit;
//}

$current_user = wp_get_current_user();
$user_name    = $current_user->first_name.' '.$current_user->last_name;
$user_name    = $current_user->display_name;
$user_id      = $current_user->ID;
$avatar       = get_avatar( $user_id, 90, get_option( 'avatar_default', 'mystery' ), $current_user->display_name, array( 'class' => 'rounded-circle' ) );
$date         = date_create( $current_user->user_registered );
$member_since = sprintf( __( 'Member since <span class="text-bold">%s</span>', 'wpinstant' ), date_format( $date, 'M Y' ) );
$users = new Users( $current_user );
//$class = new  WPInstant();
$themes     = $users->themes( $page );
$themecount = $users->themecount();
$lisensi    = $users->lisence( $user_id );
//$sisa       = $users->lisence_domain_count() - $users->domain_count();
$sisa       = $users->lisence_domain_count() - $users->domain_count();
$phone_number = do_shortcode( "[wuoyMember-value type='user' value='wuoym_extra_handphone']" );

$product_id = wpi_get_theme_option('main_product');
get_header();?>
<?php
if (have_posts()) :
	while (have_posts()) :
		the_post(); ?>

        <nav class="section mt-3 visible-md d-md-block d-none" id="main" role="main">
            <div class="container">
                <ul class="nav affiliate-nav justify-content-center">
                    <li class="nav-item"><a class="nav-link" href="/affiliate">Dashboard Affiliate</a></li>
                    <li class="nav-item"><a class="nav-link" href="/affiliate/link">Banner dan Link </a></li>
                    <li class="nav-item"><a class="nav-link" href="/affiliate/bonus">Halaman Bonus</a></li>
                    <li class="nav-item"><a class="nav-link" href="/affiliate/pixel">Setting Pixel</a></li>
                    <li class="nav-item"><a class="nav-link" href="/affiliate/coupon">Kupon Diskon</a></li>
                    <li class="nav-item"><a class="nav-link" href="/affiliate/leaderboard">Leaderboard</a></li>

                </ul>
            </div>
        </nav>
	<main id="main" class="section mt-3">
		<div class="container">

            <?php
            $parent = wp_get_post_parent_id( get_the_id() );
            //var_dump($id);
            if(!$parent){
                get_template_part('template/user','affiliate');
            }else{
                //echo $post->post_name;
                get_template_part('template/affiliate',$post->post_name);
            }

            ?>
<!--			<div class="row">-->
<!--				<div class="col-md-4">-->
<!--					<div class="card">-->
<!--						<div class="card-body text-center position-relative">-->
<!--							<a href="--><?php //echo get_the_permalink();?><!--">--><?php //echo $avatar ?><!--</a>-->
<!--							<h5>--><?php //echo $user_name; ?><!--</h5>-->
<!--							<p class="text-muted">--><?php //echo $member_since ?><!--</p>-->
<!--							<a href="--><?php //echo $edit_url;?><!--" class="edit-link position-absolute small bg-primary text-light"><i class="fa fa-pencil-square-o"></i> edit</a>-->
<!--                            <a href="/profile" class="btn btn-sm btn-secondary">Profile Page</a>-->
<!--                        </div>-->
<!--						<div class="card-body">-->
<!--							<div class="row">-->
<!--								<div class="col user-info text-center">-->
<!--									<span>--><?php //echo $themecount; ?><!--</span>-->
<!--									<span>Themes</span>-->
<!--								</div>-->
<!--								<div class="col user-info text-center">-->
<!--									<span>--><?php //echo number_format( $users->domain_count() ); ?><!--</span>-->
<!--									<span>Domain</span>-->
<!--								</div>-->
<!--								<div class="col user-info text-center">-->
<!--									<span>--><?php //echo number_format( $sisa ); ?><!--</span>-->
<!--									<span>Sisa Lisensi</span>-->
<!--								</div>-->
<!--							</div>-->
<!--						</div>-->
<!--                        --><?php //if($affiliate_enable):?>
<!--                        <div class="card-header">-->
<!--                            <strong>Affiliate</strong>-->
<!--                        </div>-->
<!--                        <ul class="list-group list-group-flush">-->
<!--                            <li class="list-group-item"><a href="/affiliate">Dashboard Affiliate</a></li>-->
<!--                            <li class="list-group-item"><a href="/affiliate/bonus">Halaman Bonus</a></li>-->
<!--                            <li class="list-group-item"><a href="/affiliate/pixel">Setting Pixel</a></li>-->
<!--                            <li class="list-group-item"><a href="/affiliate/leaderboard">Leaderboard</a></li>-->
<!--                        </ul>-->
<!---->
<!--                        --><?php //endif;?>
<!--                        <div class="card-header"><strong>Informasi Lainya</strong></div>-->
<!--						<div class="card-body">-->
<!--							<span class="d-block text-muted">Phone Number</span>-->
<!--							<span class="d-block"><strong>--><?php //echo $phone_number; ?><!--</strong></span>-->
<!---->
<!--						</div>-->
<!---->
<!---->
<!---->
<!--					</div>-->
<!--				</div>-->
<!---->
<!--				<div class="col-md-8">-->
<!--					--><?php
//					$parent = wp_get_post_parent_id( get_the_id() );
//					//var_dump($id);
//					if(!$parent){
//						get_template_part('template/user','affiliate');
//					}else{
//						//echo $post->post_name;
//						get_template_part('template/affiliate',$post->post_name);
//					}
//
//					?>
<!--					--><?php ////get_template_part('template/user','affiliate');?>
<!--					--><?php ////get_template_part('template/affiliate','bonus');?>
<!--				</div>-->
<!--			</div>-->
		</div>
	</main>
		<?php
	endwhile;
else:
	//get_template_part('content', 'none');
endif; ?>
<?php get_footer();?>